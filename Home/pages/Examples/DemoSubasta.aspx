﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DemoSubasta.aspx.cs" Inherits="Home_pages_Examples_DemoSubasta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body {
            width: 99% !important;
        }

        .nav {
            width: 100% !important;
        }

            .nav table {
                width: 10% !important;
                
                color: #d1d1d1;
                font-weight: bold;
                margin: 0 auto;
                    box-shadow: #bbb8b8 0px 0px 7px;
            }

        img {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 90%;
        }


        span {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
        }

            span:hover {
                background-color: #ddd;
                color: black;
                cursor:pointer;
            }

        .previous {
            background-color: #f1f1f1;
            color: black;
        }

        .next {
            background-color: #4CAF50;
            color: white;
        }

        .round {
            border-radius: 50%;
        }
    </style>
    <script type="text/javascript">
        

        APP_NAME = "DEMOSUBASTA";
        IMG_NUM = 0;
        IMG_NAMES = [
            "imgs/subasta1.png",
            "imgs/subasta2.png",
            "imgs/subasta3.png",
            "imgs/subasta4.png"
        ];


        //Ve si el B2B está cargado dentro de un iframe
        function isLoadedInFrame() {
            var loadedInFrame = false;
            try {
                loadedInFrame = (window.self !== window.top);
                //loadedInFrame = (window.frameElement);
            } catch (e) {
                loadedInFrame = true;
            }
            return loadedInFrame;
        }

        //Envia mensaje un iframe padre de LOADED
        function SendWebMessage(obj, data, url) {
            try {
                
                obj = obj || parent;
                data = data || { "origen": APP_NAME, "proceso": "LOADED", "params": { "msg": "Mensaje de prueba..." } };
                obj.postMessage(data, url);
            } catch (e) {
                //alert(e.message);
            }
        }

        //Recibe mensajes de un iframe padre y ejecuta metodos que recibe como parametros
        function callback_ReceiveMessage(evnt) {
            debugger;
            if (evnt.origin == "http://www.cbmex7.com" || evnt.origin == "http://www.cbmex4.com" || evnt.origin == "http://cbmex4.com") {

                var origen = evnt.data.origen;
                var proceso = evnt.data.proceso;
                var parameters = evnt.data.params;

                var dataEnviar =
                    {
                        "origen": APP_NAME,
                        "proceso": proceso,
                        "params": {}
                    };

                if (proceso == "SIGUE_VIVO") {
                    //Envia mensaje a el padre
                    evnt.source.postMessage(dataEnviar, evnt.origin);
                    return;
                }
                //Ejecuta metodo que se recibió en el mensaje
                window[origen + proceso](parameters);
            }
        }


        //Control del nav
        function getImg() {
            return document.getElementById("img_cont");
        }
        function nextImg() {
            if (IMG_NUM + 1 < 4)
                getImg().src = IMG_NAMES[++IMG_NUM];
        }
        function prevImg() {
            if (IMG_NUM - 1 > -1)
                getImg().src = IMG_NAMES[--IMG_NUM];
        }

        function _OnLoad() {
            
            //Registra evento para recibir mensajes desde un iframe y avisa de LOADED
            if (isLoadedInFrame()) {

                var data = {
                    "origen": APP_NAME,
                    "proceso": "LOADED",
                    "params": {}
                };
                SendWebMessage(parent, data, "*");
                //Evento para recibir mensajes desde un iframe
                window.addEventListener('message', callback_ReceiveMessage, false);
            }
        }

    </script>
</head>
<body onload="_OnLoad()">
    <form id="form1" runat="server">
        <div class="nav">
            <table>
                <tr>
                    <td style="text-align: left">
                        <span onclick="prevImg()" href="#" class="previous round">&#8249;</span>
                    </td>
                    <td></td>
                    <td style="text-align: right">
                        <span onclick="nextImg()" href="#" class="next round">&#8250;</span>
                    </td>
                </tr>
            </table>
            &nbsp;
        </div>
        <div>
            <img id="img_cont" src="imgs/subasta1.png" style="" />
        </div>
    </form>
</body>
</html>
