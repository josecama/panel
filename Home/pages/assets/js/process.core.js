//////////////////////////////// Enviar informacion al hijo
function sendMessage(obj, data, url) {
	 //debugger;
	url = url || "";

	if (url == "")
		url = "https://www.cbmex8wvs.com/DispachConsole/Panel/Home/pages/";

	obj.postMessage(data, url);
}

function receiveMessage(callback) {
	 //debugger;
	window.addEventListener('message', callback, false);
}

function callback(data) {
	 //debugger;
	if (data.origin == "http://www.cbmex7.com" || data.origin == "http://cbmex7.com" || data.origin == "http://www.cbmex4.com" || data.origin == "http://cbmex4.com"
		|| data.origin == "http://www.cbmex8wvs.com" || data.origin == "http://cbmex8wvs.com" || data.origin == "http://cbmex10dev.com" || data.origin == "http://www.cbmex10dev.com" || data.origin == "https://www.cbmex10dev.com"
		|| data.origin == "https://cbmex10dev.com") {
		var origen = data.data.origen;
		var proceso = data.data.proceso;
		var parameters = data.data.params;
		window[origen + proceso](parameters);
	}
}