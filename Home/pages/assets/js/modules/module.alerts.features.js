function grid_tree_activas_onRowSelect(rowID, celInd){
    Clear_Grid_Alerts();
    data_grid_alerts();
    Refresh_Grid_Alerts();
}

function grid_tree_historico_onRowSelect(rowID, celInd){
    Clear_Grid_Alerts();
    data_grid_alerts();
    Refresh_Grid_Alerts();
}

function data_grid_alerts(){
    grid_alerts_data.push({
        'Id' : 1,
        'Value' : 'Pedro Perez, 254564,  04/01/2017 - 10:45, 04/01/2017 - 11:00, Zona 4, 1, 2, 3, <span class="glyphicon glyphicon-lock" style="color: #E6B63D;"></span>'
    });
    grid_alerts_data.push({
        'Id' : 2,
        'Value' : 'Ernesto Martinez, 256234, 04/01/2017 - 15:28, 04/01/2017 - 15:30, Zona 3, 1, 1, 2'
    });
	grid_alerts_data.push({
        'Id' : 3,
        'Value' : 'Juan Sosa, 254564,  04/01/2017 - 10:35, 04/01/2017 - 10:45, Zona 4, 1, 2, 3, <span class="glyphicon glyphicon-lock"></span>'
    });
    grid_alerts_data.push({
        'Id' : 4,
        'Value' : 'Alejandro Hernandez, 256234, 04/01/2017 - 15:18, 04/01/2017 - 15:28, Zona 3, 2, 1, 3, <span class="glyphicon glyphicon-lock"></span>'
    });
	grid_alerts_data.push({
        'Id' : 5,
        'Value' : 'Jorge Perez, 254564,  04/01/2017 - 09:45, 04/01/2017 - 10:45, Zona 4, 1, 2, 3'
    });
    grid_alerts_data.push({
        'Id' : 6,
        'Value' : 'Jesús Ascencio, 256234, 04/01/2017 - 14:28, 04/01/2017 - 15:28, Zona 3, 1, 1, 2, <span class="glyphicon glyphicon-lock"></span>'
    });
}

function data_grid_tree_example(){
	grid_tree_activas_data.push({ 
		'id' : '1', 
		'cell_row' : ["1 - Opers gestión", ""], 
		'position' : 0, 
		'parent_id' : null
	});

	grid_tree_activas_data.push({ 
		'id' : '1.1', 
		'cell_row' : ["A - No inicio Jornada (NOINIJORN)", "2"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.2', 
		'cell_row' : ["B -Superv no ha asignado el folio b2b (FOLIONOASIG)"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.3', 
		'cell_row' : ["C - Operador no ha confirmado la asignación (OPERNOCONFIRMASIG)"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.4', 
		'cell_row' : ["D - Traslados: No ha iniciado recorrido", "7"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.5', 
		'cell_row' : ["E - Traslados: Retraso vs tiempo programado"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.6', 
		'cell_row' : ["F - Traslados: Parada injustificada (opc recursos)"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.7', 
		'cell_row' : ["G - Direccion no encontrada-Folio pendiente"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.8', 
		'cell_row' : ["H - Check-In no en ubic cte (INNOUBICGPS)"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.9', 
		'cell_row' : ["I - No actualizó Ubic del PDI a CF"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.10', 
		'cell_row' : ["J - Inicio tarde sig visita riesgo ruta (SIGVISITATARDE)"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.11', 
		'cell_row' : ["K - Retraso: Evidencia ini de trabajo"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.12', 
		'cell_row' : ["L - Retraso: Diagnóstico y tiempo requerido"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.13', 
		'cell_row' : ["M - Retraso: Tiempo SLA del folio en riesgo"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.14', 
		'cell_row' : ["N - Check-Out no en ubic cte (OUTNOUBICGPS)"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '1.15', 
		'cell_row' : ["O - SLA incumplido al terminar el oper el folio"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_activas_data.push({ 
		'id' : '2', 
		'cell_row' : ["2 - Opers rastreo"], 
		'position' : 0, 
		'parent_id' : null
	});
	
	grid_tree_activas_data.push({ 
		'id' : '2.1', 
		'cell_row' : ["A - Sin Rastreo"], 
		'position' : 0, 
		'parent_id' : '2'
	});

	grid_tree_activas_data.push({ 
		'id' : '3', 
		'cell_row' : ["3 - Alertas Diversas"], 
		'position' : 0, 
		'parent_id' : null
	});
	//////////////////////////////////////////////////////////////////
	grid_tree_historico_data.push({ 
		'id' : '1', 
		'cell_row' : ["1 - Opers gestión", ""], 
		'position' : 0, 
		'parent_id' : null
	});

	grid_tree_historico_data.push({ 
		'id' : '1.1', 
		'cell_row' : ["A - No inicio Jornada (NOINIJORN)", "1", null, 1], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.2', 
		'cell_row' : ["B -Superv no ha asignado el folio b2b (FOLIONOASIG)", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.3', 
		'cell_row' : ["C - Operador no ha confirmado la asignación (OPERNOCONFIRMASIG)", null, "1", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.4', 
		'cell_row' : ["D - Traslados: No ha iniciado recorrido", null, "1", null], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.5', 
		'cell_row' : ["E - Traslados: Retraso vs tiempo programado", null, "1", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.6', 
		'cell_row' : ["F - Traslados: Parada injustificada (opc recursos)", "1", null, null], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.7', 
		'cell_row' : ["G - Direccion no encontrada-Folio pendiente", "1", "1", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.8', 
		'cell_row' : ["H - Check-In no en ubic cte (INNOUBICGPS)", "1", null, "2"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.9', 
		'cell_row' : ["I - No actualizó Ubic del PDI a CF", "1", "2", "3"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.10', 
		'cell_row' : ["J - Inicio tarde sig visita riesgo ruta (SIGVISITATARDE)", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.11', 
		'cell_row' : ["K - Retraso: Evidencia ini de trabajo", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.12', 
		'cell_row' : ["L - Retraso: Diagnóstico y tiempo requerido", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.13', 
		'cell_row' : ["M - Retraso: Tiempo SLA del folio en riesgo", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.14', 
		'cell_row' : ["N - Check-Out no en ubic cte (OUTNOUBICGPS)", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '1.15', 
		'cell_row' : ["O - SLA incumplido al terminar el oper el folio", "1"], 
		'position' : 0, 
		'parent_id' : '1'
	});

	grid_tree_historico_data.push({ 
		'id' : '2', 
		'cell_row' : ["2 - Opers rastreo"], 
		'position' : 0, 
		'parent_id' : null
	});
	
	grid_tree_historico_data.push({ 
		'id' : '2.1', 
		'cell_row' : ["A - Sin Rastreo", "1"], 
		'position' : 0, 
		'parent_id' : '2'
	});

	grid_tree_historico_data.push({ 
		'id' : '3', 
		'cell_row' : ["3 - Alertas Diversas"], 
		'position' : 0, 
		'parent_id' : null
	});
}

function Menu_Grid_Llamada(IdAlert){
    alert('Llamada #' + IdAlert);
}

function Menu_Grid_Chat(IdAlert){
    alert('Chat #' + IdAlert);
}

function Menu_Grid_SMS(IdAlert){
    alert('SMS #' + IdAlert);
}