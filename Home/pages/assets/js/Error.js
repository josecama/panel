function Error() {
    this.Debug = true;
    this.Log = function(pException, methodName){
        if(methodName == undefined){
            methodName = "No function";
        }

        try {
            if (pException.message){
                this.PrintMessage(methodName + " -> " + pException.message);
            }
            if (pException.Message){
                this.PrintMessage(methodName + " -> " + pException.Message);
            }
            if (pException.stack){
                this.PrintMessage(methodName + " -> " + pException.stack);
            }
        } catch (e) {
            console.debug(e);
        }
    };
    this.PrintMessage = function(message){
        if(this.Debug){
            console.log(message);
        }
    };
}