﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlertasVisor.aspx.cs" Inherits="pages_AlertasVisor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AlertasVisor</title>
    <link rel="stylesheet" type="text/css" href="../dhtmlxV51/codebase/dhtmlx.css"/>
    <style type="text/css">
	html, body {
		width: 100%;
		height: 100%;
		overflow: hidden;
		margin: 0px;
            font-family: Tahoma, Helvetica, Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif;
            font-size: 0.95em;
	}
        .nota {
            font-size:0.82em;
            color:#999;
        }
  
        div#gmMap1, div#infoMapa {
            position: relative;
            width: 100%;
            height: 100%;
            overflow: auto;
        }
        div#infoMapa {
            padding:10px;
        }
    </style>

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAedAuMLkLE_FR-y2H7o4ZvxAUWqLca-do&libraries=geometry,places,drawing,adsense&language=es&region=mx"></script>
    <script type="text/javascript" src="../jscript/mlGoogleMaps-V10.js"></script>
    <script type="text/javascript" src="../jscript/markerwithlabel_packed.js"></script>

	<script type="text/javascript" src="../dhtmlxV51/codebase/dhtmlx.js"></script>
	<script type="text/javascript" src="../jscript/AlertasVisor.js" charset="utf-8"></script>
</head>
<body>
    <div id="gmMap1" style="display:none;">Mapa</div>
    <div id="infoMapa" style="display:none;">
        <table style="width:95%;">
            <tr><td colspan="2"><div id="dtsFolio" class="nota"></div></td></tr>
            <tr><td colspan="2" style="border-bottom:solid 1px #ddd;"></td></tr>

            <tr>
                <td style="width:20px;"><img src="../images/icons/icong.png" alt="" height="16" /></td>
                <td>Inicio de Traslado.</td>
            </tr>
            <tr><td colspan="2" style="border-bottom:solid 1px #ddd;"></td></tr>
            <tr>
                <td><img src="../images/icons/target24.png" alt="" height="16" /></td>
                <td>Ubicaci&oacute;n del PDI (Cliente).</td>
            </tr>
            <tr><td colspan="2"><div id="dtsPDI" class="nota"></div></td></tr>

            <tr><td colspan="2" style="border-bottom:solid 1px #ddd;"></td></tr>
            <tr>
                <td><img src="../images/icons/user24.png" alt="" height="16" /></td>
                <td>Ubicaci&oacute;n actual del Operador.</td>
            </tr>
            <tr><td colspan="2"><div id="dtsOper" class="nota"></div></td></tr>
            <tr><td colspan="2" style="border-bottom:solid 1px #ddd;"></td></tr>

            <tr><td colspan="2" style="height:20px;"></td></tr>
            <tr>
                <td><input type="checkbox" id="verRastreo" checked="checked" onclick="MostrarRastreo();" /></td>
                <td><label for="verRastreo">Mostrar rastreo del operador.</label></td>
            </tr>
            <tr>
                <td><input type="checkbox" id="unirRastreo" checked="checked" onclick="MostrarRastreo();" /></td>
                <td><label for="unirRastreo">Unir el rastreo con una linea.</label></td>
            </tr>
            <tr>
                <td><input type="checkbox" id="rutaTraslado" checked="checked" onclick="MostrarRuta();" /></td>
                <td><label for="rutaTraslado">Ruta de traslado.</label></td>
            </tr>
        </table>
    </div>
</body>
</html>
