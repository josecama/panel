﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_dispatch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // http://cbmex4.com/DispachConsole/panel/index.html?head_Cliente_Numero=1841254&head_Cliente_Nombre=Mario%20Lopez&head_Cliente_Dir=ni%C3%B1os%20heroes%2058%20guadalajara&head_Cliente_Cruce1=marsella&head_Cliente_Cruce2=&head_Cliente_Alias=Mario&head_Cliente_DIR=Beethoven%204914,%20Zapopan&head_DescricionOtro=HOLA%208&head_Email=HOLA%209&head_Tel=3310394617&head_LAT=0&head_LNG=0&head_NumOrden=00124&head_Ubic=123&head_ContactId=1
        RecuperaDatos();

    }

    public void RecuperaDatos(){
        //Request["head_Cliente_Latitud"]
        //Request["head_Cliente_Longitud"]
        clnNum.InnerText = (Request["head_Cliente_Numero"] == null) ? "1841254" : Request["head_Cliente_Numero"].ToString();
        clnNom.InnerHtml = (Request["head_Cliente_Nombre"] == null) ? "Mario Lopez" : Request["head_Cliente_Nombre"].ToString();
        topDomicilio.Value = (Request["head_Cliente_Dir"] == null) ? "Niños heroes 58, Guadalajara" : Request["head_Cliente_Dir"].ToString();
        topCruce1.Value = (Request["head_Cliente_Cruce1"] == null) ? "marsella" : Request["head_Cliente_Cruce1"].ToString();
        topCruce2.Value = (Request["head_Cliente_Cruce2"] == null) ? "" : Request["head_Cliente_Cruce2"].ToString();
        topClnAlias.InnerHtml = (Request["head_Cliente_Alias"] == null) ? "Mario" : Request["head_Cliente_Alias"].ToString();
        clnDirMapa.InnerHtml = (Request["head_Cliente_DirMapa"] == null) ? "Beethoven 4914, Zapopan" : Request["head_Cliente_DirMapa"].ToString();
        //string head_DescricionOtro = (Request["head_DescricionOtro"] == null) ? "Hola" : Request["head_DescricionOtro"].ToString();
        string head_Email = (Request["head_Email"] == null) ? "mario@mail.com" : Request["head_Email"].ToString();
        string head_Tel = (Request["head_Tel"] == null) ? "" : Request["head_Tel"].ToString();
        string head_LAT = (Request["head_LAT"] == null) ? "" : Request["head_LAT"].ToString();
        string head_LNG = (Request["head_LNG"] == null) ? "" : Request["head_LNG"].ToString();
        string head_NumOrden = (Request["head_NumOrden"] == null) ? "00124" : Request["head_NumOrden"].ToString();
        string head_Ubic = (Request["head_Ubic"] == null) ? "" : Request["head_Ubic"].ToString();
        string head_ContactId = (Request["head_ContactId"] == null) ? "" : Request["head_ContactId"].ToString();
    }
}