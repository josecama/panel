﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HomeMapCel.aspx.vb" Inherits="pages_HomeMapCel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Programs</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="../dhtmlxV5/skins/terrace/dhtmlx.css"/>
	<style>
		html, body {
			width: 100%;
			height: 100%;
			overflow: hidden;
			margin: 0px;
		}
        #header { padding:5px; }
	</style>

	<script type="text/javascript" src="http://cbmex7.com/DHTMLX/dhtmlxSuite_v50_std/codebase/dhtmlx.js"></script>

    <script type="text/javascript">
        var ICONS_PATH = "images/icons/";
        var IMAGES_PATH = "http://cbmex7.com/DHTMLX/dhtmlxSuite_v50_std/skins/skyblue/imgs/"; // *OJO* - debe corresponder con el archivo de skin utilizado

        // PRODUCCION: http://cbmex7.com/cbmex6/GoogleViewerOtros/RepRastreo/Programs.aspx

        dhtmlxEvent(window, "load", function () {
            CreaLayout();
            InicializaIframes();
        });

        function InicializaIframes() {
            document.getElementById("iframe1").src = "http://www.cbmex7.com/B2BApi/PagesAdmin/PanelFolio.aspx?emp=1&type=PR&r=1&token=b8cf7396516148218fdb586f9f85b8";
            document.getElementById("iframe2").src = "http://www.cbmex7.com/cbmex6/GoogleViewer/Login.aspx";
            document.getElementById("iframe3").src = "http://cbmex4.com/DispachConsole/Panel/Home/pages/dispatch.aspx";
            //http://cbmex4.com/DispachConsole/Panel/Home/pages/HomeMapCel.aspx
            //http://localhost:5641/pages/dispatch.aspx
        }

        function CreaLayout() {

            var main_layout = new dhtmlXLayoutObject(document.body, '2E');

            var header = main_layout.cells('a');
            header.setHeight('54');
            header.hideHeader();
            header.attachObject("header");

            var content = main_layout.cells('b');
            content.hideHeader();

            var params = {};

            var tabbar_1 = content.attachTabbar();
            tabbar_1.addTab('tab_1', 'B2B');
            var tab_1 = tabbar_1.cells('tab_1');
            tab_1.setActive();
            tab_1.attachObject('iframe1');

            tabbar_1.addTab('tab_2', 'Visor');
            var tab_2 = tabbar_1.cells('tab_2');
            params = {
                head_Cliente_Numero: "1841254",
                head_Cliente_Nombre: "Mario Lopez",
                head_Cliente_Dir: "Beethoven 4914, lomas del seminario, Zapopan",
                head_Cliente_Cruce1: "doctores",
                head_Cliente_Cruce2: "clouthier",
                head_Cliente_Alias: "Mario",
                head_Cliente_DIR: "Beethoven 4914, Zapopan",
                head_DescricionOtro: "HOLA 8",
                head_Email: "HOLA 9",
                head_Tel: "3310394617",
                head_LAT: "0",
                head_LNG: "0",
                head_NumOrden: "00124",
                head_Ubic: "123",
                head_ContactId: "1"
            };
            tab_2.attachObject('iframe2');
            //http://cbmex4.com/DispachConsole/panel/index.html?head_Cliente_Numero=1841254&head_Cliente_Nombre=Mario%20Lopez&head_Cliente_Dir=Beethoven%204914%20seminario%20,%20Zapopan&head_Cliente_Cruce1=doctores&head_Cliente_Cruce2=clouthier&head_Cliente_Alias=Mario&head_Cliente_DIR=Beethoven%204914,%20Zapopan&head_DescricionOtro=HOLA%208&head_Email=HOLA%209&head_Tel=3310394617&head_LAT=0&head_LNG=0&head_NumOrden=00124&head_Ubic=123&head_ContactId=1

            tabbar_1.addTab('tab_3', 'Dispatch Console');
            var tab_3 = tabbar_1.cells('tab_3');
            tab_3.attachObject('iframe3');

            var toolbar_3 = content.attachToolbar();
            toolbar_3.setIconsPath(ICONS_PATH);
            toolbar_3.addButton("a1", 1, "Btn 1", "star.png", "star.png");
            toolbar_3.addSeparator("sep", 2);
            toolbar_3.addButton("a2", 3, "Btn 2", "cross.png", "cross.png");
            toolbar_3.addSeparator("sep", 4);
            toolbar_3.addInput("a3", 5  , "buscar...", 75);
        }
    </script>
</head>
<body>
    <div id="header">
        <div style="width:33.33%; float:left; text-align:left;"><img src="http://www.cbmex7.com/cbmex6/GoogleViewer/img/logo.png" /></div>
        <div style="width:33.33%; float:left; text-align:center;"><span style="font-size:20px;">Usuario Logeado</span><br /><a href="#">salir</a></div>
        <div style="width:33.33%; float:left; text-align:right;">Otros datos</div>
    </div>
    <div><iframe id="iframe1" style="width:100%; height:100%; overflow:auto;" src=""></iframe></div>
    <div><iframe id="iframe2" style="width:100%; height:100%; overflow:auto;" src=""></iframe></div>
    <div><iframe id="iframe3" style="width:100%; height:100%; overflow:auto;" src=""></iframe></div>
</body>
</html>
