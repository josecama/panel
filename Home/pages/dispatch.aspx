﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dispatch.aspx.cs" Inherits="pages_dispatch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>DISPATCH CONSOLE</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />-->

    <link rel="stylesheet" type="text/css" href="../dhtmlxV5/skins/material/dhtmlx.css"/>
    <link rel="stylesheet" type="text/css" href="../font-awesome-4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="../css/dispatch.css"/>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpnIrpYExnUzAfqPasWGC7ZwGbbf6IfVY&libraries=geometry,places,drawing,adsense&language=es&region=mx"></script>
    <script type="text/javascript" src="../jscript/mlGoogleMaps.js"></script>
    <script type="text/javascript" src="../jscript/markerwithlabel_packed.js"></script>

	<script type="text/javascript" src="../dhtmlxV5/codebase/dhtmlx.js"></script>
	<script type="text/javascript" src="../jscript/dispatch.js" charset="utf-8"></script>
</head>
<body>
    <div id="datGrls" class="topData">
        <!-- TOP HEADER (LINEA EN NEGRO) -->
        <div style="background-color:#ecebeb; padding:5px;">
            <table style="width:100%; max-width:1200px;">
                <tr>
                    <td>
                        <b>MapCEL - Field Service:</b> <i>Consola de Búsqueda Direc - </i>2017/12/18 Lun - <span class="altColor">16:45</span>
                    </td>
                    <td style="width:150px;">
                        Dish: <span class="altColor">DashBoard-360</span>
                    </td>
                    <td style="width:110px;">
                        <select>
                            <option value="GDL">Dish-GDL</option>
                            <option value="MTY">Dish-MTY</option>
                            <option value="CHIS">Dish-CHIS</option>
                        </select>
                    </td>
                    <td style="width:65px; text-align:right;"><a href="http://cbmex4.com/DispachConsole/panel/index.html?head_Cliente_Numero=1841254&head_Cliente_Nombre=Mario%20Lopez&head_Cliente_Dir=ni%C3%B1os%20heroes%2058%20guadalajara&head_Cliente_Cruce1=marsella&head_Cliente_Cruce2=&head_Cliente_Alias=Mario&head_Cliente_DIR=Beethoven%204914,%20Zapopan&head_DescricionOtro=HOLA%208&head_Email=HOLA%209&head_Tel=3310394617&head_LAT=0&head_LNG=0&head_NumOrden=00124&head_Ubic=123&head_ContactId=1" target="_blank" title="Salir" class="button button-inline button-small button-info"><i class="fa fa-sign-out"></i> <span>Salir</span></a></td>
                </tr>
            </table>
        </div>

        <!-- INFORMACION DEL CLIENTE -->
        <div style="padding:5px;">
            <!-- DATOS DEL CLIENTE -->
            <table style="width:100%; max-width:1200px;">
                <tr>
                    <td style="width:110px;">Cliente número: </td>
                    <td style="width:284px;">
                        <div class="truncate altColor" style="width:260px;">
                            <span id="clnNum" runat="server"></span>&nbsp;
                            <span id="clnNom" runat="server"></span>
                        </div>
                    </td>
                    <td style="width:209px;"><a href="#" class="button button-inline button-small button-info"><i class="fa fa-save"></i><span> Guardar y Buscar Horarios</span></a></td>
                    <td><a href="#" class="button button-inline button-small button-info" onclick="savePDI();"><i class="fa fa-save"></i><span> Guardar</span></a></td>
                    <td style="width: 184px;">
                        Pais: 
                        <select style="width:152px;">
                            <option>México</option>
                            <option>Argentina</option>
                        </select>
                    </td>
                    <td><a href="#" onclick="changeConfiguration();" style="margin-left: 8px;" class="button button-inline button-small button-info"><i class="fa fa-cogs"></i></a></td>
                    <td>Núm. de orden en proceso:</td>
                    <td class="altColor" style="width:50px;text-align:right;">00124</td>
                </tr>
            </table>

            <!-- DOMICILIO DEL CLIENTE -->
            <table style="width:100%; max-width:1200px;">
                <tr>
                    <td style="width:106px;">Direccion cliente: </td>
                    <td style="width: 248px;"><input type="text" id="topDomicilio" runat="server" style="width: 100%;" /></td>
                    <td style="width: 58px;text-align:right;"> Cruce1: </td>
                    <td style="width:151px;"><input type="text" id="topCruce1" runat="server" style="width: 100px;" /></td>
                    <td style="width: 51px; text-align: left;"> Cruce2: </td>
                    <td style="width:140px;"><input type="text" id="topCruce2" runat="server" style="width: 100%;" /></td>
                    <td style="width: 78px;">
                        <a href="#"  onclick="BuscarDomTop();return false;" class="button button-inline button-small button-info"><i class="fa fa-search"></i><span> Buscar</span></a>
                    </td>
                    <td style="width:40px;">Alias: </td>
                    <td style="width:65px;"><div class="altColor" style="width:65px;" id="topClnAlias" runat="server">Mario</div></td>
                    <td style="width:95px;text-align:right;">
                        <select style="width:95px;">
                            <option>Ubic-ID-PDI</option>
                        </select>
                    </td>
                    <td style="width:50px;text-align:right;">
                        <span class="altColor" id="clnUbicacionId" runat="server">123</span>
                    </td>
                </tr>
            </table>

            <!-- DOMICILIO EN MAPA -->
            <table style="width:100%; max-width:1200px;">
                <tr>
                    <td style="width:110px;">Dirección mapa: </td>
                    <td style="width:488px;"><div class="truncate altColor" style="width:488px;" id="clnDirMapa" runat="server" ></div></td>
                    <td style="width:55px;text-align:right;">Cruce1:</td>
                    <td style="width:140px;"><div class="truncate altColor" style="width:100px;" id="clnCruce1Mapa" runat="server" ></div></td>
                    <td style="width:55px;text-align:right;">Cruce2:</td>
                    <td style="width:150px;"><div class="truncate altColor" style="width:100px;" id="clnCruce2Mapa" runat="server" ></div></td>
                    <td></td>
                    <td style="width:75px;text-align:right;">
                        <input type="radio" name="radActObs" id="radActObs_Activa" /> <label for="radActObs_Activa">Activa</label>
                    </td>
                    <td style="width:90px;text-align:right;">
                        <input type="radio" name="radActObs" id="radActObs_Obsoleta" /> <label for="radActObs_Obsoleta">Obsoleta</label>
                    </td>
                </tr>
            </table>

            <!-- DATOS DE CONTACTO -->
            <table style="width:100%; max-width:1200px;">
                <tr>
                    <td style="width:110px;">
                        <select style="width:100px;">
                            <option value="">Contacto ID</option>
                            <option value="">123</option>
                            <option value="">456</option>
                        </select>
                    </td>
                    <td style="width:180px;">
                        <div class="truncate altColor" style="width:180px;">
                            <span id="Span1" runat="server">12345</span>&nbsp;
                            <span id="Span2" runat="server">Abcd Efgh</span>
                        </div>
                    </td>
                    <td>Email:</td>
                    <td style="width: 271px;"><div class="truncate altColor" style="width: 271px;">gilberto.flores@mapalocalizador.com</div></td>
                    <td style="width:46px;">Tel:</td>
                    <td style="width:117px;"><div class="truncate altColor" style="width:117px;">+52 334785956</div></td>
                    <td><a href="#" style="margin-right: 5px;" class="button button-inline button-small button-info" onclick="Sugerencias(1);"><i class="fa fa-street-view"></i><span></span></a></td>
                    <td>Lat:</td>
                    <td style="width: 58px;"><div class="truncate altColor" style="width:58px;  margin-right: 6px;" id="clnLatMapa"></div></td>
                    <td>Lng:</td>
                    <td style="width: 63px;"><div class="truncate altColor" style="width:58px;" id="clnLngMapa"></div></td>
                    <td><a href="#" onclick="sendInformation();" class="button button-inline button-small button-info"><i class="fa fa-cloud"></i><span> Enviar</span></a></td>
                    <td>&nbsp;&nbsp;&nbsp;Estatus: </td>
                    <td><img src="../images/icons/pinVerde16.png" alt="" /> <span class="altColor"><b>BAP</b></span></td>
                </tr>
            </table>
        </div>
    </div>

    <div id="formDom" style="width:100%;height:100%;margin:5px;overflow:auto; color: #8e8e8e;">
        <table>
            <tr>
                <td>
                    <a href="#" onclick="clearBuscador();return false;" class="button button-inline button-small button-primary"><i class="fa fa-history"></i></a>
                </td>
            </tr>
            <tr>
                <td style="width:65px;">Calle: </td>
                <td><input type="text" id="fdomCalle" style="width:275px;" /></td>
            </tr>
            <tr>
                <td>Num.Ext: </td>
                <td>
                    <table>
                        <tr>
                            <td><input type="text" id="fdomNumExt" style="width:100px;" /></td>
                            <td>&nbsp;</td>
                            <td> Interior: </td>
                            <td><input type="text" id="fdomNumInt" style="width:100px;" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Colonia: </td>
                <td>
                    <table>
                        <tr>
                            <td><input type="text" id="fdomColonia" style="width:175px;" /></td>
                            <td> CP: </td>
                            <td><input type="text" id="fdomCodPos" style="width:60px;" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Ciudad: </td>
                <td><input type="text" id="fdomCiudad" style="width:200px;" />
                    <a href="#" onclick="BuscarDomForm();return false;" class="button button-inline button-small button-primary"><i class="fa fa-search"></i> <span>Buscar</span></a>
                </td>
            </tr>
            <tr>
                <td>Estado: </td>
                <td><input type="text" id="fdomEstado" style="width:200px;" />
                    <a href="#" onclick="MuestraGridTmp();return false;" class="button button-inline button-small button-primary"><i class="fa fa-save"></i> <span>Results</span></a>
                    <input type="hidden" id="fdomPais" value="Mexico" />
                </td>
            </tr>
            <tr>
                <td>Cruce1: </td>
                <td>
                    <table>
                        <tr>
                            <td><input type="text" id="fdomCruce1" style="width: 93px;" /><img src="../images/icons/cruce1_16x16.png" alt="" /></td>
                            <td>Cruce2: </td>
                            <td><input type="text" id="fdomCruce2" style="width: 93px;" /><img src="../images/icons/cruce2_16x16.png" alt="" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td colspan="2"><b>Busqueda de lugares cercanos a:</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <input type="radio" name="fdomTpoCerca" id="fdomTpoCerca_CP" checked="checked" />
                                <label for="fdomTpoCerca_CP">Cod.Postal</label>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="radio" name="fdomTpoCerca" id="fdomTpoCerca_D1" />
                                <label for="fdomTpoCerca_D1">Dom.Primario (1)</label>
                            </td>
                            <td style="width:20px;"></td>
                            <td>
                                <select id="fdomRadio" onchange="changeRadioCodPostal(parseInt(this.value))">
                                    <option value="250">250 mts</option>
                                    <option value="500">500 mts</option>
                                    <option value="750">750 mts</option>
                                    <option value="1000" selected="selected">1000 mts</option>
                                    <option value="1500">1500 mts</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Texto buscar: </td>
                <td><input type="text" id="fdomLugBuscar" style="width:175px;" />
                <a href="#" onclick="BuscarLugares();return false;" class="button button-inline button-small button-primary"><i class="fa fa-search"></i> <span>Buscar</span></a>
                </td>
            </tr>
        </table>
        <br />&nbsp;
    </div>

    <div id="gmMap1"></div>

    <div id="gridResult"></div>
    
    <div id="divMapAddress" style="margin:10px 0 0 10px;">
        <table>
            <tr>
                <td><input type="text" placeholder="domicilio a buscar..." id="mapTextBuscar" style="width:340px;height:22px;border:solid 1px #ff6a00;" /></td>
                <td><a href="#" onclick="GMAP1.getDom('mapTextBuscar').value = '';return false;" title="Borrar" class="button button-inline button-small button-primary"><i class="fa fa-remove"></i></a></td>
                <td><a href="#" onclick="BusquedaEnMapa();return false;" title="Buscar" class="button button-inline button-small button-primary"><i class="fa fa-search"></i> <span>Buscar</span></a></td>
            </tr>
        </table>
    </div>

    <div id="divMapCapas" style="margin:5px;font-size:14px;">
        <table>
            <tr>
                <td>
                    <span style="width:40px;text-align:right;"><a href="#" onclick="MostrarCapas();return false;" title="Ver Capas" class="button button-inline button-small button-primary"><i class="fa fa-plus"></i> <span>Mostrar Capas</span></a></span>
                </td>
            </tr>
            <tr id="tblMapCapas" style="display:none;">
                <td>
                    <table class="topData">
                        <tr><td><input type="checkbox" id="chkCapa1" checked="checked" onchange="ProcesaCapas();" /> <label for="chkCapa1">Codigo Postal</label></td><td>&nbsp;</td></tr>
                        <tr><td><input type="checkbox" id="chkCapa2" checked="checked" onchange="ProcesaCapas();" /> <label for="chkCapa2">Cruce 1</label></td><td>&nbsp;</td></tr>
                        <tr><td><input type="checkbox" id="chkCapa3" checked="checked" onchange="ProcesaCapas();" /> <label for="chkCapa3">Cruce 2</label></td><td>&nbsp;</td></tr>
                        <tr><td><input type="checkbox" id="chkCapa4" checked="checked" onchange="ProcesaCapas();" /> <label for="chkCapa4">Alrededor</label></td><td>&nbsp;</td></tr>
                        <!--
                        <tr><td><input type="checkbox" id="chkCapa5" checked="checked" onchange="ProcesaCapas();" /> <label for="chkCapa5">CP Cercanos</label></td><td>&nbsp;</td></tr>
                        <tr><td><input type="checkbox" id="chkCapa6" checked="checked" onchange="ProcesaCapas();" /> <label for="chkCapa6">Ubic. Alternas</label></td><td>&nbsp;</td></tr>
                        -->
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="divBotones" class="zoomMap">
        <button title="Z1" value="Z1" onclick="javascript: GMAP1.map.setZoom(7);" class="button button-inline button-small button-primary">Z1</button> &nbsp;&nbsp;
        <button title="Z2" value="Z2" onclick="javascript: GMAP1.map.setZoom(12);" class="button button-inline button-small button-primary">Z2</button>&nbsp;&nbsp;
        <button title="Z3" value="Z3" onclick="javascript: GMAP1.map.setZoom(17);" class="button button-inline button-small button-primary">Z3</button>
    </div>

    <div id="btn_expand_mapa" class="btn_expand_map" onclick="resizeMap()">
        <i class="fa fa-expand fa"></i>
    </div>
</body>
</html>
