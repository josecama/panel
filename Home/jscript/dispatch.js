﻿var ICONS_PATH = "../images/icons/";
var IMAGES_PATH = "../dhtmlxV5/skins/material/imgs/"; // *OJO* - debe corresponder con el archivo de skin utilizado

var GMAP1 = null;
var MAP_CONTEXT_MENU = null;
var MAP_POS_CLIC = null;
var SHAPE_DATA = null;
var GENERIC_WIN1 = null;
var WINDOWS = null;

var layMain = null;
var layDatos = null;
var layMain_Top = null;
var layDatosWidth = null;
var layMain_TopHeight = null;

var GRID_UBICS = null;
var MARKERS_UBICS = [];
var ID_UBIC = 0, ID_LUGAR = 0;
var POS_CP = null, POS_CR1 = null, POS_CR2 = null;
var ICON_DOM_PRIM = "radio-checked.png";
var ICON_DOM_SEC = "radio.png";

var WIN_TMP = null;
var GRID_TMP = null;
var MARKERS_TMP = [];

var urlWebService = "http://localhost:2982/WebService1.asmx/HelloWorld";
var urlApiCodigoPostal = "http://localhost:4181/api/CodigoPostal/";
var pendientes = "";

var latitudDefault = 0;
var longitudDefault = 0;
var zoomDefault = 0;

dhtmlxEvent(window, "load", function () {
    //provides the script as a handler of the 'onload' HTML event
    CreaLayout();
    ConfiguraMapa();

    var latitud = getValueUrl("head_Cliente_Latitud");
    var longitud = getValueUrl("head_Cliente_Longitud");

    //////////
    var idPDI = getValueUrl("head_PDI");
    var user = getValueUrl("head_user");
    var password = getValueUrl("head_password");
    
    if (latitud != null && longitud != null) {
        var position = latitud + ", " + longitud;
        GMAP1.geocodeAddress('A', position, null, null, false, function (dirs) {
            if (dirs.length > 0) {
                var dir = dirs[0];
                var numItem = 0;
                var icon = ICONS_PATH + "1V-24.png";

                MARKERS_UBICS.push({});
                var numItem = MARKERS_UBICS.length - 1;
                MARKERS_UBICS[numItem].dom = dir.dom;
                MARKERS_UBICS[numItem].pos = dir.pos;
                MARKERS_UBICS[numItem].country = dir.country;
                MARKERS_UBICS[numItem].countryShort = dir.countryShort;
                MARKERS_UBICS[numItem].externalNum = dir.externalNum;
                MARKERS_UBICS[numItem].locality = dir.locality;
                MARKERS_UBICS[numItem].neighborhood = dir.neighborhood;
                MARKERS_UBICS[numItem].phone = dir.phone;
                MARKERS_UBICS[numItem].posType = dir.posType;
                MARKERS_UBICS[numItem].postalCode = dir.postalCode;
                MARKERS_UBICS[numItem].state = dir.state;
                MARKERS_UBICS[numItem].stateShort = dir.stateShort;
                MARKERS_UBICS[numItem].streetName = dir.streetName;
                MARKERS_UBICS[numItem].sublocality = dir.sublocality;
                MARKERS_UBICS[numItem].types = dir.types;
                MARKERS_UBICS[numItem]._type = "D";
                MARKERS_UBICS[numItem]._id = 1;
                MARKERS_UBICS[numItem].accuracy = dir.types;

                MARKERS_UBICS[numItem]._marker = GMAP1.createMarker(dir.pos, icon, "", dir.dom, GMAP1.formatAddress(dir), true);
                MARKERS_UBICS[numItem]._marker.addListener('dragend', markerdragend);

                SetDirSeleccionada(MARKERS_UBICS[numItem]);
                
                BusquedaInicial_OK();

                if (MARKERS_UBICS[numItem].postalCode == "") {
                    for (var i in MARKERS_UBICS) {
                        if (MARKERS_UBICS[i]._type == "CP") {
                            MARKERS_UBICS[i]._marker.setMap(null);
                            MARKERS_UBICS[i]._circle.overlay.setMap(null);
                        }
                    }
                }
                else {
                    BuscarCodPostal2();
                }
            }
        });
    }
    else if (idPDI != null && idEmpresa != null) {
    }
    else {
        if (document.getElementById("topDomicilio").value != "") {
            BuscarDomTop();
        }
    }
    sendMessage(parent, { "origen":"BUSCARDIRNUEVO", "proceso":"LOADED",  "params": {}});
});

function CreaLayout() {
    // https://dhtmlx.com/docs/products/visualDesigner/live/#955kc6
    // https://dhtmlx.com/docs/products/visualDesigner/live/?preview=true#4ht780

    // CREA LAYOUT PRINCIPAL
    layMain = new dhtmlXLayoutObject(document.body, '3L');

    // CREA AREA PARA MENU IZQUIERDO

    var layMain_Left = layMain.cells('a');
    layMain_Left.setText('<b>MENU</b>');
    layMain_Left.setWidth('120');
    layMain_Left.fixSize(1, 0);
    layMain_Left.collapse();

    layMain.attachEvent('onPanelResizeFinish', function (e) { GMAP1.resize(); });

    var leftSidebar = layMain_Left.attachSidebar({ template: 'icons_text', width: '120', icons_path: ICONS_PATH, autohide: '', header: 'Opciones', iconset: 'awesome' });

    leftSidebar.addItem({ id: 'sidebar_item_1', text: 'VISOR GIS', icon: 'visor-gis.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_2', text: 'CLIENTES', icon: 'clientes.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_3', text: 'ORDENES', icon: 'ordenes.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_4', text: 'MONITOREO', icon: 'monitoreo.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_5', text: 'MENSAJERIA', icon: 'mensajeria.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_6', text: 'REPORTES', icon: 'reportes.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_7', text: 'CONFIG.', icon: 'config.png' });
    leftSidebar.addItem({ type: 'separator' });
    leftSidebar.addItem({ id: 'sidebar_item_8', text: 'AYUDA', icon: 'ayuda.png' });
    leftSidebar.addItem({ type: 'separator' });

    leftSidebar.attachEvent('onSelect', function (id, lastId) {
        alert(id);
    });

    // CREA AREA PARA DATOS GENERALES (CENTRO-ARRIBA)

    layMain_Top = layMain.cells('b');
    layMain_Top.hideHeader();
    layMain_Top.setHeight('168');
    layMain_Top.fixSize(0, 1);
    layMain_Top.setCollapsedText('DATOS PRINCIPALES');
    layMain_Top.attachObject("datGrls");

    // CREA AREA PARA MAPA Y DATOS DE UBICACIONES (CENTRO-ABAJO)

    var layMain_Center = layMain.cells('c');
    layMain_Center.hideHeader();
    var layMapDatos = layMain_Center.attachLayout('2U');

    layMapDatos.attachEvent('onPanelResizeFinish', function (e) { GMAP1.resize(); });

    var layMap = layMapDatos.cells('a');
    layMap.hideHeader();
    layMap.attachObject("gmMap1");

    layDatos = layMapDatos.cells('b');
    layDatos.setWidth('400');
    layDatos.setCollapsedText('DOMICILIO');
    layDatos.hideHeader();

    // CREA UN TABBAR EN AREA DE DATOS DE UBICACIONES

    var dataTabbar = layDatos.attachTabbar();

    dataTabbar.addTab('dat_tabDoms', 'Domicilio');
    var datTabDoms = dataTabbar.cells('dat_tabDoms');
    datTabDoms.setActive();

    // TAB1 - CREA 2 SECCIONES (datos domicilio y grid ubicaciones)

    var layBuscar = datTabDoms.attachLayout('2E');

    // SECCION 1 - Datos Domicilio

    var layBusForma = layBuscar.cells('a');
    layBusForma.setText('Datos de Busqueda');
    layBusForma.setHeight('220');
    layBusForma.hideHeader();
    layBusForma.attachObject("formDom");

    // SECCION 2 - Grid Ubicaciones

    var layBusGrid = layBuscar.cells('b');
    layBusGrid.hideHeader();
    GRID_UBICS = layBusGrid.attachGrid();
    GRID_UBICS.setIconsPath(ICONS_PATH);
    GRID_UBICS.setImagePath(IMAGES_PATH);

    GRID_UBICS.setHeader("Pr,No,Ver,Ubicaciones,,");
    GRID_UBICS.setInitWidths("36,40,36,*,32,0");
    GRID_UBICS.setColAlign("center,center,center,left,center,center");
    GRID_UBICS.setColTypes("img,ron,ch,ro,img,ro");
    GRID_UBICS.setColSorting("str,str,str,str,str,str");
    GRID_UBICS.enableResizing('false,false,false,true,false,false');
    GRID_UBICS.enableAutoWidth(true);
    GRID_UBICS.enableMultiselect(true);
    GRID_UBICS.init();

    GRID_UBICS.attachEvent("onCheck", function (rowId, col, checked) {
        if (col == 2) {
            // Mostrar / Ocultar Marcador
            var ix = GRID_UBICS.getUserData(rowId, 'markerIx');
            var tmp_map = (checked) ? GMAP1.map : null;
            if (!GMAP1.getDom("chkCapa4").checked)
                tmp_map = null;
            MARKERS_UBICS[ix]._marker.setMap(tmp_map);
            //ProcesaCapas();
        }
    });
    GRID_UBICS.attachEvent('onRowSelect', function (rowId, col) {
        //var value = GRID_UBICS.cells(rowId, col).getValue();
        //alert("R: " + rowId + " C: " + col);
        if (col == 4) {
            // Eliminar
            var ix = GRID_UBICS.getUserData(rowId, 'markerIx');

            // Eliminar del Grid
            GRID_UBICS.deleteRow(rowId);
            EliminaMarcador(ix);
            RegeneraEtiqsGridUbics();
        } else if (col == 3 || col == 1) {
            // Clic en Direccion
            var ix = GRID_UBICS.getUserData(rowId, 'markerIx');
            GMAP1.triggerEvent(MARKERS_UBICS[ix]._marker, "click");
        } else if (col == 0) {

            // Clic en ESTABLECER UBICACION COMO PRIMARIA
            var icon = GRID_UBICS.cells(rowId, 0).getValue();
            if (icon.indexOf(ICON_DOM_SEC) > -1) {
                var rowId_0 = GRID_UBICS.getRowId(0);
                if (rowId != rowId_0) {
                    // Establece dir seleccionada
                    var ix = GRID_UBICS.getUserData(rowId, 'markerIx');
                    var ix_0 = GRID_UBICS.getUserData(rowId_0, 'markerIx');
                    MARKERS_UBICS[ix]._marker.setIcon(ICONS_PATH + "1V-24.png");
                    MARKERS_UBICS[ix_0]._marker.setIcon(ICONS_PATH + "2-24.png");

                    SetDirSeleccionada(MARKERS_UBICS[ix]);

                    // Mueve renglon seleccionado abajo del primero
                    GRID_UBICS.moveRowTo(rowId, rowId_0, "move");
                    GRID_UBICS.cells(rowId, 0).setValue(ICON_DOM_PRIM);

                    // Mueve el primero una posicion abajo
                    GRID_UBICS.moveRow(rowId_0, "down");
                    GRID_UBICS.cells(rowId_0, 0).setValue(ICON_DOM_SEC);

                    RegeneraEtiqsGridUbics();
                    BuscarCodPostal2();
                }
            }
        }
    });

    // TAB2 - Historial de ubicaciones

    dataTabbar.addTab('dat_tabHist', 'Historico');
    var datTabHist = dataTabbar.cells('dat_tabHist');
    var histGrid = datTabHist.attachGrid();
    histGrid.setIconsPath(ICONS_PATH);

    histGrid.setHeader(["Column 1", "Column 2"]);
    histGrid.setColTypes("ro,ro");

    histGrid.setColSorting('str,str');
    histGrid.setInitWidths('*,*');
    histGrid.init();
    histGrid.load('dhtmlxV5/data/grid.xml', 'xml');

    var histToolbar = datTabHist.attachMenu();
    histToolbar.setIconsPath(ICONS_PATH);
    histToolbar.loadStruct('dhtmlxV5/data/menu.xml', function () { });

    // MENU CONTEXTUAL EN MAPA
    MAP_CONTEXT_MENU = new dhtmlXMenuObject({
        //parent: "mapMenu",
        icon_path: ICONS_PATH,
        //iconset: "awesome",  // sets the Font Awesome icons for menu
        context: true,
        auto_hide: false, auto_show: false,
        items: [

            { id: "MapMnu_QuehayAqui", text: "¿Que hay aqui?", img: "help.png", img_disabled: "help.png" },
            { id: "MapMnu_ubic", text: "Aqui es la ubicacion", img: "pinRojo16.png", img_disabled: "pinRojo16.png" },
            { id: "MapMnu_addAlrededor", text: "Agregar como 'alrededor'", img: "pinRojo16.png", img_disabled: "pinRojo16.png" },
            { type: "separator" },
            { id: "MapMnu_VistaPano", text: "Vista panoramica", img: "streetview.png", img_disabled: "streetview.png" },
            { id: "MapMnu_Trafico", text: "Mostrar trafico", img: "traffic.png", img_disabled: "traffic.png" },
            { id: "MapMnu_Altitud", text: "Altitud aqui", img: "upEnd.png", img_disabled: "upEnd.png" },
            { id: "MapMnu_Regla", text: "Mostrar regla", img: "ruler_triangle.png", img_disabled: "traffic.png" },
            { id: "MapMnu_Dibujar", text: "Barra Dibujar", img: "shape_handles.png", img_disabled: "traffic.png" },
            { type: "separator" },
            {
                id: "MapMnu_Ruta", text: "Ruta", img: "chart_curve.png", img_disabled: "fa fa-folder-open-o", items: [
                    { id: "MapMnu_RutaIni", text: "Iniciar aqui", img: "markerGreen.png", img_disabled: "markerGreen.png" },
                    { id: "MapMnu_RutaFin", text: "Terminar aqui", img: "marker.png", img_disabled: "marker.png" },
                    { id: "MapMnu_RutaElim", text: "Eliminar", img: "cross.png", img_disabled: "cross.png" },
                    { id: "MapMnu_RutaInst", text: "Ver instrucciones", img: "tables.png", img_disabled: "tables.png" }
                ]
            }
        ]
    });
    //MAP_CONTEXT_MENU.renderAsContextMenu();
    //MAP_CONTEXT_MENU.setIconset("awesome");
    //MAP_CONTEXT_MENU.setSkin("dhx_web");
    MAP_CONTEXT_MENU.attachEvent("onClick", function (id, zoneId, cas) {
        if (id == "MapMnu_DibujoSave")
            alert("Guardar: " + SHAPE_DATA.type + " - " + SHAPE_DATA.id + " " + SHAPE_DATA.name);
        else if (id == "MapMnu_DibujoCancel")
            SHAPE_DATA.overlay.setMap(null);
        else if (id == "MapMnu_ubic")
            aquiEsUbic(MAP_POS_CLIC, true);
        else if (id == "MapMnu_addAlrededor")
            Alrededor_CntxMenuClick(MAP_POS_CLIC, true);
        else if (id == "MapMnu_QuehayAqui") {
            //GMAP1.showInfoWin('<b>#dom#</b>', MAP_POS_CLIC);
            GMAP1.geocodePosition(MAP_POS_CLIC, function (dirs) {
                if (dirs.length > 0) {
                    GMAP1.showInfoWin('<b>' + dirs[0].dom + '</b>', MAP_POS_CLIC);
                }
            });
        }
        else if (id == "MapMnu_VistaPano")
            GMAP1.viewPanorama(MAP_POS_CLIC);
        else if (id == "MapMnu_Trafico")
            GMAP1.showTrafficLayer();
        else if (id == "MapMnu_Altitud") {
            GMAP1.getElevation(MAP_POS_CLIC, function (altitud) {
                if (altitud) {
                    var xStr = "<b>Altitud en este punto: " + altitud + " metros.</b>";
                    GMAP1.showInfoWin(xStr, MAP_POS_CLIC);
                } else {
                    GMAP1.MessageBox('Aviso', 'No se encontraron resultados.', function (btnOK) { });
                }
            });
        } else if (id == "MapMnu_Regla")
            GMAP1.addruler(MAP_POS_CLIC);
        else if (id == "MapMnu_Dibujar")
            GMAP1.showDrawTool();
        else if (id == "MapMnu_RutaIni")
            GMAP1.setRoute('I', MAP_POS_CLIC);
        else if (id == "MapMnu_RutaFin")
            GMAP1.setRoute('F', MAP_POS_CLIC);
        else if (id == "MapMnu_RutaElim")
            GMAP1.setRoute('B', MAP_POS_CLIC);
        else if (id == "MapMnu_RutaInst") {
            var xStr = '<div style="width:100%;height:100%;overflow:auto;">' +
                '<div style="padding:12px;">' + GMAP1.showRouteInstructions(); + '</div></div>';

            GENERIC_WIN1.attachHTMLString(xStr);
            GENERIC_WIN1.setModal(true);
            GENERIC_WIN1.show();
        }
    });

    // VENTANA
    WINDOWS = new dhtmlXWindows({
        image_path: "codebase/imgs/",
        skin: "dhx_skyblue"
    });

    GENERIC_WIN1 = WINDOWS.createWindow({
        id: "GENERIC_WIN1",
        width: 450, height: 300,
        //left: 20, top: 30,
        center: true,
        //move: true,
        //resize: true,
        //park: true,
        modal: false,
        caption: "Ejemplo de ventana"
        //header: true
    });
    GENERIC_WIN1.attachEvent("onClose", function (_this) {
        _this.hide();
        _this.setModal(false);
    });

    GENERIC_WIN1.close();
    GENERIC_WIN1.attachHTMLString("<b>Texto</b><p>Mas texto...</p>");

    WIN_TMP = WINDOWS.createWindow({
        id: "WIN_TMP",
        width: 450, height: 450,
        //left: 20, top: 30,
        center: true,
        //move: true,
        //resize: true,
        //park: true,
        modal: false,
        caption: "Resultados de Busqueda"
    });
    WIN_TMP.attachEvent("onClose", function (_this) {
        _this.hide();
        _this.setModal(false);
        MostrarMarkersGridTmp(false);
    });

    // Configura GRID_TMP

    GRID_TMP = WIN_TMP.attachGrid();
    GRID_TMP.setImagePath(IMAGES_PATH);
    GRID_TMP.setIconsPath(ICONS_PATH);
    GRID_TMP.setHeader("-,No,Domicilio o Lugar,Precisión,Kms.Cruces");
    GRID_TMP.setInitWidths("36,40,*,90,75");
    GRID_TMP.setColAlign("center,center,left,center,center");
    GRID_TMP.setColTypes("ch,ron,ro,ro,ro");
    GRID_TMP.setColSorting("str,int,str,str,str");
    GRID_TMP.enableResizing('false,false,true,false,false');
    GRID_TMP.enableAutoWidth(true);
    GRID_TMP.enableMultiselect(true);
    GRID_TMP.init();

    GRID_TMP.attachEvent('onRowSelect', function (rowId, col) {
        //var value = GRID_UBICS.cells(rowId, col).getValue();
        //alert("R: " + rowId + " C: " + col);
        if (col == 2) {
            // Clic en Direccion
            var ix = GRID_TMP.getUserData(rowId, 'markerIx');
            GMAP1.triggerEvent(MARKERS_TMP[ix]._marker, "click");
        }
    });

    var WIN_TMP_Toolbar = WIN_TMP.attachToolbar();
    WIN_TMP_Toolbar.setIconsPath(ICONS_PATH);

    WIN_TMP_Toolbar.addButton("tmpList_Sel", 1, "Seleccionar", "tick.png", "tick.png");
    WIN_TMP_Toolbar.addSeparator("tmpList_Sep1", 2);
    WIN_TMP_Toolbar.addButton("tmpList_Can", 3, "Cancelar", "cross.png", "cross.png");

    WIN_TMP_Toolbar.attachEvent("onClick", function (id) {
        if (id == "tmpList_Sel")
        {
            var markersSel = getItemsGRID_TMP();
            
            if (markersSel.length > 0) {
                var numberOfItems = MarkerFilterByUbicacionesOrLugares(markersSel[0]._type);

                if (markersSel.length + numberOfItems > 9) {
                    dhtmlx.alert("A excedido el limite de 9 ubicaciones, quite la selección de " + (markersSel.length + numberOfItems - 10) + " elemento(s).");
                }
                else {
                    GridTmpSeleccionados(markersSel);
                }
            }

            ZoomToFitMap();
        }
        if (id == "tmpList_Can") {
            WIN_TMP.close();
            GMAP1.map.setCenter(latitudDefault, longitudDefault);
            GMAP1.map.setZoom(zoomDefault);
        }
    });

    WIN_TMP.close();
}

function ConfiguraMapa() {
    // ----------------------------------------------------------------------
    // Armado de los datos de configuracion
    // ----------------------------------------------------------------------

    var gmConfig = {
        objName: 'GMAP1',
        mapDiv: 'gmMap1',
        center: { lat: 20.66375456, lng: -103.34701538 },
        getBrowserGPS: false,
        zoom: 12,
        infoWin: {
            width: 320,
            moreText: '<br/><br/><b>Latitud/Longitud:</b> #lat#, #lng#' +
            '<br/>' +
            '<a href="javascript:#objName#.viewPanorama([#lat#,#lng#]);"><img src="' + ICONS_PATH + 'streetView.png" alt="" style="vertical-align:bottom;" /> Panoramica</a>&nbsp;&nbsp;&nbsp;' +
            '<a href="javascript:aquiEsUbic([#lat#,#lng#], true);"><img src="' + ICONS_PATH + 'iconb24.png" alt="" style="vertical-align:bottom;" /> Aqui es la ubicación</a>&nbsp;&nbsp;&nbsp;' +
            '<a href="javascript:Alrededor_CntxMenuClick([#lat#,#lng#], true);"><img src="' + ICONS_PATH + 'iconr24.png" alt="" style="vertical-align:bottom;" /> Agregar "alrededor"</a>&nbsp;&nbsp;&nbsp;' +
            '<br/>&nbsp;',
            moreTextUseDefault: false
        },
        icons: {
            routeStart: ICONS_PATH + 'markerGreen.png',  // icono para marcador de inicio de ruta
            routeEnd: ICONS_PATH + 'marker.png',  // icono para marcador de fin de ruta
            routeStep: ICONS_PATH + 'markerBlue.png',  // icono para marcador en cada punto de paso de la ruta
            streetView: ICONS_PATH + 'streetView.png', // icono a mostrar en links de vista panoramica 
            marker: ICONS_PATH + 'marker.png', // icono para marcador generico
            rule: ICONS_PATH + 'ruler_triangle.png', // icono para marcador de regla para medir distancias
            cancel: ICONS_PATH + 'cross.png' // icono a mostrar como marcador en opcion de regla
        },
        onClick: function (gmap, gmapEvt) {
            //alert("CLICK:\nLatLng: " + gmapEvt.latLng.toUrlValue() + "\nXY: " + gmapEvt.pixel.toString());
        },
        onRightClick: function (gmap, data) {
            /*
            data = {
                type: <'circle,rectangle,polygon,polyline,map,marker'>,
                latLng: <lat,lng>,
                pixel: <x,y>,
                name: 'nombre del objeto',
                id: 'id asociado',
                overlay: <objeto dibujado>,
                centro: <lat,lng>,                  // circle,rectangle,polygon
                area: <metros cuadrados>,           // circle,rectangle,polygon
                radio: <lat,lng>,                   // circle
                noreste: <lista de <lat,lng>>,      // rectangle
                suroeste: <lista de <lat,lng>>,     // rectangle
                path: <lista de <lat,lng>>,         // polygon,polyline
                dist: <distancia en metros>,        // polyline
            };
            */
            MAP_POS_CLIC = data.latLng;
            if (",circle,rectangle,polygon,polyline,".indexOf("," + data.type + ",") > -1) {
                // Es un shape dibujado
                //MAP_CONTEXT_MENU.isItemHidden("MapMnu_Regla");
                SHAPE_DATA = data;
                MAP_CONTEXT_MENU.showItem("MapMnu_DibujoSave");
                MAP_CONTEXT_MENU.showItem("MapMnu_DibujoCancel");
                MAP_CONTEXT_MENU.showItem("MapMnu_DibujoSeparator");
            } else {
                SHAPE_DATA = null;
                MAP_CONTEXT_MENU.hideItem("MapMnu_DibujoSave");
                MAP_CONTEXT_MENU.hideItem("MapMnu_DibujoCancel");
                MAP_CONTEXT_MENU.hideItem("MapMnu_DibujoSeparator");
            }
            MAP_CONTEXT_MENU.showContextMenu(data.pixel.x - 7, data.pixel.y - 12);
        },
        onZoomChange: function (gmap, zoom) {
            //alert("ZOOM: " + zoom);
        },
        MessageBox: function (msg, tit, type, promptValue, callBakFunc) {
            dhtmlx.message(msg);
        }
    }
    // ----------------------------------------------------------------------
    // Creacion de la instancia de la clase del mapa
    // ----------------------------------------------------------------------
    GMAP1 = new GFMap(gmConfig);
    GMAP1.AddControl("divMapAddress", "TOP_LEFT");
    GMAP1.AddControl("divMapCapas", "RIGHT_TOP");
    GMAP1.AddControl("divBotones", "BOTTOM_CENTER");
    GMAP1.AddControl("btn_expand_mapa", "TOP_RIGHT");

}

function MostrarCapas() {
    if (GMAP1.getDom("tblMapCapas").style.display == "none")
        GMAP1.getDom("tblMapCapas").style.display = "";
    else
        GMAP1.getDom("tblMapCapas").style.display = "none";
}

function BuscarDomTop() {
    // CLIC EN BOTON DE BUSQUEDA EN TOP
    var dom = GMAP1.getDom("topDomicilio").value;
    GMAP1.getDom("fdomCruce1").value = GMAP1.getDom("topCruce1").value;
    GMAP1.getDom("fdomCruce2").value = GMAP1.getDom("topCruce2").value;

    BusquedaInicial(dom);
}

function BuscarDomForm() {
    // CLIC EN BOTON DE BUSQUEDA EN FORMULARIO DERECHO
    var xStr = GMAP1.getDom("fdomCalle").value + " " +
        GMAP1.getDom("fdomNumExt").value + ", " +
        GMAP1.getDom("fdomColonia").value + ", " +
        GMAP1.getDom("fdomCodPos").value + " " +
        GMAP1.getDom("fdomCiudad").value + ", " +
        GMAP1.getDom("fdomEstado").value + ", " +
        GMAP1.getDom("fdomPais").value;
    xStr = xStr.replaceAll("  ", " ");
    xStr = xStr.replaceAll(" ,", ",");
    xStr = xStr.replaceAll(",,", ",");
    xStr = xStr.trim();

    if (xStr == "") return;

    BusquedaInicial(xStr);
}

function BusquedaEnMapa() {
    var dom = GMAP1.getDom('mapTextBuscar').value;
    MARKERS_TMP = [];

    GMAP1.geocodeAddress('A', dom, null, null, false, function (dirs) {
        var elementosEncontrados = dirs.length;
        dirs = EliminaDuplicadasEnGrid(dirs);
        var elementosNoDuplicados = dirs.length;

        if (elementosEncontrados > 0 && elementosNoDuplicados == 0) {
            dhtmlx.alert({
                text: "Los elementos encontrados ya se encuentran actualmente en las busquedas agregadas.",
                callback: function () { }
            });
        }

        if (dirs.length > 0) {
            var markersTmp = [];
            // Agrega ubicaciones a Marcadores Temporal
            for (var i = 0; i < dirs.length; i++) {
                var dir = dirs[i];

                dir._id = i;
                dir._type = "D";
                dir._principal = false;

                var label = dir._type + dir._id;
                var title = dir.dom;
                var text = GMAP1.formatAddress(dir);
                var icon = ICONS_PATH + "posGreen_24.png";
                
                dir._marker = GMAP1.createMarker(dir.pos, icon, (i + 1).toString(), title, text);
                dir._marker.setMap(null);
                MARKERS_TMP.push(dir);
                markersTmp.push(dir.pos);
            }

            ZoomToFitMap(markersTmp);
            MuestraGridTmp();
        }
    });
}

function BusquedaInicial(dom) {
    Limpia_MARKERS_UBICS();
    MARKERS_TMP = [];
    GMAP1.getDom("mapTextBuscar").value = dom;

    GMAP1.geocodeAddress('A', dom, null, null, false, function (dirs) {
        if (dirs.length > 0) {
            // Agrega primer direccion a Marcadores Principal
            var dirSeleccionado = dirs[0]; // extend(dirs[0]);

            dirSeleccionado._id = 1;
            dirSeleccionado._type = "D";
            dirSeleccionado._principal = true;

            var label = dirSeleccionado._type + dirSeleccionado._id;
            var title = dirSeleccionado.dom;
            var text = GMAP1.formatAddress(dirSeleccionado);
            var icon = ICONS_PATH + "1V-24.png";

            dirSeleccionado._marker = GMAP1.createMarker(dirSeleccionado.pos, icon, "", title, text, true);
            dirSeleccionado._marker.addListener('dragend', markerdragend);

            MARKERS_UBICS.push(extend(dirSeleccionado));
            
            // Agrega ubicaciones a Marcadores Temporal
            dirs.splice(0, 1);
            for (var i = 0; i < dirs.length; i++) {
                var dir = dirs[i];

                dir._id = i;
                dir._type = "D";
                dir._principal = false;

                var label = dir._type + dir._id;
                var title = dir.dom;
                var text = GMAP1.formatAddress(dir);
                var icon = ICONS_PATH + "posGreen_24.png";

                dir._marker = GMAP1.createMarker(dir.pos, icon, (i + 1).toString(), title, text);
                dir._marker.setMap(null);
                MARKERS_TMP.push(dir);
            }

            // Inicializa datos desglosados de la primera direccion
            SetDirSeleccionada(dirSeleccionado);

            pendientes = "cp|cr1|cr2|";
            BuscarCodPostal();
            BuscarCruce(1);
            BuscarCruce(2);
        }
    });
}

function markerdragend(event) {
    for (var i in MARKERS_UBICS) {
        iconMarkerTmp = this.icon;
        
        if (MARKERS_UBICS[i]._type == "D" && MARKERS_UBICS[i].dom == this.title) {
            numItem = i;

            GMAP1.geocodeAddress('A', this.position.toUrlValue(), null, null, false, function (dirs) {
                if (dirs.length > 0) {
                    var dir = dirs[0];
                    MARKERS_UBICS[numItem].dom = dir.dom;
                    MARKERS_UBICS[numItem].pos = dir.pos;
                    MARKERS_UBICS[numItem].country = dir.country;
                    MARKERS_UBICS[numItem].countryShort = dir.countryShort;
                    MARKERS_UBICS[numItem].externalNum = dir.externalNum;
                    MARKERS_UBICS[numItem].locality = dir.locality;
                    MARKERS_UBICS[numItem].neighborhood = dir.neighborhood;
                    MARKERS_UBICS[numItem].phone = dir.phone;
                    MARKERS_UBICS[numItem].posType = dir.posType;
                    MARKERS_UBICS[numItem].postalCode = dir.postalCode;
                    MARKERS_UBICS[numItem].state = dir.state;
                    MARKERS_UBICS[numItem].stateShort = dir.stateShort;
                    MARKERS_UBICS[numItem].streetName = dir.streetName;
                    MARKERS_UBICS[numItem].sublocality = dir.sublocality;
                    MARKERS_UBICS[numItem].types = dir.types;

                    MARKERS_UBICS[numItem]._marker.setMap(null);
                    MARKERS_UBICS[numItem]._marker = GMAP1.createMarker(dir.pos, iconMarkerTmp, "", dir.dom, GMAP1.formatAddress(dir), true);
                    MARKERS_UBICS[numItem]._marker.addListener('dragend', markerdragend);
                    
                    SetDirSeleccionada(MARKERS_UBICS[numItem]);

                    if (MARKERS_UBICS[numItem].postalCode == "") {
                        for (var i in MARKERS_UBICS) {
                            if (MARKERS_UBICS[i]._type == "CP") {
                                MARKERS_UBICS[i]._marker.setMap(null);
                                MARKERS_UBICS[i]._circle.overlay.setMap(null);
                            }
                        }
                    }
                    else {
                        BuscarCodPostal2();
                    }

                    syncMarkerToGridUbicaciones();
                }
            });
        }
    }
}

function syncMarkerToGridUbicaciones() {
    var iRow = 0;
    GRID_UBICS.forEachRow(function (id) {
        for (var i in MARKERS_UBICS) {
            if (MARKERS_UBICS[i].accuracy == "street_address" && MARKERS_UBICS[i]._id == iRow + 1) {
                GRID_UBICS.cellByIndex(iRow, 3).setValue(MARKERS_UBICS[i].dom);
            }
        }
        iRow++;
    });
}

function BuscarCodPostal() {
    POS_CP = null;
    var xStr = "", codpos = GMAP1.getDom("fdomCodPos").value;
    if (codpos != "") {
        xStr = codpos + " " +
            GMAP1.getDom("fdomCiudad").value + ", " +
            GMAP1.getDom("fdomEstado").value + ", " +
            GMAP1.getDom("fdomPais").value;
    }
    xStr = xStr.replaceAll("  ", " ");
    xStr = xStr.replaceAll(" ,", ",");
    xStr = xStr.replaceAll(",,", ",");
    xStr = xStr.trim();

    if (xStr == "") {
        // NO HAY CODIGO POSTAL
        pendientes = pendientes.replaceAll("cp|", "");
        if (pendientes == "")
            BusquedaInicial_OK();
        return;
    }
    
    var params = {
        'pais': GMAP1.getDom("fdomPais").value,
        'estado': GMAP1.getDom("fdomEstado").value,
        'colonia': GMAP1.getDom("fdomColonia").value,
        'municipio': GMAP1.getDom("fdomCiudad").value,
        'codigopostal': GMAP1.getDom("fdomCodPos").value
    };
    
    loadCodigoPostal(params, "GetByCodigoPostal", function (data) {
        data = window.dhx.s2j(data.xmlDoc.responseText);

        var PolygonPositions = data.cpShapePositions.trim().split(" ");
        
        for (var i in PolygonPositions) {
            var pointPosition = PolygonPositions[i].split(",");
            PolygonPositions[i] = {};
            
            PolygonPositions[i].lat = parseFloat(pointPosition[0]);
            PolygonPositions[i].lng = parseFloat(pointPosition[1]);
        }
        
        if (data != null) {
            var dir = {};

            dir._type = "CP";
            dir._principal = false;
            dir.pos =  {
                    "lat": function () {
                        return data.cpLatitud;
                    },
                    "lng": function () {
                        return data.cpLongitud;
                    }
                }

            POS_CP = dir.pos;
            
            var label = dir._type + " " + codpos;
            var title = label;
            var text = label;
            var icon = ICONS_PATH + "cpv_24.png";
            
            dir._marker = GMAP1.createMarker(dir.pos, icon, "", title, text);
            dir._circle = GMAP1.CreateCircle(dir.pos, 1000, "#FFCC00", 0.33, '1', 'CP', false);
            dir._polygon = GMAP1.CreatePolygon(PolygonPositions, "#a800ff", 0.33, '1000', 'Polygon', false);
            
            MARKERS_UBICS.push(extend(dir));
        }

        pendientes = pendientes.replaceAll("cp|", "");
        if (pendientes == "")
            BusquedaInicial_OK();
    });
}

function BuscarCodPostal2() {
    POS_CP = null;
    var xStr = "", codpos = GMAP1.getDom("fdomCodPos").value;
    if (codpos != "") {
        xStr = codpos + " " +
            GMAP1.getDom("fdomCiudad").value + ", " +
            GMAP1.getDom("fdomEstado").value + ", " +
            GMAP1.getDom("fdomPais").value;
    }
    xStr = xStr.replaceAll("  ", " ");
    xStr = xStr.replaceAll(" ,", ",");
    xStr = xStr.replaceAll(",,", ",");
    xStr = xStr.trim();

    if (xStr == "") {

        return;
    }

    var params = {
        'codigopostal': GMAP1.getDom("fdomCodPos").value
    };

    loadCodigoPostal(params, "GetByCodigoPostal", function (data) {
        data = window.dhx.s2j(data.xmlDoc.responseText);

        if (data == null) {
            debugger;
        }
        var PolygonPositions = data.cpShapePositions.trim().split(" ");

        for (var i in PolygonPositions) {
            var pointPosition = PolygonPositions[i].split(",");
            PolygonPositions[i] = {};

            PolygonPositions[i].lat = parseFloat(pointPosition[0]);
            PolygonPositions[i].lng = parseFloat(pointPosition[1]);
        }
        
        if (data != null) {
            var dir = data;

            dir._id = ++ID_UBIC;
            dir._type = "CP";
            dir._principal = false;
            dir.pos = {
                "lat": function () {
                    return data.cpLatitud;
                },
                "lng": function () {
                    return data.cpLongitud;
                }
            }

            POS_CP = dir.pos;

            var label = dir._type + " " + codpos;
            var title = label;
            var text = label;
            var icon = ICONS_PATH + "cpv_24.png";

            var existMarker = false;

            for (var i in MARKERS_UBICS) {
                if (MARKERS_UBICS[i]._type == "CP") {
                    MARKERS_UBICS[i]._marker.setMap(null);
                    MARKERS_UBICS[i]._circle.overlay.setMap(null);
                    MARKERS_UBICS[i]._polygon.overlay.setMap(null);

                    MARKERS_UBICS[i]._marker = GMAP1.createMarker(dir.pos, icon, "", title, text);
                    MARKERS_UBICS[i]._circle = GMAP1.CreateCircle(dir.pos, 1000, "#FFCC00", 0.33, '1', 'CP', false);
                    MARKERS_UBICS[i]._polygon = GMAP1.CreatePolygon(PolygonPositions, "#a800ff", 0.33, '1000', 'Polygon', false);


                    MARKERS_UBICS[i]._marker.setPosition(dir.pos);
                    MARKERS_UBICS[i]._circle.overlay.setCenter(dir.pos);
                    MARKERS_UBICS[i]._circle.overlay.setRadius(parseInt(document.getElementById("fdomRadio").value));
                    MARKERS_UBICS[i].pos = dir.pos;

                    existMarker = true;
                    break;
                }
            }

            if (!existMarker) {
                dir._marker = GMAP1.createMarker(dir.pos, icon, "", title, text);
                dir._circle = GMAP1.CreateCircle(dir.pos, 1000, "#FFCC00", 0.33, '1', 'CP', false);
                dir._polygon = GMAP1.CreatePolygon(PolygonPositions, "#a800ff", 0.33, '1000', 'Polygon', false);

                MARKERS_UBICS.push(extend(dir));
            }

            //GMAP1.CreateCircle
            //Buscar si ya existe registo que tenga Type CP y si ya existe remplazarlo por dir, si no existe se agrega

        }
    });
}

function BuscarCruce(numCruce) {
    var xStr = "";
    var calle = GMAP1.getDom("fdomCalle").value;
    var cruce = (numCruce == 1) ? GMAP1.getDom("fdomCruce1").value : GMAP1.getDom("fdomCruce2").value;

    if (numCruce == 1)
        POS_CR1 = null;
    else
        POS_CR2 = null;

    if (calle != "" && cruce != "") {
        xStr = calle + " & " + cruce + ", " +
            //GMAP1.getDom("fdomColonia").value + ", " +
            GMAP1.getDom("fdomCiudad").value + ", " +
            GMAP1.getDom("fdomEstado").value + ", " +
            GMAP1.getDom("fdomPais").value;
    }
    xStr = xStr.replaceAll("  ", " ");
    xStr = xStr.replaceAll(" ,", ",");
    xStr = xStr.replaceAll(",,", ",");
    xStr = xStr.trim();

    if (xStr == "") {
        // NO HAY CRUCE
        numCruce = (numCruce == 1) ? "cr1|" : "cr2|";
        pendientes = pendientes.replaceAll(numCruce, "");
        if (pendientes == "")
            BusquedaInicial_OK();
        return;
    }

    GMAP1.geocodeAddress('A', xStr, null, null, false, function (dirs) {
        if (dirs.length > 0) {
            var dir = dirs[0];

            dir._id = ++ID_UBIC;
            dir._type = "CR" + numCruce;
            dir._principal = false;

            if (numCruce == 1)
                POS_CR1 = dir.pos;
            else
                POS_CR2 = dir.pos;

            var label = dir._type;
            var title = dir.dom;
            var text = GMAP1.formatAddress(dir);
            var icon = (numCruce == 1) ? ICONS_PATH + "cruce1_24.png" : ICONS_PATH + "cruce2_24.png";
            
            dir._marker = GMAP1.createMarker(dir.pos, icon, "Cruce " + numCruce, title, text);

            MARKERS_UBICS.push(extend(dir));
        }

        numCruce = (numCruce == 1) ? "cr1|" : "cr2|";
        pendientes = pendientes.replaceAll(numCruce, "");
        if (pendientes == "")
            BusquedaInicial_OK();
    });
}

function BusquedaInicial_OK() {
    if (MARKERS_UBICS.length == 0)
        return;
    ZoomToFitInicial();

    // Agrega primer marcador a Grid Principal
    var marker = MARKERS_UBICS[0];
    SetDirSeleccionada(marker);
    var id = GRID_UBICS.uid();
    GRID_UBICS.addRow(id, [ICON_DOM_PRIM, marker._type + marker._id, 1, marker.dom, "cross.png", ""]);
    GRID_UBICS.setUserData(id, 'markerIx', 0);

    MuestraGridTmp();
}

function SetDirSeleccionada(dirSel) {
    GMAP1.getDom("clnDirMapa").innerHTML = dirSel.dom;
    GMAP1.getDom("clnLatMapa").innerHTML = dirSel.pos.lat();
    GMAP1.getDom("clnLngMapa").innerHTML = dirSel.pos.lng();
    GMAP1.getDom("clnCruce1Mapa").innerHTML = GMAP1.getDom("fdomCruce1").value;
    GMAP1.getDom("clnCruce2Mapa").innerHTML = GMAP1.getDom("fdomCruce2").value;

    GMAP1.getDom("fdomCalle").value = dirSel.streetName;
    GMAP1.getDom("fdomNumExt").value = dirSel.externalNum;
    GMAP1.getDom("fdomNumInt").value = "";
    GMAP1.getDom("fdomColonia").value = dirSel.neighborhood;
    GMAP1.getDom("fdomCodPos").value = dirSel.postalCode;
    GMAP1.getDom("fdomCiudad").value = dirSel.locality;
    GMAP1.getDom("fdomEstado").value = dirSel.state;
}

function ZoomToFitInicial() {
    var arrPos = [];
    for (var i = 0; i < MARKERS_UBICS.length; i++) {
        arrPos.push(MARKERS_UBICS[i].pos);
        if (MARKERS_UBICS[i]._type == "CP") {
            var bound = MARKERS_UBICS[i]._circle.overlay.getBounds();
            arrPos.push(bound.getNorthEast());
            arrPos.push(bound.getSouthWest());
        }
    }
    for (var i = 0; i < MARKERS_TMP.length; i++) {
        arrPos.push(MARKERS_TMP[i].pos);
        if (MARKERS_TMP[i]._type == "CP") {
            var bound = MARKERS_TMP[i]._circle.overlay.getBounds();
            arrPos.push(bound.getNorthEast());
            arrPos.push(bound.getSouthWest());
        }
    }
    GMAP1.ZoomToFit(arrPos);
}

function MuestraGridTmp() {
    var rowsGrid_Tmp = [];
    // Agrega Marcadores temporales a Grid Temporal
    GRID_TMP.clearAll();
    var arrPos = [];

    MARKERS_TMP.sort(function (a, b) {
        return accuracyToPorcentajeInteger(a.posType) < accuracyToPorcentajeInteger(b.posType);
    });

    var MARKERS_TMP_10Items = [];

    for (var i in MARKERS_TMP) {
        if (MARKERS_TMP_10Items.length < 10) {
            MARKERS_TMP_10Items.push(MARKERS_TMP[i]);
        }
    }
    MARKERS_TMP = MARKERS_TMP_10Items;

    var posGrid = 1;
    for (var i = 0; i < MARKERS_TMP.length; i++) {
        var dir = MARKERS_TMP[i];

        // Checa que esta ubicacion no exista en Grid de Ubicaciones seleccionadas
        var Existe = false;
        var domTmp = dir.dom + dir.name;

        for (var j = 0; j < MARKERS_UBICS.length; j++) {
            var domUbic = MARKERS_UBICS[j].dom + MARKERS_UBICS[j].name;
            if (domUbic == domTmp) {
                Existe = true;
                break;
            }
        }
        if (!Existe) {
            var id = GRID_TMP.uid();

            var dist1 = GMAP1.distance(dir.pos, POS_CR1, "K");
            var dist2 = GMAP1.distance(dir.pos, POS_CR2, "K");

            dist1 = dist1 + dist2;

            GRID_TMP.addRow(id, [0, posGrid, dir.dom, accuracyToPorcentaje(dir.posType), dist1]);
            GRID_TMP.setUserData(id, 'markerIx', i)

            dir._marker.setMap(GMAP1.map);

            posGrid++;
            arrPos.push(dir.pos);
            if (dir._type == "CP") {
                var bound = dir._circle.overlay.getBounds();
                arrPos.push(bound.getNorthEast());
                arrPos.push(bound.getSouthWest());
            }
        }
    }
    if (arrPos.length > 0) {
        WIN_TMP_SHOW();
    }
}

function BuscarLugares() {
    var buscar = GMAP1.getDom("fdomLugBuscar").value;
    var radio = GMAP1.getDom("fdomRadio").value;
    var tipoCentro = GMAP1.getDom("fdomTpoCerca_D1").checked ? "D1" : "CP";
    var centro = null;

    // Busca centro
    for (var i = 0; i < MARKERS_UBICS.length; i++) {
        if (MARKERS_UBICS[i]._type == tipoCentro) {
            centro = MARKERS_UBICS[i].pos;
            break;
        }
    }

    // Elimina marcadores temporales previos
    MARKERS_TMP = [];
    GRID_TMP.clearAll();

    GMAP1.geocodeAddress('P', buscar, centro, radio, false, function (dirs) {
        dirs = EliminaDuplicadasEnGrid(dirs);
        for (var i = 0; i < dirs.length; i++) {
            if (dirs[i].isPlace) {
                var dir = dirs[i];

                dir._id = i + 1;
                dir._type = "A";

                var label = dir._type + dir._id;
                var title = dir.name;
                var text = GMAP1.formatAddress(dir);
                var icon = ICONS_PATH + "icong.png";
                
                dir._marker = GMAP1.createMarker(dir.pos, icon, "", title, text);

                MARKERS_TMP.push(dir);

                var id = GRID_TMP.uid();
                GRID_TMP.addRow(id, [0, i + 1, dir.name, '']);
                GRID_TMP.setUserData(id, 'markerIx', MARKERS_TMP.length - 1)
            }
        }

        WIN_TMP_SHOW();
    });
}

function WIN_TMP_SHOW() {
    latitudDefault = GMAP1.map.getCenter().lat();
    longitudDefault = GMAP1.map.getCenter().lng();
    zoomDefault = GMAP1.map.getZoom();

    WIN_TMP.setModal(true);
    WIN_TMP.show();
}

function EliminaDuplicadasEnGrid(dirs) {
    for (var i = 0; i < dirs.length; i++) {
        var dirDomName = dirs[i].dom + dirs[i].name;
        var n = GRID_UBICS.getRowsNum();
        for (var j = 0; j < n; j++) {
            var rId = GRID_UBICS.getRowId(j);
            var ix = GRID_UBICS.getUserData(rId, 'markerIx');
            var markerDomName = MARKERS_UBICS[ix].dom + MARKERS_UBICS[ix].name;
            if (dirDomName == markerDomName) {
                dirs.splice(i, 1);
                i = i - 1;
            }
        }
    }
    return dirs;
}

function getItemsGRID_TMP() {
    var ids = GRID_TMP.getCheckedRows(0);
    var arrIDS = (ids == "") ? [] : ids.split(',');
    var markersSel = [];

    for (var i = 0; i < arrIDS.length; i++) {
        var ix = GRID_TMP.getUserData(arrIDS[i], 'markerIx');
        markersSel.push(MARKERS_TMP[ix]);
    }

    return markersSel;
}

function GridTmpSeleccionados(markersSel) {
    WIN_TMP.close();

    // Agrega marcadores temporales seleccionados al Grid de Ubicaciones

    for (var i = 0; i < markersSel.length; i++) {
        var dir = markersSel[i];

        //dir._id = (dir._type == "A") ? ++ID_LUGAR : ++ID_UBIC;
        dir._principal = false;

        var label = dir._type + dir._id;
        var title = ((dir.name == "") ? dir.dom : dir.name);
        var text = GMAP1.formatAddress(dir);
        var icon = (dir._type == "A") ? ICONS_PATH + "posRed_24.png" : ICONS_PATH + "2-24.png";
        
        dir._marker = GMAP1.createMarker(dir.pos, icon, "", title, text, true);
        dir._marker.addListener('dragend', function (event) {
            var idMarker = this.icon.replace("../images/icons/", "").replace("-24.png", "");
            for (var i in MARKERS_UBICS) {
                iconMarkerTmp = this.icon;

                if (MARKERS_UBICS[i]._type == "D" && MARKERS_UBICS[i]._id == idMarker) {
                    numItem = i;
                    
                    GMAP1.geocodeAddress('A', this.position.toUrlValue(), null, null, false, function (dirs) {
                        if (dirs.length > 0) {
                            var dir = dirs[0];
                            MARKERS_UBICS[numItem].dom = dir.dom;
                            MARKERS_UBICS[numItem].pos = dir.pos;
                            MARKERS_UBICS[numItem].country = dir.country;
                            MARKERS_UBICS[numItem].countryShort = dir.countryShort;
                            MARKERS_UBICS[numItem].externalNum = dir.externalNum;
                            MARKERS_UBICS[numItem].locality = dir.locality;
                            MARKERS_UBICS[numItem].neighborhood = dir.neighborhood;
                            MARKERS_UBICS[numItem].phone = dir.phone;
                            MARKERS_UBICS[numItem].posType = dir.posType;
                            MARKERS_UBICS[numItem].postalCode = dir.postalCode;
                            MARKERS_UBICS[numItem].state = dir.state;
                            MARKERS_UBICS[numItem].stateShort = dir.stateShort;
                            MARKERS_UBICS[numItem].streetName = dir.streetName;
                            MARKERS_UBICS[numItem].sublocality = dir.sublocality;
                            MARKERS_UBICS[numItem].types = dir.types;

                            MARKERS_UBICS[numItem]._marker.setMap(null);
                            MARKERS_UBICS[numItem]._marker = GMAP1.createMarker(dir.pos, iconMarkerTmp, "", dir.dom, GMAP1.formatAddress(dir), true);
                            MARKERS_UBICS[numItem]._marker.addListener('dragend', markerdragend);

                            syncMarkerToGridUbicaciones();
                        }
                    });
                }
            }
        });

        MARKERS_UBICS.push(extend(dir));

        var id = GRID_UBICS.uid();
        var dom = (dir.name == "") ? dir.dom : dir.name + " - " + dir.dom;
        // [DomPrimario, NumUbic , VerUbic, Nombre Ubic, Eliminar]
        var domPrim = (dir._type == "D") ? ICON_DOM_SEC : "pixel.gif";

        GRID_UBICS.addRow(id, [domPrim, label, 1, dom, "cross.png", ""]);
        GRID_UBICS.setUserData(id, 'markerIx', MARKERS_UBICS.length - 1);
    }
    RegeneraEtiqsGridUbics();
}

function RegeneraEtiqsGridUbics() {
    var iDom = 0, iAlr = 0;
    var n = GRID_UBICS.getRowsNum();

    // ORDENAR POR  1 DOM / 2 LUG    1 PRIM / 2 NO PRIM    IX
    for (var i = 0; i < n; i++) {
        var rId = GRID_UBICS.getRowId(i);
        var ix = GRID_UBICS.getUserData(rId, 'markerIx');
        var dir = MARKERS_UBICS[ix];
        var xStr = (dir._type == "D") ? "1|" : "2|";
        xStr += (GRID_UBICS.cells(rId, 0).getValue() == ICON_DOM_PRIM) ? "1|" : "2|";
        ix = '00' + i;
        xStr += ix.substr(ix.length - 3);

        GRID_UBICS.cells(rId, 5).setValue(xStr);
    }
    GRID_UBICS.sortRows(5, "str", "asc");

    for (var i = 0; i < n; i++) {
        var rId = GRID_UBICS.getRowId(i);
        var ix = GRID_UBICS.getUserData(rId, 'markerIx');
        var dir = MARKERS_UBICS[ix];

        var arrSort = GRID_UBICS.cells(rId, 5).getValue().split('|');

        dir._id = (dir._type == "D") ? ++iDom : ++iAlr;

        var label = dir._type + dir._id;
        var title = label + " - " + dir.dom;
        var text = "<b>" + label + "</b><br/>" + GMAP1.formatAddress(dir);
        var icon = "";
        if (dir._type == "D") {
            if (iDom == 1)
                icon = "1V-24.png";
            else
                icon = iDom + "-24.png";
        } else {
            icon = "a" + iAlr + "-24.png";
        }
        MARKERS_UBICS[ix]._marker.setIcon(ICONS_PATH + icon);

        // Reasigna Valor en Grid
        GRID_UBICS.cells(rId, 1).setValue(label);
    }
}

// extends 'from' object with members from 'to'. 
// If 'to' is null, a deep clone of 'from' is returned
function extend(from, to) {
    if (from == null || typeof from != "object") return from;
    if (from.constructor != Object && from.constructor != Array) return from;
    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
        return new from.constructor(from);

    to = to || new from.constructor();

    for (var name in from) {
        to[name] = typeof to[name] == "undefined" ? extend(from[name], null) : to[name];
    }

    return to;
}

function MostrarMarkersGridTmp(show) {
    GRID_TMP.forEachRow(function (id) {
        var ix = GRID_TMP.getUserData(id, 'markerIx');
        MARKERS_TMP[ix]._marker.setMap((show) ? GMAP1.map : null);
    });

    for (var i in MARKERS_UBICS) {
        if (MARKERS_UBICS[i]._type.indexOf("CR") >= 0) {
            MARKERS_UBICS[i]._marker.setMap(null);
            MARKERS_UBICS[i]._marker = GMAP1.createMarker(MARKERS_UBICS[i]._marker.position, MARKERS_UBICS[i]._marker.icon, "", MARKERS_UBICS[i].dom, GMAP1.formatAddress(MARKERS_UBICS[i]));
        }
    }
}

function Limpia_MARKERS_UBICS() {
    for (var i = 0; i < MARKERS_UBICS.length; i++) {
        MARKERS_UBICS[i]._marker.setMap(null);
        if (MARKERS_UBICS[i]._type == "CP")
            MARKERS_UBICS[i]._circle.overlay.setMap(null);
    }
    MARKERS_UBICS = [];
    GRID_UBICS.clearAll();
    ID_UBIC = 0;
}

function EliminaMarcador(ix) {
    // Eliminar Marcador en Mapa
    MARKERS_UBICS[ix]._marker.setMap(null);

    // Eliminar Marcador en arreglo
    MARKERS_UBICS.splice(ix, 1);

    // Regenera indice a Marcadores en Grid
    var n = GRID_UBICS.getRowsNum();
    for (var i = 0; i < n; i++) {
        var rId = GRID_UBICS.getRowId(i);
        var markerIx = GRID_UBICS.getUserData(rId, 'markerIx');
        if (markerIx > ix)
            GRID_UBICS.setUserData(rId, "markerIx", markerIx - 1);
    }
}

function sendInformation() {
    var clnNum = GMAP1.getDom("clnNum").innerHTML;
    var clnUbicacionId = GMAP1.getDom("clnUbicacionId").innerHTML;
    var clnDirMapa = GMAP1.getDom("clnDirMapa").innerHTML;
    var clnLatMapa = GMAP1.getDom("clnLatMapa").innerHTML;
    var clnLngMapa = GMAP1.getDom("clnLngMapa").innerHTML;

    var textConfirm = "<div style='display: block; text-align: left;'>" +
        "<table>" +
        "<tr>" +
        "<td><b>Cliente ID:</b> " + clnNum + "</td>" +
        "<td><b>Ubicación ID:</b> " + clnUbicacionId + " </td>" +
        "</tr>" +
        "<tr><td colspan='2'>&nbsp;</td></tr>" +
        "<tr>" +
        "<td colspan='2'><b>Dirección:</b> " + clnDirMapa + " </td>" +
        "</tr>" +
        "<tr><td colspan='2'>&nbsp;</td></tr>" +
        "<tr>" +
        "<td><b>Latitud:</b> " + parseFloat(clnLatMapa).toFixed(6) + "</td>" +
        "<td><b>Longitud:</b> " + parseFloat(clnLngMapa).toFixed(6) + " </td>" +
        "</tr>" +
        "<tr><td colspan='2'>&nbsp;</td></tr>" +
        "</table>" +
        "</div>";
    var message = dhtmlx.confirm({
        type: "confirm",
        title: "Enviar información",
        text: textConfirm,
        width: "400px",
        ok: "Aceptar",
        cancel: "Cancelar",
        callback: function (isOK) {
            if (isOK) {
                window.dhx.ajax.post(urlWebService, "clienteid=" + clnNum + "&ubicacionid=" +
                    clnUbicacionId + "&direccion=" + clnDirMapa + "&latitud=" + clnLatMapa + "&longitud=" +
                    clnLngMapa, function (data) { }
                );
            }
        }
    });
}

function changeConfiguration() {
    var textConfirm = "Uri servidor: " +
        "<input type='text' id='txtUriServer' class='dhtmlx_popup_input' style='width:345px;' /><br/><br/>" +
        "<div class='dhtmlx_popup_controls'>" +
        "<div class='dhtmlx_popup_button' onclick='changeConfigurationEnviar();' result='true'>" +
        "<div>Aceptar</div>" +
        "</div>" +
        "<div class='dhtmlx_popup_button' onclick='changeConfigurationCancelar();' result='false'>" +
        "<div>Cancelar</div>" +
        "</div>" +
        "</div>";
    var message = dhtmlx.modalbox({
        type: "confirm",
        title: "URL Para envio de domicilio encontrado",
        text: textConfirm,
        width: "500px"
    });

    GMAP1.getDom("txtUriServer").value = urlWebService;
}

function changeConfigurationEnviar() {
    urlWebService = GMAP1.getDom("txtUriServer").value;
    GMAP1.getDom('txtUriServer').value = " ";
}

function changeConfigurationCancelar() {
    GMAP1.getDom('txtUriServer').value = " ";
}

function resizeMap() {

    if (layDatos.isCollapsed() || layMain_Top.isCollapsed()) {
        layDatos.expand();
        layMain_Top.expand();
    } else {
        layDatos.collapse();
        layMain_Top.collapse();
    }
    /*
    var widthOtherPanels = 5;
    var layMain_TopMinHeight = 30;
    var layDatosMinHeight = 40;

    if (layDatos.getWidth() != layDatosMinHeight && layMain_Top.getHeight() != layMain_TopMinHeight) {
        layDatosWidth = layDatos.getWidth();
        layMain_TopHeight = layMain_Top.getHeight();

        layDatos.setWidth(layDatosMinHeight);
        layMain_Top.setHeight(layMain_TopMinHeight);
    }
    else {
        layDatos.setWidth(layDatosWidth);
        layMain_Top.setHeight(layMain_TopHeight);
    }
    */
    GMAP1.resize();
}

function changeRadioCodPostal(radio) {
    for (var i in MARKERS_UBICS) {
        if (MARKERS_UBICS[i]._type == "CP") {
            MARKERS_UBICS[i]._circle.overlay.setRadius(radio);
            break;
        }
    }
}

function accuracyToPorcentaje(acc) {
    if (acc == "ROOFTOP" || acc == "convenience_store") {
        return "100%";
    } else if (acc == "RANGE_INTERPOLATED") {
        return "85%";
    } else if (acc == "GEOMETRIC_CENTER") {
        return "65%";
    } else if (acc == "ROOFTOP") {
        return "45%";
    } else if (acc == "APPROXIMATE") {
        return "25%";
    } else {
        return "10%";
    }
}

function accuracyToPorcentajeInteger(acc) {
    if (acc == "ROOFTOP") {
        return 100;
    } else if (acc == "RANGE_INTERPOLATED") {
        return 85;
    } else if (acc == "GEOMETRIC_CENTER") {
        return 65;
    } else if (acc == "ROOFTOP") {
        return 45;
    } else if (acc == "APPROXIMATE") {
        return 25;
    } else {
        return 10;
    }
}

function MarkerFilterByUbicaciones() {
    var numberOfItems = 0;
    for (var i in MARKERS_UBICS) {
        if (MARKERS_UBICS[i]._type == "D") {
            numberOfItems++;
        }
    }

    return numberOfItems;
}

function MarkerFilterByLugares() {
    var numberOfItems = 0;
    for (var i in MARKERS_UBICS) {
        if (MARKERS_UBICS[i]._type == "A") {
            numberOfItems++;
        }
    }

    return numberOfItems;
}

function aquiEsUbic(pos) {
    pos = GMAP1.parsePos(pos);

    GMAP1.geocodePosition(pos, function (dirs) {
        if (dirs.length > 0) {
            var p = pos;

            //
            var dir = dirs[0];

            //dir._id = (dir._type == "A") ? ++ID_LUGAR : ++ID_UBIC;
            dir._id = MARKERS_UBICS.length;
            dir._type = "D";

            dir._principal = true;

            var label = dir._type + dir._id;
            var title = ((dir.name == "") ? dir.dom : dir.name);
            var text = GMAP1.formatAddress(dir);
            var icon = ICONS_PATH + "1V-24.png";

            dir._marker = GMAP1.createMarker(dir.pos, icon, "", title, text);

            MARKERS_UBICS.push(extend(dir));
            var marker = MARKERS_UBICS[MARKERS_UBICS.length - 1];
            //SetDirSeleccionada(marker );
            var id = GRID_UBICS.uid();
            // [DomPrimario, NumUbic , VerUbic, Nombre Ubic, Eliminar]
            GRID_UBICS.addRow(id, [ICON_DOM_PRIM, marker._type + marker._id, 1, "click: " + marker.dom, "cross.png", ""]);
            GRID_UBICS.setUserData(id, 'markerIx', MARKERS_UBICS.length - 1);

            //---------
            var rowId = GRID_UBICS.getRowId(GRID_UBICS.getRowsNum() - 1);
            var rowId_0 = GRID_UBICS.getRowId(0);
            // Establece dir seleccionada
            var ix = MARKERS_UBICS.length - 1;
            var ix_0 = GRID_UBICS.getUserData(rowId_0, 'markerIx');
            MARKERS_UBICS[ix]._marker.setIcon(ICONS_PATH + "1V-24.png");
            MARKERS_UBICS[ix_0]._marker.setIcon(ICONS_PATH + "2-24.png");

            SetDirSeleccionada(marker);

            // Mueve renglon seleccionado abajo del primero
            GRID_UBICS.moveRowTo(rowId, rowId_0, "move");
            GRID_UBICS.cells(rowId, 0).setValue(ICON_DOM_PRIM);

            // Mueve el primero una posicion abajo
            GRID_UBICS.moveRow(rowId_0, "down");
            GRID_UBICS.cells(rowId_0, 0).setValue(ICON_DOM_SEC);
            //---------

            RegeneraEtiqsGridUbics();
            BuscarCodPostal2();
            //

            dhtmlx.message({
                title: "AVISO",
                type: "alert",
                text: 'Ubicación establecida exitosamente: ' + dirs[0].dom,
                callback: null
            });

        } else {
            dhtmlx.alert({
                text: "No se encontraron resultados de la búsqueda en este punto.",
                callback: null
            });
        }
    });

}

function Alrededor_CntxMenuClick(pos) {
    _POS_ALREDEDOR = pos;
    var textConfirm = "" +
        "<input type='text' id='txtAlrededorAlias' placeholder='Ingrese un alias para este lugar' class='dhtmlx_popup_input' style='width:345px;' /><br/><br/>" +
        "<div class='dhtmlx_popup_controls'>" +
        "<div class='dhtmlx_popup_button' onclick='Alrededor_Add_Ok(_POS_ALREDEDOR );' result='true'>" +
        "<div>Aceptar</div>" +
        "</div>" +
        "<div class='dhtmlx_popup_button' onclick='Alrededor_Add_Cancelar();' result='false'>" +
        "<div>Cancelar</div>" +
        "</div>" +
        "</div>";

    var message = dhtmlx.modalbox({
        type: "confirm",
        title: "Alias",
        text: textConfirm,
        width: "500px"
    });

    GMAP1.getDom("txtAlrededorAlias").focus();
}

function Alrededor_Add_Cancelar() {
    GMAP1.getDom('txtAlrededorAlias').value = "";
}

function Alrededor_Add_Ok(pos) {
    var Alias = GMAP1.getDom("txtAlrededorAlias").value;
    GMAP1.getDom('txtAlrededorAlias').value = " ";
    Alias = (Alias == "" ? "Alrededor: " : Alias + ": ")

    GMAP1.geocodePosition(pos, function (dirs) {
        if (dirs.length > 0) {
            var p = pos;
            var dir = dirs[0];

            dir.isPlace = true;
            dir._id = MARKERS_UBICS.length;
            dir._type = "A";

            dir._principal = false;

            var label = dir._type + dir._id;
            var title = ((dir.name == "") ? dir.dom : dir.name);
            var text = Alias + GMAP1.formatAddress(dir);
            var icon = ICONS_PATH + "posRed_24.png";

            dir._marker = GMAP1.createMarker(dir.pos, icon, "", title, text);

            MARKERS_UBICS.push(extend(dir));
            var marker = MARKERS_UBICS[MARKERS_UBICS.length - 1];

            var id = GRID_UBICS.uid();
            // [DomPrimario, NumUbic , VerUbic, Nombre Ubic, Eliminar]
            GRID_UBICS.addRow(id, ["pixel.gif", marker._type + marker._id, 1, Alias + marker.dom, "cross.png", ""]);
            GRID_UBICS.setUserData(id, 'markerIx', MARKERS_UBICS.length - 1);

            RegeneraEtiqsGridUbics();
            BuscarCodPostal2();

            dhtmlx.message({
                title: "AVISO",
                text: 'Ubicacion como "alrededor" agregado exitosamente: ' + dir.dom,
                expire: 3000,
                type: "customCss" // 'customCss' - css class
            });

        } else {
            dhtmlx.alert({
                text: "No se encontraron resultados en este punto.",
                callback: null
            });
        }
    });

}

function ProcesaCapas() {
    //D, CP, CR1, CR2, A

    var capasSel = [];

    if (GMAP1.getDom("chkCapa1").checked)
        capasSel.push("CP");
    if (GMAP1.getDom("chkCapa2").checked)
        capasSel.push("CR1");
    if (GMAP1.getDom("chkCapa3").checked)
        capasSel.push("CR2");
    if (GMAP1.getDom("chkCapa4").checked)
        capasSel.push("A");

    for (var x = 0; x < MARKERS_UBICS.length; x++) {
        if (MARKERS_UBICS[x]._type != "D")
            MARKERS_UBICS[x]._marker.setMap(null);
        if (MARKERS_UBICS[x]._type == "CP") {
            MARKERS_UBICS[x]._circle.overlay.setMap(null);
        }
    }

    for (var i = 0; i < capasSel.length; i++) {
        for (var x = 0; x < MARKERS_UBICS.length; x++) {
            if (capasSel.indexOf(MARKERS_UBICS[x]._type) > -1) {
                MARKERS_UBICS[x]._marker.setMap(GMAP1.map);
                if (MARKERS_UBICS[x]._type == "CP") {
                    MARKERS_UBICS[x]._circle.overlay.setMap(GMAP1.map);
                }
            }
        }
    }
}

function Sugerencias(type) {
    if (type == 1) {
        dhtmlx.message({
            title: "Sugerencia de como buscar objetos.",
            type: "alert",
            width: '50%',
            text: 'Presione "click derecho" en el mapa y seleccione "¿Qué hay aquí?"...<br/>'
            + 'A continuación escoja la opción "Aquí es la ubicación" para poder verificar el lugar '
            + 'o escoja directamente la misma opción desde ese menú.'
        });
    }
}

function MarkerFilterByUbicacionesOrLugares(type) {
    if (type == "D") {
        return MarkerFilterByUbicaciones();
    }

    if (type == "A") {
        return MarkerFilterByLugares();
    }
}

function ZoomToFitMap(markersTmp) {
    var bounds = [];

    if (markersTmp != undefined) {
        for (var i in markersTmp) {
            bounds.push(markersTmp[i]);
        }
    }

    for (var i in MARKERS_UBICS) {
        bounds.push(MARKERS_UBICS[i]._marker.position);
    }
    
    GMAP1.ZoomToFit(bounds); // map should be your map class
}

function clearBuscador() {
    var calle = (document.getElementById("fdomCalle").value == "" ? "" : document.getElementById("fdomCalle").value).concat(document.getElementById("fdomNumExt").value == "" && document.getElementById("fdomCalle").value != "" ? ", " : " ");
    var numExt = document.getElementById("fdomNumExt").value == "" ? "" : document.getElementById("fdomNumExt").value + ", ";
    var colonia = document.getElementById("fdomColonia").value == "" ? "" : document.getElementById("fdomColonia").value + ", ";
    var cp = (document.getElementById("fdomCodPos").value == "" ? "" : document.getElementById("fdomCodPos").value + ", ").concat(document.getElementById("fdomCiudad").value == "" && document.getElementById("fdomCodPos").value != "" ? ", " : " ");
    var municipio = document.getElementById("fdomCiudad").value == "" ? "" : document.getElementById("fdomCiudad").value + ", ";
    var estado = document.getElementById("fdomEstado").value == "" ? "" : document.getElementById("fdomEstado").value + ", ";
    var pais = "México";

    var direccionFull = calle + numExt + colonia + cp + municipio + estado + pais;
    direccionFull = direccionFull.trim();

    document.getElementById("mapTextBuscar").value = direccionFull;

    BusquedaEnMapa();
}

function getValueUrl(str) {
    var v = window.location.search.match(new RegExp('(?:[\?\&]' + str + '=)([^&]+)'));
    return v ? v[1] : null;
}

function savePDI() {
    window.dhx.ajax.get("http://localhost:4181/api/CodigoPostal/GetCodigoPostalByColonia?estado=Jalisco&municipio=Acatic&colonia=Chivatero", function (data) {
        data = window.dhx.s2j(data.xmlDoc.responseText);
        
        if (data != null) {
        }
    });
}

function loadCodigoPostal(params, oper, funcCallback) {
    var queryParams = urlApiCodigoPostal + oper + "?";

    for (var key in params) {
        queryParams += "&"+ key + "=" + params[key];
    }

    window.dhx.ajax.get(queryParams, funcCallback);
}