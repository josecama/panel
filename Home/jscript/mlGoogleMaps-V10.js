/*-------------------------------------------------------------
Clase Google Maps
http://blog.extjs.eu/
http://try.sencha.com/
http://www.swarmonline.com/2011/05/20-things-to-avoid-or-do-when-getting-started-with-extjs-and-sencha-touch/
http://miamicoder.com/ext-js-tutorials/
http://training.figleaf.com/tutorials/toc_senchacomplete.cfm
http://devjs.eu/en/

---------------------------------------------------------------*/
var GFMap = function (config) {
	//------------------------------------------------------------
	// PUBLIC CLASS
	//------------------------------------------------------------
	var _GFMap = {
		publicTest: function(param){
			privateTest(param);
		},
		
		config: {},
		mapCtlID: null,
		map: null,
		infoWin: null,
		geocoder: null,
		eventPos: null,
		panorama: null,
		ruler: null,
		elevator: null,
		searchs: {
			type: 'A',  // A=Address  P=Places
			address: '',
			radio: 0,
			center: null,
			circle: null,
			callback: null,
			dirs: [],  // ubicaciones encontradas (incluir campo para indice del marker (overlay) asociado: markerIx)
			placesResult: [],
			placesCounter: 0,
			placesTimer: null
		},
		draw: null,
		
		directionsService: null,
		directionsDisplay: null,
		directionsDisplayStr: "",
		route: {
			options: {
				origin: null,
				destination: null,
				waypoints: [], // Ej: waypoints.push({location:latlon, stopover:true}); El número máximo de hitos permitido es ocho, además del origen y del destino.
				optimizeWaypoints: true,
				avoidHighways: false,	// evitar carreteras principales donde sea posible
				avoidTolls: false,		// evitar las autopistas de peaje donde sea posible
				provideRouteAlternatives: false,
				travelMode: google.maps.DirectionsTravelMode.DRIVING // DRIVING, WALKING, BICYCLING
			},
			markers: []
		},

		MessageBoxObj: null,

		lists: [],
	
		clearMapPanes: function(){
			var pane = this.map.getPanes().floatShadow;
			/*
			0 - mapPane - This pane is the lowest pane and is above the tiles. It may not receive DOM events. (Pane 0).
			1 - overlayLayer - This pane contains polylines, polygons, ground overlays and tile layer overlays. It may not receive DOM events. (Pane 1).
			2 - overlayShadow - This pane contains the marker shadows. It may not receive DOM events. (Pane 2).
			3 - overlayImage - This pane contains the marker foreground images. (Pane 3).
			4 - floatShadow - This pane contains the info window shadow. It is above the overlayImage, so that markers can be in the shadow of the info window. (Pane 4).
			5 - overlayMouseTarget - This pane contains elements that receive DOM mouse events, such as the transparent targets for markers. It is above the floatShadow, so that markers in the shadow of the info window can be clickable. (Pane 5).
			6 - floatPane - This pane contains the info window. It is above all map overlays. (Pane 6).
			*/
		},

		AddControl: function (div, pos) {
		    /*
            TOP_LEFT	        Elements are positioned in the top left and flow towards the middle.
            TOP_CENTER	        Elements are positioned in the center of the top row.
            TOP_RIGHT           Elements are positioned in the top right and flow towards the middle.

            LEFT_TOP	        Elements are positioned on the left, below top-left elements, and flow downwards.
            RIGHT_TOP	        Elements are positioned on the right, below top-right elements, and flow downwards.

            LEFT_CENTER	        Elements are positioned in the center of the left side.
            RIGHT_CENTER	    Elements are positioned in the center of the right side.

            LEFT_BOTTOM	        Elements are positioned on the left, above bottom-left elements, and flow upwards.
            RIGHT_BOTTOM	    Elements are positioned on the right, above bottom-right elements, and flow upwards.

            BOTTOM_LEFT	        Elements are positioned in the bottom left and flow towards the middle. Elements are positioned to the right of the Google logo.
            BOTTOM_CENTER	    Elements are positioned in the center of the bottom row.
            BOTTOM_RIGHT	    Elements are positioned in the bottom right and flow towards the middle. Elements are positioned to the left of the copyrights.
            */
		    var ctlPos = google.maps.ControlPosition.TOP_RIGHT;
		    if (pos == "TOP_LEFT")
		        ctlPos = google.maps.ControlPosition.TOP_LEFT;
		    if (pos == "TOP_CENTER")
		        ctlPos = google.maps.ControlPosition.TOP_CENTER;
		    if (pos == "TOP_RIGHT")
		        ctlPos = google.maps.ControlPosition.TOP_RIGHT;

		    if (pos == "LEFT_TOP")
		        ctlPos = google.maps.ControlPosition.LEFT_TOP;
		    if (pos == "RIGHT_TOP")
		        ctlPos = google.maps.ControlPosition.RIGHT_TOP;

		    if (pos == "LEFT_CENTER")
		        ctlPos = google.maps.ControlPosition.LEFT_CENTER;
		    if (pos == "RIGHT_CENTER")
		        ctlPos = google.maps.ControlPosition.RIGHT_CENTER;

		    if (pos == "LEFT_BOTTOM")
		        ctlPos = google.maps.ControlPosition.LEFT_BOTTOM;
		    if (pos == "RIGHT_BOTTOM")
		        ctlPos = google.maps.ControlPosition.RIGHT_BOTTOM;

		    if (pos == "BOTTOM_LEFT")
		        ctlPos = google.maps.ControlPosition.BOTTOM_LEFT;
		    if (pos == "BOTTOM_CENTER")
		        ctlPos = google.maps.ControlPosition.BOTTOM_CENTER;
		    if (pos == "BOTTOM_RIGHT")
		        ctlPos = google.maps.ControlPosition.BOTTOM_RIGHT;

		    this.map.controls[ctlPos].push(this.getDom(div));
		},

		PosInBounds: function (arrPos) {
		    for (var i = 0; i < arrPos.length; i++) {
		        if (!this.map.getBounds().contains(this.parsePos(arrPos[i]))) {
		            return false;
		        }
		    }
		    return true;
		},

		Center: function (pos){
		    this.map.setCenter(this.parsePos(pos));
		},

		ZoomToFit: function (latlngArray) {
		    if (latlngArray.length == 0) return;

		    var latMin = 999, latMax = -999, lngMin = 999, lngMax = -999;
		    var bounds = new google.maps.LatLngBounds()
		    for (var i = 0; i < latlngArray.length; i++) {
		        var pos = this.parsePos(latlngArray[i]);
		        var lat = pos.lat(), lng = pos.lng();
		        if (lat < latMin) latMin = lat;
		        if (lat > latMax) latMax = lat;
		        if (lng < lngMin) lngMin = lng;
		        if (lng > lngMax) lngMax = lng;
		        //bounds.extend(this.parsePos(latlngArray[i]));
		    }
		    var NE = new google.maps.LatLng(latMax, lngMax);
		    var SW = new google.maps.LatLng(latMin, lngMin);
		    bounds.extend(NE);
		    bounds.extend(SW);

		    this.map.fitBounds(bounds);
		},

	    // CIRCLE / POLYGON / POLYLINE / RECTANGLE

		CreateCircle: function(pos, radio, color, opacity, id, name, editable){
		    var ovy = new google.maps.Circle({
		        center: pos,
		        radius: radio,
		        map: this.map,
		        clickable: editable,
		        draggable: false,
		        fillColor: color,
		        fillOpacity: opacity,
		        strokeColor: color,
		        strokeOpacity: 1.0,
		        strokeWeight: 1
		    });
		    var overlayEdit = { overlay: ovy, type: 'circle', id: id, name: name };

		    if (editable) {
		        this.editOverlay(overlayEdit);
            }
		    return overlayEdit;
		},

		CreateRectangle: function (posNE, posSW, color, opacity, id, name, editable) {
		    var ovy = new google.maps.Rectangle({
		        fillColor: color,
		        fillOpacity: opacity,
		        strokeColor: color,
		        strokeOpacity: 1.0,
		        strokeWeight: 1,
		        map: this.map,
		        clickable: editable,
		        draggable: false,
		        bounds: new google.maps.LatLngBounds(posNE, posSW)
		    });

		    /*
		  polygonOptions: {
			clickable: true,
			draggable: false,
			fillColor: '#66cc00',
			fillOpacity: 0.30,
			strokeColor: '#009900',
			strokeOpacity: 0.6,
			strokeWeight: 3
		  },
		  polylineOptions: {
			clickable: true,
			draggable: false,
			strokeColor: '#cc3399',
			strokeOpacity: 0.5,
			strokeWeight: 7
		  }
            
            */
		    var overlayEdit = { overlay: ovy, type: 'rectangle', id: id, name: name };
		    if (editable) {
		        this.editOverlay(overlayEdit);
		    }
		    return overlayEdit;
		},

		getObjPos: function(obj){
			var pos = {x:0, y:0};
			while(obj){
				pos.x += obj.offsetLeft;
				pos.y += obj.offsetTop;
				obj = obj.offsetParent;
			}
			return pos;
		},

		getDom: function(elementId) {
			var object = null;
			if (document.getElementById)
				object = document.getElementById(elementId);
			else if (document.all)
				object = document.all[elementId];
			else if (document.layers)
				object = document.layers[elementId];
			return object;
		},

		resize: function (){
			if (this.map){
				google.maps.event.trigger(this.map, 'resize');
			}
		},

		parsePos: function(pos){
			if (!pos || pos === 'undefined')
				pos = null;
			else if (!(pos instanceof google.maps.LatLng)){
				if (typeof pos === 'string')
					pos = pos.split(',');
				var lat = pos[0] || pos.lat || 0;
				var lng = pos[1] || pos.lng || 0;
				if (lat == 0 || lng == 0)
					pos = null;
				else
					pos = new google.maps.LatLng(lat, lng);
			}
			return pos;
		},

		showTrafficLayer: function(show){
			if (this.trafficLayer.getMap() == null || show)
				this.trafficLayer.setMap(this.map);
			else
				this.trafficLayer.setMap(null);
		},

		showDrawTool: function(show){
			if (this.draw.getMap() == null || show)
				this.draw.setMap(this.map);
			else
				this.draw.setMap(null);
		},
		
		editOverlay: function(obj){
			var _this = this;
			// google.maps.drawing.OverlayType.CIRCLE / POLYGON / POLYLINE / RECTANGLE
			
			//obj.overlay.setDraggable(false);
			obj.overlay.setMap(_this.map);
			google.maps.event.addListener(obj.overlay, 'click', function(ev) {
				var editable = !obj.overlay.getEditable();
				obj.overlay.setEditable(editable);
				obj.overlay.setDraggable(editable);
			});
			google.maps.event.addListener(obj.overlay, 'rightclick', function (ev) {
			    var data = {
			        type: obj.type,
			        name: obj.name,
			        id: obj.id,
			        overlay: obj.overlay,
			        //pixel: { x: (ev.ua.clientX), y: (ev.ua.clientY) },
			        latLng: ev.latLng
			    };

			    if (obj.type == google.maps.drawing.OverlayType.CIRCLE) {
			        var rad = obj.overlay.getRadius();
			        data.centro = obj.overlay.getCenter();
			        data.radio = rad;
			        data.area = Math.PI * Math.pow(rad, 2);
			    } else if (obj.type == google.maps.drawing.OverlayType.RECTANGLE) {
			        var bounds = obj.overlay.getBounds();
			        var NE = bounds.getNorthEast();
			        var SW = bounds.getSouthWest();
			        var path = [NE, new google.maps.LatLng(NE.lat(), SW.lng()), SW, new google.maps.LatLng(SW.lat(), NE.lng())];
			        data.noreste = NE;
			        data.suroeste = SW;
			        data.centro = bounds.getCenter();
			        data.area = google.maps.geometry.spherical.computeArea(path);
			    } else if (obj.type == google.maps.drawing.OverlayType.POLYLINE) {
			        var path = obj.overlay.getPath();
			        data.path = path;
			        data.dist = google.maps.geometry.spherical.computeLength(path);
			    } else if (obj.type == google.maps.drawing.OverlayType.POLYGON) {
			        var path = obj.overlay.getPath();
			        data.path = path;
			        data.centro = obj.overlay.getBounds().getCenter();
			        data.area = google.maps.geometry.spherical.computeArea(path);
			    }

			    if (_this.config.onRightClick) _this.config.onRightClick(_this, data);
			});
		},

		Message: function (tit, msg, callbackFunc) {
		    callbackFunc = callbackFunc || null;
		    if (this.config.MessageBox) {
		        this.config.MessageBox(msg, tit, "E", "", function (OK, text) {
		            // si OK = true, entonces presiono boton OK
		            if (callbackFunc) callbackFunc(OK, text);
		        });
		    }
		},

		Msg: {
			alert: function(tit, msg, callbackFunc){
			    callbackFunc = callbackFunc || null;
			    if (_GFMap.config.MessageBox) {
			        _GFMap.config.MessageBox(msg, tit, "E", "", function (OK) {
			            // si OK = true, entonces presiono boton OK
			            if (callbackFunc) callbackFunc(OK, text);
			        });
			    }
			},
			confirm: function(tit, msg, callbackFunc){
				callbackFunc = callbackFunc || null;
				webix.alert({
				    text: msg, title: tit, type: "error", ok: "OK", cancel: "CANCELAR",
				    callback: function (res) {
				        if (callbackFunc) callbackFunc((res == true));
				    }
				});
			},
			prompt: function(tit, msg, val, callbackFunc){
			    callbackFunc = callbackFunc || null;
                /*
				Ext.Msg.prompt(tit, msg, function(btn, text){
					if (callbackFunc) callbackFunc(btn=='yes' || btn=='ok', text);
				},null,true,val);
                */
			    webix.alert({
			        text: msg, title: tit, type: "error", ok: "OK", cancel: "CANCELAR",
			        callback: function (res) {
			            if (callbackFunc) callbackFunc((res == true), text);
			        }
			    });
			},
			error: function(tit, msg, callbackFunc){
				this.show(tit, msg, 'E', null, '', callbackFunc);
			},
			warning: function(tit, msg, callbackFunc){
				this.show(tit, msg, 'W', null, '', callbackFunc);
			},
			info: function(tit, msg, callbackFunc){
				this.show(tit, msg, 'I', null, '', callbackFunc);
			},
			show: function(tit, msg, type, width, inputValue, callbackFunc){
				// type: I=Info  W=Warning  Q=Question  E=Error  P=Prompt
				// return: btnOK, text 

			    /*
			    type = type || '';
				callbackFunc = callbackFunc || null;
				
				// OK/YES/NO/CANCEL/OKCANCEL/YESNO/YESNOCANCEL
				var btns = (type == 'P' || type == 'Q') ? Ext.Msg.OKCANCEL : Ext.Msg.OK;
	
				var _icon = '';
				if (type == 'W')
					_icon = Ext.MessageBox.WARNING;
				else if (type == 'Q')
					_icon = Ext.MessageBox.QUESTION;
				else if (type == 'E')
					_icon = Ext.MessageBox.ERROR;
				else if (type == 'I')
					_icon = Ext.MessageBox.INFO;
	
				Ext.Msg.show({
					title: tit || 'Alert', msg: msg || 'Procesando... por favor espere.',
					width: width || 300, modal: true, closable: true,

					multiline: (type=='P'),
					prompt: (type=='P'),
					value: inputValue || '',
					icon: _icon,
					buttons: btns,
					fn: function(btn, text, opt){
						// btn (string): ok/yes/no/cancel
						var btnOK = (btn == 'ok' || btn == 'yes');
						if (callbackFunc) callbackFunc(btnOK, text);
					}
				});
                */
			},
			close: function(){
			    //Ext.MessageBox.close();
			}
		},

		getElevation: function(pos, callbakFunc){
			var _this = this;
			var locations = [];
	
			pos = this.parsePos(pos);
			if (!pos || !callbakFunc) return;
			
			locations.push(pos);
		
			// Create a LocationElevationRequest object using the array's one value
			var positionalRequest = { 'locations': locations }
		
			// Initiate the location request
			this.elevator.getElevationForLocations(positionalRequest, function(results, status) {
				if (status == google.maps.ElevationStatus.OK) {
					// Retrieve the first result
					if (results[0]){
						callbakFunc(parseInt(results[0].elevation));
					} else {
						callbakFunc(null);
					}
				} else {
					callbakFunc(null);
				}
			});
		},

		viewPanorama: function(pos){
			pos = this.parsePos(pos);
			if (pos){
				this.panorama.setPosition(pos);				
				this.panorama.setVisible(true);
			}
		},

		togglePanorama: function(){
			var toggle = this.panorama.getVisible();
			if (toggle == false) {
				this.panorama.setVisible(true);
			} else {
				this.panorama.setVisible(false);
			}
		},

		formatAddress: function(dirSel){
		    var xStr = '<b>' + dirSel.dom + '</b>';
			if (dirSel.isPlace){
				xStr = '<b>' + dirSel.name + '</b><br/>' + dirSel.dom;
				if (dirSel.phone != '')
					xStr += '<br/><b>Tel: </b>' + dirSel.phone;
				if (dirSel.url != '')
					xStr += '<br/><a href="' + dirSel.url + '" target="_blank">' + dirSel.url + '</a>';
			}
			return xStr;
		},

		geocodePosition: function(pos, callbackFunc){
			var _this = this;
			pos = this.parsePos(pos);
			geocoderOptions = { address: pos.toUrlValue(8), language: "SP" };
			_getGeocodeDirs(geocoderOptions, null, 0, callbackFunc, function(dirs, originalCallBak){
			    originalCallBak(dirs);
			});
		},

		geocodeAddress: function(type, address, center, radio, showDirs, callbackFunc){
			var _this = this;
			address = address.trim();
			if (address.length == 0)
				callbackFunc([]);
			else if (type == 'P')
				_geocodePlaces(address, center, radio, showDirs, callbackFunc);
			else
				_geocodeAddress(address, center, radio, showDirs, callbackFunc);
		},

		clearSearchResults: function(){
			var _this = this;

			this.searchs.dirs = [];
			this.searchs.circle.setMap(null);
			this.markers.delete(this.config.searchsListname, '');
		},

		createMarker: function(pos, icono, label, titulo, texto){
			var _this = this;
			pos = this.parsePos(pos);
			var mostrarInfo = (texto != '') ? true : false;
			
			var marker = null;
			if (label != '') {
			    var n = label.lastIndexOf("#BG:");
			    var bg = '#FFFF55';
			    if (n > 0) {
			        bg = label.substring(n + 4);
			        label = label.substring(0, n);
			    }

				marker = new MarkerWithLabel({
					position: pos,
					map: _this.map,
					icon: icono,
					labelContent: label,        // plain text or an HTML DOM node
					title: '' + titulo.replace(/<(?:.|\n)*?>/gm, ''),
					raiseOnDrag: false,
					labelAnchor: new google.maps.Point(0, 7),
					//labelClass: "labels", // the CSS class for the label
					labelStyle: {
                        /*
			            borderRadius: '12px 0px 12px 0px', 
			            mozBorderRadius: '12px 0px 12px 0px', 
			            webkitBorderRadius: '12px 0px 12px 0px', 
			            border: '1px solid #999',
                        */
						whiteSpace: 'nowrap',
						fontFamily: 'arial', 
						fontWeight: 'bold',
						padding: '2px 4px 2px 7px', 
						backgroundColor: bg, 
						opacity: '0.75',
						filter: 'alpha(opacity=75)',
						color: '#000',
						textAlign: 'center'
					},
					labelInBackground: true,
					labelVisible: true
				});
			} else {				
				marker = new google.maps.Marker({
					position: pos,
					map: _this.map,
					title: titulo, // .replace(/<(?:.|\n)*?>/gm, '')
					icon: icono,
					flat: true,
					clickable: mostrarInfo
				});
			}

			if (mostrarInfo)
				this.attachMarkerInfoWin(marker, texto);

			google.maps.event.addDomListener(marker, 'rightclick', function (ev) {
			    _this.eventPos = marker.getPosition();
			    data = { type: 'marker', pixel: { x: (ev.ua.clientX), y: (ev.ua.clientY) }, latLng: ev.latLng };
			    if (_this.config.onRightClick) _this.config.onRightClick(_this, data);
			});

			return marker;
		},

		attachMarkerInfoWin: function(marker,content){
		    var _this = this;
		    google.maps.event.addListener(marker, 'click', function(ev){
		        // ev.latLng.toUrlValue(8)   marker.getPosition().toUrlValue(8)
		        _this.showInfoWin(content, marker.getPosition());
		    });
		},

		replaceTags: function(text, pos){
			pos = this.parsePos(pos);
			text = text.replaceAll('#objName#', this.config.objName);
			text = text.replaceAll('#lat#', (pos) ? pos.lat().toFixed(8) : '');
			text = text.replaceAll('#lng#', (pos) ? pos.lng().toFixed(8): '');
			return text;
		},

		showInfoWin: function(text, pos){
			// #objName#, #lat#, #lng#, #dom#
			pos = this.parsePos(pos);
			text = this.replaceTags(text,pos);
			var xStr = '<div class="infoText">' + text + this.replaceTags(this.config.infoWin.moreText,pos) + '</div>';

			var _this = this;

			if (xStr.lastIndexOf('#dom#') > -1){
				this.geocodePosition(pos, function(dirs){
					var p = pos;
					if (dirs.length > 0){
						xStr = xStr.replaceAll('#dom#', dirs[0].dom);
						text = text.replaceAll('#dom#', dirs[0].dom);
						//p = dirs[0].pos;
					} else {
						xStr = xStr.replaceAll('#dom#', '*** Informacion no disponible ***');
						text = text.replaceAll('#dom#', '*** Informacion no disponible ***');
					}
					_this.infoWin.setContent(xStr);
					_this.infoWin.setPosition(pos);
					_this.infoWin.open(_this.map);
				});
			} else {
				//this.infoWin.close();
				this.infoWin.setContent(xStr);
				this.infoWin.setPosition(pos);
				this.infoWin.open(this.map);
			}
		},

		triggerEvent: function(obj, eventName){
			google.maps.event.trigger(obj, eventName);
		},

		distance: function(pos1, pos2, medida) {
			pos1 = this.parsePos(pos1);
			pos2 = this.parsePos(pos2);
			var dist = 0;
			if (pos1 && pos2){
				dist = google.maps.geometry.spherical.computeDistanceBetween(pos1, pos2);
				if (medida=="K"){
					dist = dist / 1000; // Kms  
				}
			}
			return Math.round(dist*100)/100;
		},

		setRoute: function(action, pos){
			// action: I=Inicio  F=Fin  B=Borrar
			pos = this.parsePos(pos);
			var _this = this;
			if (action == 'B'){
				this.route.options.origin = null;
				this.route.options.destination = null;
				this.removeRoute();
				this.directionsDisplay.setMap(null);
			} else if (pos){
				if (action == 'I'){
					this.route.options.origin = pos;
					if (!this.route.options.destination){
						// Muestra marcador de Inicio de ruta
						var marker = this.createMarker(pos, this.config.icons.routeStart, '', 'Inicio de ruta', '');
						this.route.markers.push(marker);
					}
				} else if (action == 'F'){
					this.route.options.destination = pos;
					if (!this.route.options.origin){
						// Muestra marcador de fin de ruta
						var marker = this.createMarker(pos, this.config.icons.routeEnd, '', 'Final de ruta', '');
						this.route.markers.push(marker);
					}
				}
			}
			if (!(this.route.options.origin && this.route.options.destination)){
				return;
			}

			this.ZoomToFit([this.route.options.origin,this.route.options.destination]);

			//this.route.options.waypoints = [];
			//var p1 = new google.maps.LatLng(20.7097221,-103.40731847);
			//this.route.options.waypoints.push({location: p1, stopover: true});
	
			// Route the directions and pass the response to a
			// function to create markers for each step.
			var _this = this;
			this.directionsDisplay.setMap(this.map);
			this.directionsService.route(this.route.options, function(response, status){
				if (status == google.maps.DirectionsStatus.OK){
					_this.directionsDisplay.setDirections(response);
					// esto se ejecutará en el evento "directions_changed" definido en el método "inicializa"
					//_this.showRoute(_this.directionsDisplay.directions); 
				}
			});
		},

		showRoute: function(directionsResult){
			// First, remove any existing markers from the map.
			this.removeRoute();
			// For each step, place a marker, and add the text to the marker's
			// info window. Also attach the marker to an array so we
			// can keep track of it and remove it when calculating new
			// routes.
			var directionRoute = directionsResult.routes[0];
			var myRoute = directionRoute.legs[0];
	
			this.route.options.origin = myRoute.start_location;
			this.route.options.destination = myRoute.end_location;
			
			this.directionsDisplayStr = directionRoute.copyrights + '<br/></i>';
			for (var i=0; i < directionRoute.warnings.length; i++){
				this.directionsDisplayStr += directionRoute.warnings[i] + '<br/>';
			}
			this.directionsDisplayStr += '</i><hr/>';

			var _this = this;
			var nSteps = myRoute.steps.length;
            if (nSteps > 0)
                this.directionsDisplayStr += "<b>Distancia: </b>" + myRoute.distance.text +
                    "&nbsp;&nbsp;&nbsp;<b>Duración: </b>" + myRoute.duration.text + "<hr/>";

            for (var i = 0; i < nSteps; i++) {
				this.directionsDisplayStr += (i+1) + ") " + myRoute.steps[i].instructions + "<br/>";
				if (i == 0) continue;
	
				var title = i + '/' + nSteps + ' - ' + myRoute.steps[i].instructions;
				var pos = myRoute.steps[i].start_point;
				var marker = this.createMarker(myRoute.steps[i].start_point, this.config.icons.routeStep, '', title, title);
				_this.route.markers.push(marker);
			}

			if (myRoute.start_location.equals(myRoute.end_location)){
				this.directionsDisplayStr = "";
			}
		},

		showRouteInstructions: function () {
		    var xStr = "NO HAY INSTRUCCIONES DE RUTA.";
			if (this.directionsDisplayStr != ""){
			    xStr = this.directionsDisplayStr;
			}
			return xStr;
		},

		removeRoute: function(){
			// First, remove any existing markers from the map.
			this.showOverlays(this.route.markers, false);
			// Now, clear the array itself.
			this.route.markers = [];
			this.route.options.waypoints = [];
			this.directionsDisplayStr = "";
		},

		markers: {
			dirs: [],

			add: function(lista, callbackFunc){
				// regresa: nothing
				var AddMarkersConfig = {
					list: lista,
					prevListName: '',
					addToGrid: false,
					callback: callbackFunc,
					ix: 0
				};
				addMarkersTimer(AddMarkersConfig);
			},

			delete: function(listName, code){
				var i = 0, removed = 0;
				while (i < this.dirs.length){
					if (this.dirs[i].list == listName){
						if (code == '' || (code != '' && this.dirs[i].code == code)){
							if (this.dirs[i].ovy){
								this.dirs[i].ovy.setMap(null);
							}
							this.dirs.splice(i,1);
							removed++;
						} else {
							i++;
						}
					} else {
						i++;
					}
				}
				return removed;
			},
			
			deleteById: function (id){
				var _parent = getSuper();
				for (var i=0; i < this.dirs.length; i++){
					if (this.dirs[i]._id == id){
						var rec = this.dirs[i];
						this.dirs.splice(i,1);
						
						if (rec.ovy) rec.ovy.setMap(null);

						return true;
					}
				}
				return false;
			},
			
			update: function (rec){
				var _parent = getSuper();

				var id = rec._id || -1;
				if (id > -1){
					for (var i=0; i < this.dirs.length; i++){
						if (this.dirs[i]._id == id){
							this.dirs[i] = rec;
							return true;
						}
					}
				}
				return false;
			},
			
			get: function(listName, code){
				var arr = [];
				for (var i=0; i < this.dirs.length; i++){
					if (this.dirs[i].list == listName){
						if (code == '' || (code != '' && this.dirs[i].code == code)){
							arr.push(this.dirs[i]);
						}
					}
				}
				return arr;
			},
			
			getById: function (id){
				for (var i=0; i < this.dirs.length; i++){
					if (this.dirs[i]._id == id){
						return this.dirs[i];
					}
				}
				return null;
			},

			show: function(listName, visible){
				var _parent = getSuper();
				for (var i=0; i < this.dirs.length; i++){
					if (this.dirs[i].list == listName){
						if (this.dirs[i].ovy){
							this.dirs[i].ovy.setVisible(visible);
						}
					}
				}
			}
		},
		
		setList: function(list){
			// return '' or 'ERROR: Ya existe una Lista con ese nombre'
			var error = '';
			
			var newlist = {
				name: list.name.trim() || '',
				codeField: list.codeField || {},
				nameField: list.nameField || {},
				menu: list.menu || []
			};

			newlist.codeField.show = list.codeField.show || false;
			newlist.codeField.title = list.codeField.title || 'Code';
			newlist.codeField.width = list.codeField.width || 60;

			newlist.nameField.show = list.nameField.show || false;
			newlist.nameField.title = list.nameField.title || 'Name';
			newlist.nameField.width = list.nameField.width || 150;
			
			newlist.menu = (newlist.menu instanceof Array) ? newlist.menu : [];
			
			if (newlist.name == '')
				error += 'Invalid listname. ';
			else {
				var ix = this.findList(newlist.name);
				if (ix == -1)
					this.lists.push(newlist);
				else
					this.lists[ix] = newlist;
			}
			return error;
		},
		
		findList: function(listName){
			for (var i=0; i < this.lists.length; i++){
				if (this.lists[i].name.toUpperCase() == listName.toUpperCase()) return i;
			}
			return -1;
		},

		showOverlays: function(overlays, show){
			if (!overlays) return;
			this.showOverlaysTimer(0, overlays, show);
		},

		showOverlaysTimer: function(i, overlays, show){
			// Esto se hace para evitar que el navegador muestre el mensaje:
			// "Un proceso de javascript se está tardando demasiado"
			var timeout = (i+1) % 250 == 0 ? 250 : 1;
			var _this = this; 
			if (i < overlays.length){
				var map = overlays[i].getMap();
				if (show && !map){
					overlays[i].setMap(this.map);
				} else if (!show && map){
					overlays[i].setMap(null);
				}
				window.setTimeout(function(){ _this.showOverlaysTimer(i+1, overlays, show); }, timeout);
			} 
		},

		addruler: function(pos){
			var _this = this;
			var dist = 0;
			pos = this.parsePos(pos);
			pos = pos || this.map.getCenter();
	
			if (this.ruler){
				// Mostrar u ocultar en el mapa los objetos de regla
				this.ruler.Activo = !this.ruler.Activo;
				var xMap = (this.ruler.Activo) ? _this.map : null;
				
				this.ruler.Rpoly.setMap(xMap);
				this.ruler.R1.setMap(xMap);
				this.ruler.R2.setMap(xMap);
				
				//this.ruler.R1label.setMap(map);
				//this.ruler.R2label.setMap(map);
			} else {
				// Creación del objeto
				this.ruler = new Object;
				this.ruler.Activo = true;
				
				this.ruler.R1 = this.createMarker(pos, this.config.icons.rule, ' #BG:#63eaff', '', '');
				this.ruler.R1.setDraggable(true);
				
				this.ruler.R2 = this.createMarker(pos, this.config.icons.rule, ' #BG:#63eaff', '', '');
				this.ruler.R2.setDraggable(true);
				
				this.ruler.Rpoly = new google.maps.Polyline({
					path: [this.ruler.R1.position, this.ruler.R2.position] ,
					strokeColor: "#0066CC",
					strokeOpacity: .5,
					strokeWeight: 3
				});
				this.ruler.Rpoly.setMap(this.map);

				//this.ruler.R1label = new Label({ map: this.map });
				//this.ruler.R2label = new Label({ map: this.map });

				var ubicaRuler = function(){
					_this.ruler.Rpoly.setPath([_this.ruler.R1.getPosition(), _this.ruler.R2.getPosition()]);
					dist = _this.distance(_this.ruler.R1.getPosition(), _this.ruler.R2.getPosition(), 'M');
					dist = (dist > 1000) ? (dist/1000).toFixed(2) + " kms" : dist.toFixed(0) + " mts";
		
					_this.ruler.R1.set('labelContent',dist);
					_this.ruler.R2.set('labelContent', dist);

    				_this.ruler.R1.setTitle(dist);
					_this.ruler.R2.setTitle(dist);

					//_this.ruler.R1label.set('text',dist);
					//_this.ruler.R2label.set('text',dist);
				};

				google.maps.event.addListener(this.ruler.R1, 'drag', ubicaRuler);
				google.maps.event.addListener(this.ruler.R2, 'drag', ubicaRuler);
			}

			//this.ruler.R1label.bindTo('position', this.ruler.R1, 'position');
			//this.ruler.R2label.bindTo('position', this.ruler.R2, 'position');

			if (this.ruler.Activo){
				this.ruler.R1.setPosition(pos);
				this.ruler.R2.setPosition(pos);

				this.ruler.Rpoly.setPath([this.ruler.R1.getPosition(), this.ruler.R2.getPosition()]);

				dist = "0 mts";
				_this.ruler.R1.set('labelContent', dist);
				_this.ruler.R2.set('labelContent', dist);
			    //this.ruler.R1label.set('text', dist);
				//this.ruler.R2label.set('text', dist);
			}
		},

		fromLatLngToPixel: function (position) {
			position = this.parsePos(position);
			var scale = Math.pow(2, this.map.getZoom());
			var proj = this.map.getProjection();
			var bounds = this.map.getBounds();

			var nw = proj.fromLatLngToPoint(
				new google.maps.LatLng(
					bounds.getNorthEast().lat(),
					bounds.getSouthWest().lng()
				)
			);
			var point = proj.fromLatLngToPoint(position);

			return new google.maps.Point(
				Math.floor((point.x - nw.x) * scale),
				Math.floor((point.y - nw.y) * scale)
			);
		},

		fromPixelToLatLng: function (pixel) {
			var scale = Math.pow(2, Map.getZoom());
			var proj = Map.getProjection();
			var bounds = Map.getBounds();

			var nw = proj.fromLatLngToPoint(
			new google.maps.LatLng(
				bounds.getNorthEast().lat(),
				bounds.getSouthWest().lng()
			));
			var point = new google.maps.Point();

			point.x = pixel.x / scale + nw.x;
			point.y = pixel.y / scale + nw.y;

			return proj.fromPointToLatLng(point);
		}
	};

	//------------------------------------------------------------
	// PRIVATE CLASS DATA
	//------------------------------------------------------------
	config.objName = config.objName || '';
	config.mapDiv = config.mapDiv || '';
	config.zoom = parseInt(config.zoom) || 5;
	config.center = config.center || {lat:23.78125547, lng: -102.47001323};
	config.center.lat = parseFloat(config.center.lat) || 23.78125547;
	config.center.lng = parseFloat(config.center.lng) || -102.47001323;
	config.getBrowserGPS = (typeof config.getBrowserGPS === 'undefined') ? false : config.getBrowserGPS;

	config.mapWin = config.mapWin || {};
	config.mapWin.useWindow = (typeof config.mapWin.useWindow === 'undefined') ? false : config.mapWin.useWindow;
	config.mapWin.autoShow = (typeof config.mapWin.autoShow === 'undefined') ? true : config.mapWin.autoShow;
	config.mapWin.title = config.mapWin.title || 'Google Map';
	config.mapWin.icon = config.mapWin.icon || '';
	config.mapWin.width = config.mapWin.width || 600;
	config.mapWin.height = config.mapWin.height || 400;
	config.mapWin.modal = (typeof config.mapWin.modal === 'undefined') ? false : config.mapWin.modal;
	config.mapWin.menuInTootlbar = (typeof config.mapWin.menuInTootlbar === 'undefined') ? true : config.mapWin.menuInTootlbar;

	config.maxLists = config.maxLists || 99;
	config.maxMarkers = config.maxMarkers || 9999;
	config.searchsListname = config.searchsListname || 'Search Results';

	config.icons = config.icons || {};
	config.icons.routeStart = config.icons.routeStart || '';
	config.icons.routeEnd = config.icons.routeEnd || '';
	config.icons.routeStep = config.icons.routeStep || '';
	config.icons.marker = config.icons.marker || '';
	config.icons.cancel = config.icons.cancel || '';
	config.icons.rule = config.icons.rule || '';
	config.icons.streetView = config.icons.streetView || '';

	config.onClick = config.onClick || null;
	config.onRightClick = config.onRightClick || null;
	config.onZoomChange = config.onZoomChange || null;
	config.MessageBox = config.MessageBox || null;

	config.infoWin = config.infoWin || {};
	config.infoWin.width = parseInt(config.infoWin.width) || 320;
	config.infoWin.moreText = config.infoWin.moreText || ''; // #objName#, #lat#, #lng#
	config.infoWin.moreTextUseDefault = (typeof config.infoWin.moreTextUseDefault === 'undefined') ? false : config.infoWin.moreTextUseDefault;
	if (config.infoWin.moreTextUseDefault){
		config.infoWin.moreText = '<br/><br/><b>Latitud/Longitud:</b> #lat#, #lng#'+
			'<br/><a href="javascript:#objName#.viewPanorama([#lat#,#lng#]);"><img src="' + config.icons.streetView + '" alt="" style="vertical-align:bottom;" /> Panorámica</a>&nbsp;&nbsp;&nbsp;' +
			'<b>RUTA: </b>' +
			'<a href="javascript:#objName#.setRoute(\'I\',\'#lat#,#lng#\');"><img src="' + config.icons.routeStart + '" alt="" style="vertical-align:bottom;" /> Inicio</a>&nbsp;&nbsp;' +
			'<a href="javascript:#objName#.setRoute(\'F\',\'#lat#,#lng#\');"><img src="' + config.icons.routeEnd + '" alt="" style="vertical-align:bottom;" /> Fin</a>&nbsp;&nbsp;' +
			'<a href="javascript:#objName#.setRoute(\'B\',\'0,0\');"><img src="' + config.icons.cancel + '" style="vertical-align:bottom;" alt="" /> Borrar</a>&nbsp;' +
			'<br/>&nbsp;';
	}

	config.adsense = config.adsense || {};
	config.adsense.format = config.adsense.format || 'HALF_BANNER';
	config.adsense.position = config.adsense.position || 'RIGHT_BOTTOM';
	config.adsense.backgroundColor = config.adsense.backgroundColor || '#ddeeff';
	config.adsense.borderColor = config.adsense.borderColor || '#ddeeff';
	config.adsense.titleColor = config.adsense.titleColor || '#0000cc';
	config.adsense.textColor = config.adsense.textColor || '#333333';
	config.adsense.urlColor = config.adsense.urlColor || '#009900';
	config.adsense.publisherId = config.adsense.publisherId || '';
	
	var _LastID = 0;
		// datos adsense: 
		//	format, position, backgroundColor, borderColor, 
		//	titleColor, textColor, urlColor, publisherId
		// format: 
		//	LEADERBOARD, BANNER, HALF_BANNER, SKYSCRAPER, WIDE_SKYSCRAPER, VERTICAL_BANNER, 
		//	BUTTON, SMALL_SQUARE, SQUARE, SMALL_RECTANGLE, MEDIUM_RECTANGLE, LARGE_RECTANGLE, 
		//	SMALL_VERTICAL_LINK_UNIT, MEDIUM_VERTICAL_LINK_UNIT, LARGE_VERTICAL_LINK_UNIT, 
		//	X_LARGE_VERTICAL_LINK_UNIT, SMALL_HORIZONTAL_LINK_UNIT, LARGE_HORIZONTAL_LINK_UNIT
		// position:
		//	TOP_CENTER, TOP_LEFT, TOP_RIGHT, RIGHT_TOP, LEFT_TOP, RIGHT_CENTER,
		//	LEFT_CENTER, BOTTOM_CENTER, BOTTOM_LEFT, BOTTOM_RIGHT, LEFT_BOTTOM, RIGHT_BOTTOM

	//------------------------------------------------------------
	// PRIVATE CLASS FUNCTIONS
	//------------------------------------------------------------

	initialize = function(){
	    _GFMap.config = config;

		if (config.mapDiv == '')
			_GFMap.mapCtlID = document.createElement('DIV');
		else
		    _GFMap.mapCtlID = _GFMap.getDom(config.mapDiv);
		_GFMap.mapCtlID.style = 'height:100%; width:100%;';

		var mapOptions = {
		  zoom: config.zoom
		  ,maxZoom: 20
		  ,minZoom: 3
		  ,center: new google.maps.LatLng(config.center.lat, config.center.lng)
		  ,mapTypeId: google.maps.MapTypeId.ROADMAP
		  ,mapTypeControlOptions: {
		    position: google.maps.ControlPosition.TOP_RIGHT,
		    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		  }
          ,panControl: false
	      ,streetViewControl: true
		  ,disableDefaultUI: false
		};

		// ----------------------------------------------------------------------
		// CREACION DEL MAPA
		// ----------------------------------------------------------------------
		_GFMap.map = new google.maps.Map(_GFMap.mapCtlID, mapOptions);

		// ----------------------------------------------------------------------
		// ANUNCIOS GOOGLE (agregar en config inicial)
		// ----------------------------------------------------------------------
		if (config.adsense.publisherId != ''){
			var adUnitDiv = document.createElement('div');
			//adUnitDiv.style.cssText = 'padding:2px;';
			var adUnitOptions = {
				format: google.maps.adsense.AdFormat[config.adsense.format],
				position: google.maps.ControlPosition[config.adsense.position],
				backgroundColor: config.adsense.backgroundColor,
				borderColor: config.adsense.borderColor,
				titleColor: config.adsense.titleColor,
				textColor: config.adsense.textColor,
				urlColor: config.adsense.urlColor,
				publisherId: config.adsense.publisherId,
				map: _GFMap.map,
				visible: true
			};
			var adUnit = new google.maps.adsense.AdUnit(adUnitDiv, adUnitOptions);
			/*
			adUnit.setFormat(google.maps.adsense.AdFormat['<format>']);
			adUnit.setPosition(google.maps.ControlPosition['<position>']);
			adUnit.setBackgroundColor(adStyle['color_bg']);
			adUnit.setBorderColor(adStyle['color_border']);
			adUnit.setTitleColor(adStyle['color_link']);
			adUnit.setTextColor(adStyle['color_text']);
			adUnit.setUrlColor(adStyle['color_url']);
			*/
		}

		// ----------------------------------------------------------------------
		// EVENTOS EN EL MAPA
		// ----------------------------------------------------------------------
		google.maps.event.addDomListener(window, "resize", function(){
			var center = _GFMap.map.getCenter();
			google.maps.event.trigger(_GFMap.map, "resize");
			_GFMap.map.setCenter(center);
		});

		// <<< EVENTOS EN MAPA >>>
		google.maps.event.addDomListener(_GFMap.map, 'click', function (ev) {
		    _GFMap.eventPos = ev.latLng;
		    if (_GFMap.config.onClick) _GFMap.config.onClick(_GFMap, ev);
		});
		google.maps.event.addDomListener(_GFMap.map, 'rightclick', function (ev) {
		    _GFMap.eventPos = ev.latLng;
		    var mapPos = _GFMap.getObjPos(_GFMap.map.getDiv());
		    data = { type: 'map', pixel: { x: (ev.pixel.x + mapPos.x), y: (ev.pixel.y + mapPos.y) }, latLng: ev.latLng };
			if (_GFMap.config.onRightClick) _GFMap.config.onRightClick(_GFMap, data);
		});
		google.maps.event.addDomListener(_GFMap.map, 'zoom_changed', function () {
		    if (_GFMap.config.onZoomChange) _GFMap.config.onZoomChange(_GFMap, _GFMap.map.getZoom());
		});

		// ----------------------------------------------------------------------
		// SERVICIOS EN EL MAPA
		// ----------------------------------------------------------------------

		// <<< ALTITUD >>>
		_GFMap.elevator = new google.maps.ElevationService();

		// <<< CODIFICACION GEOGRAFICA >>>
		_GFMap.geocoder = new google.maps.Geocoder();

		// <<< TRAFICO >>>
		_GFMap.trafficLayer = new google.maps.TrafficLayer();
		_GFMap.trafficLayer.setMap(null);

		var panoOptions = {
			position: new google.maps.LatLng(config.center.lat, config.center.lng),
			pov: {
				heading: 35,
				pitch: 0
			},
			visible: false,
			enableCloseButton: true,

			addressControl: true,
			addressControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER
			},

			linksControl: true,
			panControl: true,
			zoomControl: true,
			zoomControlOptions: {
				position: google.maps.ControlPosition.TOP_LEFT,
				style: google.maps.ZoomControlStyle.DEFAULT   // DEFAULT, LARGE, SMALL
			}
		};
		_GFMap.panorama = new google.maps.StreetViewPanorama(_GFMap.map.getDiv(), panoOptions);

		// <<< VENTANA PARA MENSAJES INFORMATIVOS >>>

		_GFMap.infoWin = new google.maps.InfoWindow({
			content: "",
			disableAutoPan: false,
			maxWidth: config.infoWin.width
		});

		// <<< PARA CALCULO DE RUTAS >>>

		var rendererOptions = {
			map: _GFMap.map 
			,draggable: true
			//,hideRouteList: true
			//,preserveViewport: true
		};
		_GFMap.directionsService = new google.maps.DirectionsService();				
		_GFMap.directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

		// Esto es válido porque se definió 'draggable: true' en 'rendererOptions'
		google.maps.event.addListener(_GFMap.directionsDisplay, 'directions_changed', function(){
			_GFMap.showRoute(_GFMap.directionsDisplay.directions);
		});

		// <<< PARA BUSQUEDA DE LUGARES >>>

		_GFMap.searchs.circle = new google.maps.Circle({
			center: null,
			radius: 0,
			map: null,
			clickable: false,
			fillColor: '#FFCC00',
			fillOpacity: 0.2,
			strokeColor: '#FF6600',
			strokeOpacity: 0.5,
			strokeWeight: 1
		});
		
		// TEST DRAW OVERLAY
		/*
		_GFMap.searchs.circle.name = "Radio de busqueda";
		_GFMap.searchs.circle.id = 1;
		_GFMap.editOverlay(_GFMap.searchs.circle);
		*/
		//

		_GFMap.placesService = new google.maps.places.PlacesService(_GFMap.map);

		// Drawing Control		
		_GFMap.draw = new google.maps.drawing.DrawingManager({
		  map: null,
		  drawingMode: null, // google.maps.drawing.OverlayType.MARKER
		  drawingControl: true,
		  drawingControlOptions: {
			position: google.maps.ControlPosition.TOP_CENTER,
			drawingModes: [
			  google.maps.drawing.OverlayType.CIRCLE,
			  google.maps.drawing.OverlayType.POLYGON,
			  google.maps.drawing.OverlayType.POLYLINE,
			  google.maps.drawing.OverlayType.RECTANGLE
			]
		  },
		  circleOptions: {
			clickable: true,
			draggable: false,
			fillColor: '#ffcc00',
			fillOpacity: 0.30,
			strokeColor: '#ffcc00',
			strokeOpacity: 0.7,
			strokeWeight: 2
		  },
		  rectangleOptions: {
			clickable: true,
			draggable: false,
			fillColor: '#66ccff',
			fillOpacity: 0.30,
			strokeColor: '#66ccff',
			strokeOpacity: 0.6,
			strokeWeight: 2
		  },
		  polygonOptions: {
			clickable: true,
			draggable: false,
			fillColor: '#66cc00',
			fillOpacity: 0.30,
			strokeColor: '#009900',
			strokeOpacity: 0.6,
			strokeWeight: 3
		  },
		  polylineOptions: {
			clickable: true,
			draggable: false,
			strokeColor: '#cc3399',
			strokeOpacity: 0.5,
			strokeWeight: 7
		  }
		});
		google.maps.event.addListener(_GFMap.draw, 'overlaycomplete', function(obj) {
			_GFMap.draw.setDrawingMode(null);

			obj.name = 'New ' + obj.type;
			obj.id = 0;

			_GFMap.editOverlay(obj);
		});
		if (config.getBrowserGPS) getNavGeolocation();
	};

	getNavGeolocation = function(){
		var posOptions = {
			enableHighAccuracy: false, // default = false, true might be slower 
			timeout: 7000, // (no default) in milliseconds 
			maximumAge: 600000 // default = 0, in milliseconds (aceptar posiciones no mayores a este tiempo) 
		};
		var watchId = null;
		if (navigator.geolocation){
			/*
			watchId = navigator.geolocation.watchPosition(function(position){
				var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				_GFMap.showInfoWin('<i>Su ubicación actual es:</i><br/>#dom#', pos);
				_GFMap.map.setZoom(15);
				_GFMap.map.setCenter(pos);
			}, function(err){
				// err.code : 1=PERMISSION_DENIED  2=POSITION_UNAVAILABLE  3=TIMEOUT
				// err.message
			}, posOptions);
			*/

			navigator.geolocation.getCurrentPosition(function(position){
				var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				_GFMap.showInfoWin('<i>Su ubicación actual es:</i><br/>#dom#', pos);
				_GFMap.map.setZoom(15);
				_GFMap.map.setCenter(pos);
			}, function(err){
				// err.code : 1=PERMISSION_DENIED  2=POSITION_UNAVAILABLE  3=TIMEOUT
				// err.message
			}, posOptions);
		} else {
			// Browser doesn't support Geolocation
		}

		// Cancelar repeticion de busqueda en 5 minutos
		// Cancel the updates when the user clicks a button.
		window.setTimeout(function(){ navigator.geolocation.clearWatch(watchId); }, (5 * 60 * 1000));
	};

	addMarkersTimer = function(markersConfig){
		var ID = new Date().valueOf();
		if (ID == _LastID){
			window.setTimeout(function(){ addMarkersTimer(markersConfig); }, 1);
			return;
		}
		_LastID = ID;
		
		var _this = _GFMap;
		var ix = markersConfig.ix;
		var timeout = (ix+1) % 100 == 0 ? 100 : 1;
		if (ix < markersConfig.list.length){
			var itemList = markersConfig.list[ix];
			itemList.ovy = null;
			itemList._id = ID;
			var pos = _this.parsePos([itemList.lat,itemList.lng]);
			if (pos){
				var marker = _this.createMarker(pos, itemList.icon, itemList.label, itemList.name, itemList.desc);
				itemList.ovy = marker;
			}
			_this.markers.dirs.push(itemList);
			
			markersConfig.prevListName = itemList.list;

			markersConfig.ix = ix+1;
			window.setTimeout(function(){ addMarkersTimer(markersConfig); }, timeout);
		} else {
			markersConfig.callback();
		}
	};

	_geocodeAddress = function(address, center, radio, showDirs, callbackFunc){
		var _this = _GFMap;
		center = _this.parsePos(center);

		//_this.Message('Procesando','Buscando ubicaciones, por favor espere...',function(btnOK){});
		_this.clearSearchResults();

		_this.searchs.type = 'A';
		_this.searchs.address = address;
		_this.searchs.radio = radio;
		_this.searchs.center = center;
		_this.searchs.circle.setCenter(center);
		_this.searchs.circle.setRadius(radio);
		_this.searchs.callback = callbackFunc;
		
		var geocoderOptions = null;
        // remplazar & por %26
		if (center && radio > 0){
			var maxBounds = _this.searchs.circle.getBounds();
			geocoderOptions = { address: address, language: "es", bounds: maxBounds };
		} else {
		    geocoderOptions = { address: address, language: "es" };
		}
		
		this._getGeocodeDirs(geocoderOptions, center, radio, callbackFunc, function (dirs, originalCallBak) {
			_this.Msg.close();
			_this.searchs.dirs = dirs;
			if (showDirs && _this.searchs.dirs.length > 1){
				createSearchMarkers(function(){
					_this.selectDirs(_this.config.searchsListname, _this.searchs.callback);
				});
			} else {
			    originalCallBak(_this.searchs.dirs);
			}
		});
	};
	
	_getGeocodeDirs = function(geocoderOptions, center, radio, originalCallBak ,callbakFunc){
		var _this = _GFMap;
		center = _this.parsePos(center);
		isBounds = (typeof geocoderOptions.bounds === 'undefined') ? false : true;
		var dirs = [];
		_this.geocoder.geocode(geocoderOptions, function(results, status){
			if (status == google.maps.GeocoderStatus.OK){
				// .ERROR -> There was a problem contacting the Google servers.
				// .INVALID_REQUEST -> This GeocoderRequest was invalid.
				// .OVER_QUERY_LIMIT -> The webpage has gone over the requests limit in too short a period of time.
				// .REQUEST_DENIED -> The webpage is not allowed to use the geocoder.
			    // .UNKNOWN_ERROR  .ZERO_RESULTS
			    /*
                    postcode_localities[] - es una matriz que denota todas las localidades que comprende un código postal. Esto solo se presenta cuando el resultado es un código postal que contiene varias localidades. 
                    geometry.location_type:
                        "ROOFTOP" indica que el resultado devuelto es un geocódigo exacto para el cual contamos con información de ubicación precisa que puede delimitarse hasta la dirección.
                        "RANGE_INTERPOLATED" indica que el resultado devuelto refleja una aproximación (generalmente en una calle) interpolada entre dos puntos precisos (como intersecciones). Generalmente se devuelven resultados interpolados cuando no se encuentran disponibles geocódigos exactos para una dirección.
                        "GEOMETRIC_CENTER" indica que el resultado devuelto es el centro geométrico de un resultado como una polilínea (por ejemplo, una calle) o un polígono (región).
                        "APPROXIMATE" indica que el resultado devuelto es aproximado.

                    Tipos de dirección y tipos de componentes de dirección

                    - street_address indica una dirección exacta.
                    - route indica la denominación de una carretera (como "US 101").
                    - intersection indica una intersección principal, generalmente de dos calles importantes.
                    - political indica una entidad política. Generalmente, este tipo indica un polígono de alguna administración pública.
                    - country indica la entidad política nacional, y es generalmente el tipo de orden más alto que devuelve el geocodificador.
                    - administrative_area_level_1 indica una entidad civil de primer orden por debajo del nivel de país. En Estados Unidos, estos niveles administrativos son los estados. No todos los países poseen estos niveles administrativos.
                    - administrative_area_level_2 indica una entidad civil de segundo orden por debajo del nivel de país. En Estados Unidos, estos niveles administrativos son los condados. No todos los países poseen estos niveles administrativos.
                    - administrative_area_level_3 indica una entidad civil de tercer orden por debajo del nivel de país. Este tipo indica una división civil inferior. No todos los países poseen estos niveles administrativos.
                    - administrative_area_level_4 indica una entidad civil de cuarto orden por debajo del nivel de país. Este tipo indica una división civil inferior. No todos los países poseen estos niveles administrativos.
                    - administrative_area_level_5 indica una entidad civil de quinto orden por debajo del nivel de país. Este tipo indica una división civil inferior. No todos los países poseen estos niveles administrativos.
                    - colloquial_area indica un nombre alternativo de uso frecuente para la entidad.
                    - locality indica una entidad política constituida de una ciudad o un pueblo.
                    - ward indica un tipo específico de localidad japonesa para facilitar la distinción entre los múltiples componentes de localidad en una dirección japonesa.
                    - sublocality indica una entidad civil de primer orden por debajo de una localidad. Algunas ubicaciones pueden recibir uno de los tipos adicionales: sublocality_level_1 a sublocality_level_5. Cada nivel de sublocalidad es una entidad civil. Los números más altos indican un área geográfica más pequeña.
                    - neighborhood indica un barrio determinado.
                    - premise indica una ubicación determinada, generalmente un edificio o un conjunto de edificios con un nombre en común.
                    - subpremise indica una entidad de primer orden por debajo de una ubicación determinada; generalmente un edificio en particular en un conjunto de edificios con un nombre en común.
                    - postal_code indica un código postal tal como se usa para identificar una dirección de correo postal dentro del país.
                    - natural_feature indica una atracción natural destacada.
                    - airport indica un aeropuerto.
                    - park indica un parque determinado.
                    - point_of_interest indica un punto de interés determinado. Generalmente, estos "PI" son entidades locales destacadas que no pueden incluirse fácilmente en otra categoría, como el edificio "Empire State" o la "Estatua de la libertad".

                    Además de lo dicho antes, los componentes de dirección pueden incluir los siguientes tipos.

                    Nota: Esta lista no está completa y está sujeta a cambio.

                    • floor indica el piso en la dirección de un edificio.
                    • establishment generalmente indica un lugar que aún ha sido categorizado.
                    • point_of_interest indica un punto de interés determinado.
                    • parking indica un área de estacionamiento o una estructura de estacionamiento.
                    • post_box indica una casilla de correo específica.
                    • postal_town indica un conjunto de áreas geográficas, como locality y sublocality, utilizadas para las direcciones postales en algunos países.
                    • room indica una sala en la dirección de un edificio.
                    • street_number indica el número exacto de una calle.
                    • bus_station, train_station y transit_station indican la ubicación de una parada de autobús, tren o transporte público respectivamente.

                */

				var n = results.length;
				for (var i=0; i < n; i++){
					var lugar = results[i];
					var numExt="", calle="", colonia="", deleg="", codPos="", ciudad="", estado="", edoCorto="", pais="", paisCorto="";

					for (var j=0; j < lugar.address_components.length; j++){
						var dirComp = lugar.address_components[j];
						for (var k=0; k < dirComp.types.length; k++){
							if (dirComp.types[k] == "street_number" && numExt == '')
								numExt = dirComp.long_name;
							else if (dirComp.types[k] == "route" && calle == '')
								calle = dirComp.long_name;
							else if (dirComp.types[k] == "neighborhood" && colonia == '')
								colonia = dirComp.long_name;
							else if (dirComp.types[k] == "sublocality" && colonia == '')
							    colonia = dirComp.long_name;
							else if (dirComp.types[k] == "postal_code" && codPos == '')
								codPos = dirComp.long_name;
							else if (dirComp.types[k] == "locality" && ciudad == '')
								ciudad = dirComp.long_name;
							else if (dirComp.types[k] == "administrative_area_level_3" && deleg == '')
							    deleg = dirComp.long_name;
							else if (dirComp.types[k] == "administrative_area_level_2" && ciudad == '')
								ciudad = dirComp.long_name;
							else if (dirComp.types[k] == "administrative_area_level_1" && estado == ''){
								estado = dirComp.long_name;
								edoCorto = dirComp.short_name;
							} else if (dirComp.types[k] == "country" && pais == ''){
								pais = dirComp.long_name;
								paisCorto = dirComp.short_name;
							}
						}
					}
					var dist = (center) ? _this.distance(center, lugar.geometry.location, 'M') : 0;
					var dirInBounds = (center && radio > 0) ? (dist <= radio) : true;
					if (dirInBounds) {
					    dirs.push({
					        pos: lugar.geometry.location,
					        types: lugar.types.join(","),
					        posType: lugar.geometry.location_type,
					        accuracy: lugar.types[0], // lugar.geometry.location_type, 
					        dist: dist,
					        dom: lugar.formatted_address,
					        streetName: calle,
					        externalNum: numExt,
					        neighborhood: colonia,
					        sublocality: deleg,
					        postalCode: codPos,
					        locality: ciudad,
					        state: estado,
					        stateShort: edoCorto,
					        country: pais,
					        countryShort: paisCorto,
					        isPlace: false,
					        name: '',
					        phone: '',
					        url: ''
					    });
					}
				}
			}
			callbakFunc(dirs, originalCallBak);
		});
	};

	_geocodePlaces = function(address, center, radio, showDirs, callbackFunc){
		var _this = _GFMap;

		center = _this.parsePos(center);
		if (!center){
			center = _this.map.getCenter();
			if (radio < 1000) radio = 10000;
		}
		//_this.Message('Procesando', 'Buscando ubicaciones, por favor espere...', function(btnOK){});
		_this.clearSearchResults();

		_this.searchs.type = 'P';
		_this.searchs.address = address;
		_this.searchs.radio = radio;
		_this.searchs.center = center;
		_this.searchs.circle.setCenter(center);
		_this.searchs.circle.setRadius(radio);

		// for nearbySearch
		var request = {
			location: center,
			radius: radio,
			//types: ['store'],		// store, food, establishment, ....
			//name: '',				// Restricts results to Places that include this text in the name.
			keyword: address		// A term to be matched against all available fields, including but not limited to name, type, and address, as well as customer reviews and other third-party content.
		    //query: address
	    };

		
		// for textSearch
		/*
		request = {
			//bounds:   // Both location and radius will be ignored if bounds is set.
			location: center,
			radius: radio,
			query: address
			//types: ['store'],		// store, food, establishment, ....
		};
		*/

		_this.placesService.nearbySearch(request, function (results, status, pagination) {
            // name, vicinity, types[strings], geometry
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				_this.searchs.callback = callbackFunc;
				_this.searchs.placesResult = results;
				_this.searchs.placesCounter = 0;
				_this.searchs.pagination = pagination;
				_this.searchs.placesTimer = null;

			    // Hasta aqui ya tenemos la info básica de los lugares (nombre, domicilio, posicion, y referencia)
			    // ahora es necesario otro servicio para recuperar los DETALLES DEL LUGAR en base a la 'referencia'
                /*
				_this.searchs.placesTimer = setTimeout(function () { _geocodePlacesDetails(); }, 1);
                */

			    // SI NO QUEREMOS "DETALLES DEL LUGAR" PODEMOS REGRESAR LO BASICO
    
                for (var i=0; i < results.length; i++){
				    dist = _this.distance(_this.searchs.center, results[i].geometry.location, 'M');
				    if (dist < (_this.searchs.radio * 1.05)) {
				        _this.searchs.dirs.push({
				            pos: results[i].geometry.location,
				            types: results[i].types.join(","),
				            posType: results[i].geometry.location_type,
				            accuracy: results[i].types[0], //results[i].geometry.location_type,
				            dist: dist,
				            dom: results[i].vicinity,
				            streetName: "",
				            externalNum: "",
				            neighborhood: "",
				            sublocality: "",
				            postalCode: "",
				            locality: "",
				            state: "",
				            stateShort: "",
				            country: "",
				            countryShort: "",
				            isPlace: true,
				            name: results[i].name,
				            phone: "",
				            url: ""
				        });
				    }
                }
                _this.searchs.callback(_this.searchs.dirs);

			} else {
				_this.Msg.close();
				callbackFunc([]);
			}
		});
	};

	_geocodePlacesDetails = function(){
		var _this = _GFMap;

		clearTimeout(_this.searchs.placesTimer);
		_this.searchs.placesTimer = null;

		if (_this.searchs.placesCounter < _this.searchs.placesResult.length){
			// todavia faltan lugares de obtener sus detalles
			_this.Message('Procesando', 'Recuperando información. ubicacion ' + (_this.searchs.placesCounter + 1) + '/' + _this.searchs.placesResult.length, function(btnOK){});
		} else {
			// ya terminamos de recuperar los detalles de los lugares
//			if (_this.searchs.pagination.hasNextPage && _this.searchs.dirs.length < 50){
//				_this.searchs.pagination.nextPage();
//			} else {
				_this.Msg.close();
				_this.searchs.callback(_this.searchs.dirs);
//			}
			return;
		}

		var place = _this.searchs.placesResult[_this.searchs.placesCounter];
        // icon, id, name, types[<strings>]
		var nombre = place.name;
		var pos = place.geometry.location;
		var dom = place.vicinity;
		// place.types[] --> tipos de lugar
		var dist=0, numExt="", calle="", colonia="", deleg="", codPos="", ciudad="", estado="", edoCorto="", pais="", paisCorto="", tel="", url="";

		var req = {reference: place.reference};
		_this.placesService.getDetails(req, function(details, stat){
		    _this.searchs.placesCounter += 1;
			if (stat == google.maps.places.PlacesServiceStatus.OK){
			    dom = details.formatted_address || dom;
			    for (var j = 0; j < details.address_components.length; j++) {
					var dirComp = details.address_components[j];
					for (var k=0; k < dirComp.types.length; k++){
						if (dirComp.types[k] == "street_number" && numExt == '')
							numExt = dirComp.long_name;
						else if (dirComp.types[k] == "route" && calle == '')
							calle = dirComp.long_name;
						else if (dirComp.types[k] == "neighborhood" && colonia == '')
							colonia = dirComp.long_name;
						else if (dirComp.types[k] == "postal_code" && codPos == '')
							codPos = dirComp.long_name;
						else if (dirComp.types[k] == "sublocality" && deleg == '')
							deleg = dirComp.long_name;
						else if (dirComp.types[k] == "locality" && ciudad == '')
							ciudad = dirComp.long_name;
						else if (dirComp.types[k] == "administrative_area_level_1" && estado == ''){
							estado = dirComp.long_name;
							edoCorto = dirComp.short_name;
						} else if (dirComp.types[k] == "country" && pais == ''){
							pais = dirComp.long_name;
							paisCorto = dirComp.short_name;
						}
					}
				}
				dist = _this.distance(_this.searchs.center, details.geometry.location,'M');
				
				tel = details.formatted_phone_number || details.international_phone_number || '';
				url = details.website || '';

				if (dist < (_this.searchs.radio * 1.25)) {
				    _this.searchs.dirs.push({
				        pos: pos,
				        types: details.types.join(","),
				        posType: details.geometry.location_type,
				        accuracy: details.types[0], //details.geometry.location_type,
				        dist: dist,
				        dom: dom,
				        streetName: calle,
				        externalNum: numExt,
				        neighborhood: colonia,
				        sublocality: deleg,
				        postalCode: codPos,
				        locality: ciudad,
				        state: estado,
				        stateShort: edoCorto,
				        country: pais,
				        countryShort: paisCorto,
				        isPlace: true,
				        name: nombre,
				        phone: tel,
				        url: url
				    });
				}
            }
			_this.searchs.placesTimer = setTimeout(function(){ _geocodePlacesDetails();},100);
		});
	};

	createSearchMarkers = function(callbackFunc){
		var _this = _GFMap;

		var n = _this.searchs.dirs.length;
		var markersList = [];
		for (var i=0; i < n; i++){
			var dirSel = _this.searchs.dirs[i];
				
			var dirEdt = '<!--#' + dirSel.dist + '|' +  dirSel.dom + '|' +  dirSel.streetName + '|' + dirSel.externalNum + '|' + dirSel.neighborhood + '|' +
				dirSel.sublocality + '|' + dirSel.postalCode + '|' + dirSel.locality + '|' +
				dirSel.state + '|' + dirSel.stateShort + '|' + dirSel.country + '|' + dirSel.countryShort + '|' +
				(dirSel.isPlace ? '1' : '0') + '|' + dirSel.name + '|' + dirSel.phone + '|' + dirSel.url + '|' + dirSel.prec + '#-->';
			
			markersList.push({
				list: _this.config.searchsListname,
				code: 'S-'+(i+1),
				lat: dirSel.pos.lat(),
				lng: dirSel.pos.lng(),
				icon: _this.config.icons.marker, 
				name: dirSel.isPlace ? dirSel.name + ' / ' + dirSel.dom : dirSel.dom,
				desc: _this.formatAddress(dirSel) + dirEdt,
				label: 'S'+(i+1)
			});
		}
		if (_this.searchs.center && _this.searchs.radio > 0){
			_this.searchs.circle.setCenter(_this.searchs.center);
			_this.searchs.circle.setRadius(_this.searchs.radio);
			_this.searchs.circle.setMap(_this.map);
		}
		_this.markers.delete(_this.config.searchsListname, "");
		_this.markers.add(markersList, function(){
			callbackFunc();
		});
	};
	
	getSuper = function(){ return _GFMap; };

	privateTest = function(param){
		alert(param + ' - ' + _GFMap.config.objName);
	};

	//------------------------------------------------------------
	initialize();
	return _GFMap;
};

google.maps.Polygon.prototype.getBounds=function(){
    var bounds = new google.maps.LatLngBounds()
    this.getPath().forEach(function(element,index){bounds.extend(element)})
    return bounds
}

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};
/*-------------------------------------------------------------
	FIN Clase Google Maps
---------------------------------------------------------------*/
