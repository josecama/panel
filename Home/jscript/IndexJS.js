﻿var Main_Layout = null;
var NavigationHeader = null;
var NavigationToolBar = null;
var NavigationTabBar = null;
var VisorTab = null;
var B2BTab = null;
var DispatchConsoleTab = null;
var DispatchConsoleNuevoTab = null;
var DispatchConsole2Tab = null;
var BITab = null;
var AllowedModules = null;
var ultimoSigoVivoVisor = null;
var ultimoSigoVivoB2B = null;
var ultimoSigoVivoVisorAlertas = null;
var EsperandoParaSeleccionarFolio = null;
var EsperandoParaSeleccionarFolioData = null;

var EsperandoParaSeleccionarPDI = null;
var EsperandoParaSeleccionarPDIData = null;

var sesionData = null;

var dhxWins, w1, w2, w3;

var IframeCargados = {};

var HeightNavHome = null;
ACTIVE_TAB = '';
var IS_DEBUG = false;



$(function () {
	 // debugger;
	ShowModalCargando();
	Login();
	receiveMessage(callback);

	HeightNavHome = $('.navbar-custom').height();
	mueveReloj();
});

function Login() {
	// debugger;
	var dataModules = [];
	//dataModules.push({ Name: '#Login', Description: 'Iniciar sesión', Uri:'http://www.cbmex7.com/cbmex6/GoogleViewerTemp/Login.aspx' });
	dataModules.push({ Name: '#Login', Description: 'Iniciar sesión', Uri: 'https://cbmex8wvs.com/GoogleViewerMapcel/Produccion/pages/GoogleViewer.aspx' });
	AllowedModules = dataModules;

	$('#btn-CerrarSesion').hide();

	RefreshNav();

	select_first_nav();

	$('.user-loged').hide();
	$(".user-loged").text("");

	$(".img-enterprise").hide();
}

function setUserData() {
	//$(".user-data-body").text("testttttttt");
// debugger;
	var operName = sesionData.OPERNAME == null || sesionData.OPERNAME == '' ? 'N/A' : sesionData.OPERNAME;
	var operZH = sesionData.USERTIMEZONE == null || sesionData.USERTIMEZONE == '' ? 'N/A' : sesionData.USERTIMEZONE;
	var operLogin = sesionData.USERNAME == null || sesionData.USERNAME == '' ? 'N/A' : sesionData.USERNAME;
	var userSoporte = sesionData.USRSOPORTE == null || sesionData.USRSOPORTE == '' ? 'N/A' : sesionData.USRSOPORTE;
	

	document.getElementById('operName').innerHTML = '<b>' + operName + '</b>';
	document.getElementById('operZH').innerHTML = '<b>' + operZH + '</b>';
	document.getElementById('operLogin').innerHTML = '<b>' + operLogin + '</b>';
	document.getElementById('userSoporte').innerHTML = '<b>' + userSoporte + '</b>';

	

}
function showUserData() {
	setUserData();
	$("#modalUserData").modal();
}

function ShowModalCargando() {
	// debugger;
	//$("#cargandoModal").modal({ backdrop: 'static', keyboard: false });
	$("#cargandoModal").modal();
}
function HideModalCargando() {
	// debugger;
	$("#cargandoModal").modal("hide");
}

function CheckSesionIsValid(timeCurrent, ultimoSigoVivo) {
	// // debugger;
	if (ultimoSigoVivo != null) {
		var deltaTime = timeCurrent - ultimoSigoVivo;
		if (deltaTime > 30000) {
			CloseSession();
		}
	}
}
function select_first_nav() {
	 // debugger;
	window.location.hash = $(".navbar-nav-home a").eq(0).attr("href");
	hashchange_function();
}
function hashchange_function() {
	// debugger;
	var hash = window.location.hash;
	ChangeTabActive(hash, true, true);
}
function StartSession(permisos) {
	 // debugger;
	var dataModules = [];

	//dataModules.push({ Name: '#DemosSubasta', Description: 'Demos subasta', Uri: 'http://localhost:49987/Home/pages/Examples/DemoSubasta.aspx' });
	for (var i in permisos) {

		switch (permisos[i].trim()) {
			case "ALERTAS360":
				dataModules.push({ Name: '#Alerts', Description: 'Alertas 360', Uri: 'http://cbmex4.com/DispachConsole/Panel/Home/pages/ModuleAlertsindex.html' });
				break;
			case "B2B":
				if (IS_DEBUG)
					dataModules.push({ Name: '#B2B', Description: 'B2B', Uri: 'http://cbmex7.com/stch/ML-Apps/TMP/CYVSA/BanorteSite/PagesAdmin/PanelFolio.aspx?' });
				else
					dataModules.push({ Name: '#B2B', Description: 'B2B', Uri: 'http://cbmex7.com/B2BApi/PagesAdmin/PanelFolio.aspx?' });
				break;
			case "VISORALERTAS":
				dataModules.push({ Name: '#VisorAlertas', Description: 'Visor Alertas', Uri: 'http://cbmex4.com/DispachConsole/panel/Home/pages/AlertasVisor.aspx?ciaid=595&numfol=FOL-142806&codoper=Gayosso02&codpdi=HOS-ANGMEX&horaini=2017-06-14 14:00:00&horafin=2017-06-14 14:20:59&axyc=' + Date.now() });
				break;
			case "VISOR":
				dataModules.push({ Name: '#Visor', Description: 'Visor', Uri: 'https://cbmex8wvs.com/GoogleViewerMapcel/Produccion/Login.aspx' });
				break;
			case "BUSCARDIR":
				dataModules.push({ Name: '#DispatchConsole', Description: 'Buscar Dir', Uri: 'http://cbmex4.com/DispachConsole/panel/?ciaId=667&head_Cliente_Numero=12569&head_Cliente_Nombre=Mario%20Lopez&head_Cliente_Dir=ni%C3%B1os%20heroes 2058%20guadalajara&head_Cliente_Cruce1=marsella&head_Cliente_Cruce2=&head_Cliente_Alias=Mario&head_Cliente_DIR=Beethoven%204914,%20Zapopan&head_DescricionOtro=Juan Hdez.&head_Email=juanh@hotmail.com&head_Tel=3310394617&head_LAT=0&head_LNG=0&h-b2b-folio=00124&head_Pdi_cod=GH2514-1&head_ContactId=1876' });
				break;
			case "BUSCARDIRNUEVO":
				dataModules.push({ Name: '#DispatchConsoleNuevo', Description: 'Buscar Dir (Nuevo)', Uri: 'http://cbmex4.com/DispachConsole/Panel/Home/pages/dispatch.html' });
				break;
			case "DASHBOARDBI":
				dataModules.push({ Name: '#BI', Description: 'Dash Board BI', Uri: 'http://tiny.cc/MapCel' });
				break;
			case "DEMOSUBASTA":
				dataModules.push({ Name: '#DemosSubasta', Description: 'Demos subasta', Uri: 'http://cbmex4.com/DispachConsole/Panel/Home/pages/Examples/DemoSubasta.aspx' });
				break;
			case "RUTAS":
				//http://localhost:54722/Debug/TEST_PostMessage.aspx


				//http://www.cbmex7.com/B2BApi/Debug/TEST_PostMessage.aspx
				//dataModules.push({ Name: '#Rutas', Description: 'Rutas', Uri: 'http://localhost:54722/Debug/TEST_PostMessage.aspx' });


				dataModules.push({ Name: '#Rutas', Description: 'Rutas', Uri: 'https://www.cbmex8wvs.com/Nuevos/RutasEV-1/Produccion/pages/Index.aspx' });

				break;
		}
	}
	
	AllowedModules = dataModules;
	 // debugger;
	RefreshNav();
}

function CloseSession() {
	 // debugger;
	window.location.href = "https://loginml.com/LoginMapcel/pages/Login.aspx";
	/*Login();

	RefreshNav();

	select_first_nav();

	$('.user-loged').hide();
	$(".user-loged").text("");

	$(".img-enterprise").hide();*/
}
function formatDate(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
}
function mueveReloj() {
	momentoActual = new Date();
	hora = momentoActual.getHours();
	minuto = momentoActual.getMinutes();
	segundo = momentoActual.getSeconds();

	fecha = formatDate(momentoActual);

	str_segundo = new String(segundo);
	if (str_segundo.length == 1)
		segundo = "0" + segundo;

	str_minuto = new String(minuto);
	if (str_minuto.length == 1)
		minuto = "0" + minuto;

	str_hora = new String(hora);
	if (str_hora.length == 1)
		hora = "0" + hora;
	
	horaImprimible = fecha + ' ' + hora + ':' + minuto + ':' + segundo;

	document.getElementById("reloj").innerHTML = "";
	document.getElementById("reloj").innerHTML = horaImprimible;

	setTimeout("mueveReloj()", 1000);
} 

function RefreshNav() {
	 // debugger;
	$(".navbar-nav-home").empty();

	for (var i in AllowedModules) {
		if (AllowedModules.hasOwnProperty(i)) {
			var module = AllowedModules[i];
			var $li = $("<li>");
			var $a = $("<a>");
			$a.text(module.Description);
			$a.attr("href", module.Name);

			$li.append($a);
			$(".navbar-nav-home").append($li);

			var name = AllowedModules[i].Name;
			if (name == "#Login") {
				$("#Visoriframe").attr("src", AllowedModules[i].Uri);
			}
		}
	}
}

function ChangeTabActive(ItemTab, ChangeIframe, ReloadSource) {
	 // debugger;
	var tab_selected = $(".navbar-home a[href='" + ItemTab + "']");



	$(".navbar-home li").removeClass("active");
	tab_selected.parent().addClass("active");

	if (ItemTab == "#CerrarSesion") {
		CloseSession();
		return;
	}

	if (ChangeIframe) {
		for (var i in AllowedModules) {
			if (ItemTab == AllowedModules[i].Name) {
				if (sesionData != null && ItemTab != "#Login") {
					$('#btn-CerrarSesion').show();
				}

				if (ItemTab == "#Login") {
					ItemTab = "#Visor";
				}

				$("iframe").hide();
				//$(".Content-iframe").hide();

				var iframeSelected = ItemTab + "iframe";

				if (ItemTab != "#Login" && ItemTab != "#Visor") {

					if (IframeCargados[iframeSelected] == undefined) {

						IframeCargados[iframeSelected] = "OK";

						for (var i in AllowedModules) {

							if (AllowedModules[i].Name == ItemTab) {

								if (iframeSelected != "#BIiframe") {
									ShowModalCargando();
								}
								if (ReloadSource) {
									var extraDataUri = "";

									if (iframeSelected != "#B2Biframe") {
										extraDataUri += "?";
									}

									extraDataUri += "emp=" + sesionData.COMPANY_ID + "&loginId=" + sesionData.USERID + "&operId=" + sesionData.OPERID;

									if (iframeSelected == "#B2Biframe") {
										extraDataUri += "&type=PR&r=" + sesionData.B2B_RAMAID + "&depId=" + sesionData.USER_RAMADEPTOID + "&tiposDep=" + sesionData.MigrarRamasTEMP + "&token=7296a0494adc434fa8ed758de45ffe"
									}

									$(iframeSelected).attr("src", AllowedModules[i].Uri + extraDataUri);
								}
							}
						}
					}
				}

				$(iframeSelected).show();
				$(iframeSelected).height(window.innerHeight - HeightNavHome);
			}
		}
	}
	ACTIVE_TAB = ItemTab;
	console.log('ACTIVE_TAB:' + ACTIVE_TAB);
}


function VISORLOGINOK(data) {
	// debugger;
	ShowModalCargando();
}


function RUTASLOADED(data) {
	// debugger;
	HideModalCargando();

	setTimeout(function () {
		sendMessage(document.getElementById("Rutas").contentWindow, { "origen": "Rutas", "proceso": "SIGUE_VIVO", "params": {} }, document.getElementById("Rutas").src);
	}, 10000);
}

function DEMOSUBASTALOADED(data) {
	// debugger;
	if (EsperandoParaSeleccionarFolio) {
		sendMessage(document.getElementById("DemosSubastaiframe").contentWindow, { "origen": "DEMOSUBASTA_", "proceso": "selectFolioByID", "params": EsperandoParaSeleccionarFolioData }, document.getElementById("DemosSubastaiframe").src)
		EsperandoParaSeleccionarFolio = false;
	}
	HideModalCargando();

	setTimeout(function () {
		sendMessage(document.getElementById("DemosSubastaiframe").contentWindow, { "origen": "HomeMapCel", "proceso": "SIGUE_VIVO", "params": {} }, document.getElementById("DemosSubastaiframe").src);
	}, 10000);
}

function B2BLOADED(data) {
	// debugger;
	if (EsperandoParaSeleccionarFolio) {
		sendMessage(document.getElementById("B2Biframe").contentWindow, { "origen": "B2B_", "proceso": "selectFolioByID", "params": EsperandoParaSeleccionarFolioData }, document.getElementById("B2Biframe").src)
		EsperandoParaSeleccionarFolio = false;
	}
	HideModalCargando();

	setTimeout(function () {
		sendMessage(document.getElementById("B2Biframe").contentWindow, { "origen": "HomeMapCel", "proceso": "SIGUE_VIVO", "params": {} }, document.getElementById("B2Biframe").src);
	}, 10000);
}

function VISORALERTASLOADED(data) {
	// debugger;
	if (EsperandoParaSeleccionarPDI) {
		sendMessage(document.getElementById("VisorAlertasiframe").contentWindow, { "origen": "HomeMapCel", "proceso": "CARGARPDI", "params": EsperandoParaSeleccionarPDIData }, document.getElementById("VisorAlertasiframe").src);
		EsperandoParaSeleccionarPDI = false;
	}
	HideModalCargando();
}

function ALERTSLOADED(data) {
	// debugger;
	HideModalCargando();
}


function VISOR_END_SESSION(data) {
	//http://www.cbmex7.com/cbmex6/GoogleViewer/Login.aspx
	//http://www.cbmex7.com/cbmex6/GoogleViewerTemp/Login.aspx
	HideModalCargando();
	// debugger;
	if (window.location.href.toUpperCase().indexOf("GOOGLEVIEWERTEMP") > 0) {
		window.location.href = "https://loginml.com/LoginMapcel/pages/Login.aspx";
	} else if (window.location.href.toUpperCase().indexOf("LOCALHOST") > 0) {
		window.location.href = "https://loginml.com/LoginMapcel/pages/Login.aspx";
		//window.location.href = "http://localhost:85/googleviewer/Login.aspx";
	} else {
		window.location.href = "https://loginml.com/LoginMapcel/pages/Login.aspx";
	}
}	
function VISORLOADED(data) {
	// debugger;
	sesionData = data;
	StartSession(sesionData.PERMISOS_NUEVO_MAPCEL.split(","));
	$('#btn-CerrarSesion').show();

	var userName = "";
	var operName = "";

	if (data.USERNAME != undefined) {
		userName = data.USERNAME;
	}

	if (data.usrName != undefined) {
		userName = data.USERNAME;
	}

	if (data.OPERNAME != undefined) {
		operName = data.OPERNAME;
	}


	$(".user-loged").text(userName);
	$(".user-loged").show();

	if (operName != "")
		$(".user-name").text(operName);
	else
		$(".user-name").text('');


	ChangeTabActive("#Visor");

	if ((sesionData.CIA_ICON2 != "" && sesionData.CIA_ICON2 != null) && sesionData.CIA_ICON2 != "mpacelbco-30.png") {
		$(".img-enterprise").attr("src", "https://cbmex8wvs.com/cbmex6/GoogleViewerMapcel/Produccion/img/" + sesionData.CIA_ICON2);
		$(".img-enterprise").show();
	}
	if (sesionData.COMPANY_ID == 684) {
		$(".main-logo").attr("src", "https://cbmex8wvs.com/Nuevos/Panel/Home/pages/assets/img/rautrucks-mini.png");
	} else {
		$(".main-logo").attr("src", "https://cbmex8wvs.com/Nuevos/Panel/Home/pages/assets/img/logo.png");
	}

	//HideModalCargando();

	setTimeout(function () {
		sendMessage(document.getElementById("Visoriframe").contentWindow, { "origen": "HomeMapCel", "proceso": "SIGUE_VIVO", "params": {} }, document.getElementById("Visoriframe").src);
	}, 10000);

	setTimeout(function () {
		var timeCurrent = Date.now();

		CheckSesionIsValid(timeCurrent, ultimoSigoVivoVisor);
		CheckSesionIsValid(timeCurrent, ultimoSigoVivoB2B);
		CheckSesionIsValid(timeCurrent, ultimoSigoVivoVisorAlertas);
	}, 30000);

	//TODO: Se ejecuta cuando carga el visor.

	
	if (typeof data.COMPANY_PESTANA_PORTAL_WEB == 'undefined' || data.COMPANY_PESTANA_PORTAL_WEB == 'Visor') {
		setTimeout(function () {
			HideModalCargando();
		}, 1500)
	} else {
		setTimeout(function () {
			ChangeTabActive('#' + data.COMPANY_PESTANA_PORTAL_WEB, true, true);
		}, 3000)
	}
	

}

function BUSCARDIRLOADED(data) {
	// debugger;
	HideModalCargando();
}

function BUSCARDIRNUEVOLOADED(data) {
	// debugger;
	HideModalCargando();
}


function ALERTSCARGARFOLIO(data) {
	// debugger;
	if ($("#B2Biframe").attr("src") != "") {
		ChangeTabActive("#B2B", true, false);
		sendMessage(document.getElementById("B2Biframe").contentWindow, { "origen": "B2B_", "proceso": "selectFolioByID", "params": data }, document.getElementById("B2Biframe").src);
	}
	else {
		EsperandoParaSeleccionarFolio = true;
		EsperandoParaSeleccionarFolioData = data;
		ChangeTabActive("#B2B", true, true);
	}

	window.location.hash = "B2B";
}

function ALERTSCARGARPDI(data) {
	// debugger;
	if ($("#VisorAlertasiframe").attr("src") != "") {
		ChangeTabActive("#VisorAlertas", true, false);
		sendMessage(document.getElementById("VisorAlertasiframe").contentWindow, { "origen": "HomeMapCel", "proceso": "CARGARPDI", "params": data }, document.getElementById("VisorAlertasiframe").src);
	}
	else {
		EsperandoParaSeleccionarPDI = true;
		EsperandoParaSeleccionarPDIData = data;
		ChangeTabActive("#VisorAlertas", true, true);
	}

	window.location.hash = "VisorAlertas";
}



function VISORSIGUE_VIVO(data) {
	// debugger;
	ultimoSigoVivoVisor = Date.now()
}
function VISORALERTASSIGUE_VIVO(data) {
	// debugger;
	ultimoSigoVivoVisorAlertas = Date.now()
}
function B2BSIGUE_VIVO(data) {
	// debugger;
	ultimoSigoVivoB2B = Date.now()
}

function VISORFINSESION(data) {
	// debugger;
	CloseSession();
}
function B2BFINSESION(data) {
	// debugger;
	window.location.href = "https://loginml.com/LoginMapcel/pages/Login.aspx";
	//window.location.hash = "B2B";
	//hashchange_function();
}


function B2BGET_ACTIVE(data) {
	// debugger;
	sendMessage(document.getElementById("B2Biframe").contentWindow, { "origen": "B2B_", "proceso": "ACTIVE_TAB", "params": ACTIVE_TAB }, document.getElementById("B2Biframe").src)
}