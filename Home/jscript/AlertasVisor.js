﻿var ICONS_PATH = "../images/icons/";
var IMAGES_PATH = "../dhtmlxV51/codebase/imgs/"; // *OJO* - debe corresponder con el archivo de skin utilizado

var GMAP1 = null;
var MAP_CONTEXT_MENU = null;
var MAP_POS_CLIC = null;
var SHAPE_DATA = null;
var GENERIC_WIN1 = null;
var WINDOWS = null;
var LOADED_IN_FRAME = false;
var MI_ORIGEN = "VISORALERTAS";

var MARKERS = [];
var rutaMostrada = false;
var P1Ruta = null, P2Ruta = null;

var MARKER_PDI = null;

// http://cbmex4.com/DispachConsole/panel/Home/pages/AlertasVisor.aspx
// ?ciaid=595&numfol=FOL-142806&codoper=Gayosso07&codpdi=HOS-ANGMEX&horaini=2017-02-24 08:00:00&horafin=2017-02-24 08:35:59
// ?ciaid=595&numfol=FOL-142806&codoper=Gayosso02&codpdi=HOS-ANGMEX&horaini=2017-06-14 14:00:00&horafin=2017-06-14 14:20:59

dhtmlxEvent(window, "load", function () {
    LOADED_IN_FRAME = LoadedInFrame();
    addEvent(window, 'message', ReceiveWebMessage);

    CreaLayout();
    ConfiguraMapa();

    var config = {};
    config.CiaId = getQueryVariable('ciaid');
    config.NumFol = getQueryVariable('numfol');
    config.CodOper = getQueryVariable('codoper');
    config.CodPdi = getQueryVariable('codpdi');
    config.HoraIni = getQueryVariable('horaini');  // "AAAA-MM-DD HH:MM:SS"
    config.HoraFin = getQueryVariable('horafin');  // "AAAA-MM-DD HH:MM:SS"

    MuestraDatos(config);

    var msgData = { "origen": MI_ORIGEN, "proceso": "LOADED", "params": {} };
    SendWebMessage(parent, msgData, "*");
});

function CreaLayout() {
    // ------------------------------------------------------------
    // CREA LAYOUT PRINCIPAL
    // ------------------------------------------------------------

    //window.dhx4.skin = 'dhx_skyblue';
    var main_layout = new dhtmlXLayoutObject(document.body, '2U');

    main_layout.attachEvent('onResizeFinish', function () {
        GMAP1.resize();
    });

    main_layout.attachEvent('onPanelResizeFinish', function () {
        GMAP1.resize();
    });

    main_layout.attachEvent('onExpand', function (id) {
        GMAP1.resize();
    });

    main_layout.attachEvent('onCollapse', function (id) {
        GMAP1.resize();
    });

    // ------------------------------------------------------------
    // CREA SECCION: MAPA DEL VISOR
    // ------------------------------------------------------------

    var mapa = main_layout.cells('a');
    mapa.hideHeader();

    // ------------------------------------------------------------
    // Toolbar del Mapa
    // ------------------------------------------------------------
    /*
    var TbarMapa = mapa.attachToolbar();
    TbarMapa.setIconsPath(ICONS_PATH);

    TbarMapa.attachEvent('onClick', function (id) {
        alert('onClick');
    });

    TbarMapa.attachEvent('onEnter', function (id, value) {
        alert('onEnter');
    });

    TbarMapa.loadStruct('../dhtmlxV5/data/toolbar.xml', function () { });
    */

    mapa.attachObject(document.getElementById("gmMap1"));

    // ------------------------------------------------------------
    // CREA SECCION: DATOS DEL VISOR
    // ------------------------------------------------------------

    var data = main_layout.cells('b');
    data.setText('Información en Mapa');
    data.setWidth('350');
    //data.hideHeader();
    data.attachObject(document.getElementById("infoMapa"));

    /*
    var GridDatos = data.attachGrid();
    GridDatos.setIconsPath(ICONS_PATH);

    GridDatos.setHeader(["Column 1", "Column 2"]);
    GridDatos.setColTypes("ro,ro");

    GridDatos.setColSorting('str,str');
    GridDatos.setInitWidths('*,*');
    GridDatos.init();
    GridDatos.load('../dhtmlxV5/data/grid.xml', 'xml');

    GridDatos.attachEvent('onRowSelect', function (id, ind) {
        alert('onRowSelect');
    });
    */

    // ------------------------------------------------------------
    // Toolbar Grid de Datos
    // ------------------------------------------------------------
    /*
    var TbarGridDatos = data.attachToolbar();
    TbarGridDatos.setIconsPath(ICONS_PATH);

    TbarGridDatos.attachEvent('onClick', function (id) {
        alert('onClick');
    });

    TbarGridDatos.attachEvent('onEnter', function (id, value) {
        alert('onEnter');
    });

    TbarGridDatos.attachEvent('onStateChange', function (id, state) {
        alert('onStateChange');
    });

    TbarGridDatos.attachEvent('onValueChange', function (id, value) {
        alert('onValueChange');
    });

    TbarGridDatos.loadStruct('../dhtmlxV5/data/toolbar.xml', function () { });
    */

    // ------------------------------------------------------------
    // MENU CONTEXTUAL EN MAPA
    // ------------------------------------------------------------

    MAP_CONTEXT_MENU = new dhtmlXMenuObject({
        //parent: "mapMenu",
        //iconset: "awesome",  // sets the Font Awesome icons for menu
        icon_path: ICONS_PATH,
        context: true,
        auto_hide: false, auto_show: false,
        items: [
            { id: "MapMnu_DibujoSave", text: "Guardar Dibujo", img: "disk.png", img_disabled: "disk.png" },
            { id: "MapMnu_DibujoCancel", text: "Cancelar Dibujo", img: "cross.png", img_disabled: "cross.png" },
            { id: "MapMnu_DibujoSeparator", type: "separator" },
            { id: "MapMnu_QuehayAqui", text: "Que hay aqui?", img: "help.png", img_disabled: "help.png" },
            { id: "MapMnu_VistaPano", text: "Vista panoramica", img: "streetview.png", img_disabled: "streetview.png" },
            { id: "MapMnu_Trafico", text: "Mostrar trafico", img: "traffic.png", img_disabled: "traffic.png" },
            { id: "MapMnu_Altitud", text: "Altitud aqui", img: "upEnd.png", img_disabled: "upEnd.png" },
            { id: "MapMnu_Regla", text: "Mostrar regla", img: "ruler_triangle.png", img_disabled: "traffic.png" },
            { id: "MapMnu_Dibujar", text: "Barra Dibujar", img: "shape_handles.png", img_disabled: "traffic.png" },
            { type: "separator" },
            {
                id: "MapMnu_Ruta", text: "Ruta", img: "chart_curve.png", img_disabled: "fa fa-folder-open-o", items: [
                    { id: "MapMnu_RutaIni", text: "Iniciar aqui", img: "markerGreen.png", img_disabled: "markerGreen.png" },
                    { id: "MapMnu_RutaFin", text: "Terminar aqui", img: "marker.png", img_disabled: "marker.png" },
                    { id: "MapMnu_RutaElim", text: "Eliminar", img: "cross.png", img_disabled: "cross.png" },
                    { id: "MapMnu_RutaInst", text: "Ver instrucciones", img: "legend.png", img_disabled: "legend.png" }
                ]
            }
        ]
    });
    //MAP_CONTEXT_MENU.renderAsContextMenu();
    //MAP_CONTEXT_MENU.setIconset("awesome");
    //MAP_CONTEXT_MENU.setSkin("dhx_web");
    MAP_CONTEXT_MENU.attachEvent("onClick", function (id, zoneId, cas) {
        if (id == "MapMnu_DibujoSave")
            alert("Guardar: " + SHAPE_DATA.type + " - " + SHAPE_DATA.id + " " + SHAPE_DATA.name);
        else if (id == "MapMnu_DibujoCancel")
            SHAPE_DATA.overlay.setMap(null);
        else if (id == "MapMnu_QuehayAqui") {
            //GMAP1.showInfoWin('<b>#dom#</b>', MAP_POS_CLIC);
            GMAP1.geocodePosition(MAP_POS_CLIC, function (dirs) {
                if (dirs.length > 0) {
                    GMAP1.showInfoWin('<b>' + dirs[0].dom + '</b>', MAP_POS_CLIC);
                }
            });
        }
        else if (id == "MapMnu_VistaPano")
            GMAP1.viewPanorama(MAP_POS_CLIC);
        else if (id == "MapMnu_Trafico")
            GMAP1.showTrafficLayer();
        else if (id == "MapMnu_Altitud") {
            GMAP1.getElevation(MAP_POS_CLIC, function (altitud) {
                if (altitud) {
                    var xStr = "<b>Altitud en este punto: " + altitud + " metros.</b>";
                    GMAP1.showInfoWin(xStr, MAP_POS_CLIC);
                } else {
                    GMAP1.MessageBox('Aviso', 'No se encontraron resultados.', function (btnOK) { });
                }
            });
        } else if (id == "MapMnu_Regla")
            GMAP1.addruler(MAP_POS_CLIC);
        else if (id == "MapMnu_Dibujar")
            GMAP1.showDrawTool();
        else if (id == "MapMnu_RutaIni")
            GMAP1.setRoute('I', MAP_POS_CLIC);
        else if (id == "MapMnu_RutaFin")
            GMAP1.setRoute('F', MAP_POS_CLIC);
        else if (id == "MapMnu_RutaElim")
            GMAP1.setRoute('B', MAP_POS_CLIC);
        else if (id == "MapMnu_RutaInst") {
            var xStr = '<div style="width:100%;height:100%;overflow:auto;">' +
            '<div style="padding:12px;">' + GMAP1.showRouteInstructions(); + '</div></div>';

            GENERIC_WIN1.attachHTMLString(xStr);
            GENERIC_WIN1.setModal(true);
            GENERIC_WIN1.show();
        }
    });

}

function ConfiguraMapa() {
    // ----------------------------------------------------------------------
    // Armado de los datos de configuracion
    // ----------------------------------------------------------------------

    var gmConfig = {
        objName: 'GMAP1',
        mapDiv: 'gmMap1',
        center: { lat: 19.40993536, lng: -99.13699951 },
        getBrowserGPS: false,
        zoom: 12,
        infoWin: {
            width: 320,
            moreText: '<br/><br/><b>Latitud/Longitud:</b> #lat#, #lng#' +
                '<br/><a href="javascript:#objName#.viewPanorama([#lat#,#lng#]);"><img src="' + ICONS_PATH + 'streetView.png" alt="" style="vertical-align:bottom;" /> Panoramica</a>&nbsp;&nbsp;&nbsp;' +
                '<b>RUTA: </b>' +
                '<a href="javascript:#objName#.setRoute(\'I\',\'#lat#,#lng#\');"><img src="' + ICONS_PATH + 'markerGreen.png" alt="" style="vertical-align:bottom;" /> Inicio</a>&nbsp;&nbsp;' +
                '<a href="javascript:#objName#.setRoute(\'F\',\'#lat#,#lng#\');"><img src="' + ICONS_PATH + 'marker.png" alt="" style="vertical-align:bottom;" /> Fin</a>&nbsp;&nbsp;' +
                '<a href="javascript:#objName#.setRoute(\'B\',\'0,0\');"><img src="' + ICONS_PATH + 'cross.png" style="vertical-align:bottom;" alt="" /> Borrar</a>&nbsp;' +
                '<br/>&nbsp;',
            moreTextUseDefault: false
        },
        icons: {
            routeStart: ICONS_PATH + 'markerGreen.png',  // icono para marcador de inicio de ruta
            routeEnd: ICONS_PATH + 'marker.png',  // icono para marcador de fin de ruta
            routeStep: ICONS_PATH + 'markerBlue.png',  // icono para marcador en cada punto de paso de la ruta
            streetView: ICONS_PATH + 'streetView.png', // icono a mostrar en links de vista panoramica 
            marker: ICONS_PATH + 'marker.png', // icono para marcador generico
            rule: ICONS_PATH + 'ruler_triangle.png', // icono para marcador de regla para medir distancias
            cancel: ICONS_PATH + 'cross.png' // icono a mostrar como marcador en opcion de regla
        },
        onClick: function (gmap, gmapEvt) {
            //alert("CLICK:\nLatLng: " + gmapEvt.latLng.toUrlValue() + "\nXY: " + gmapEvt.pixel.toString());
        },
        onRightClick: function (gmap, data) {
            /*
            data = {
                type: <'circle,rectangle,polygon,polyline,map,marker'>,
                latLng: <lat,lng>,
                pixel: <x,y>,
                name: 'nombre del objeto',
                id: 'id asociado',
                overlay: <objeto dibujado>,
                centro: <lat,lng>,                  // circle,rectangle,polygon
                area: <metros cuadrados>,           // circle,rectangle,polygon
                radio: <lat,lng>,                   // circle
                noreste: <lista de <lat,lng>>,      // rectangle
                suroeste: <lista de <lat,lng>>,     // rectangle
                path: <lista de <lat,lng>>,         // polygon,polyline
                dist: <distancia en metros>,        // polyline
            };
            */
            MAP_POS_CLIC = data.latLng;
            if (",circle,rectangle,polygon,polyline,".indexOf("," + data.type + ",") > -1) {
                // Es un shape dibujado
                //MAP_CONTEXT_MENU.isItemHidden("MapMnu_Regla");
                SHAPE_DATA = data;
                MAP_CONTEXT_MENU.showItem("MapMnu_DibujoSave");
                MAP_CONTEXT_MENU.showItem("MapMnu_DibujoCancel");
                MAP_CONTEXT_MENU.showItem("MapMnu_DibujoSeparator");
            } else {
                SHAPE_DATA = null;
                MAP_CONTEXT_MENU.hideItem("MapMnu_DibujoSave");
                MAP_CONTEXT_MENU.hideItem("MapMnu_DibujoCancel");
                MAP_CONTEXT_MENU.hideItem("MapMnu_DibujoSeparator");
            }
            MAP_CONTEXT_MENU.showContextMenu(data.pixel.x - 7, data.pixel.y - 12);
        },
        onZoomChange: function (gmap, zoom) {
            //alert("ZOOM: " + zoom);
        },
        MessageBox: function (msg, tit, type, promptValue, callBakFunc) {
            dhtmlx.message(msg);
        }
    }
    // ----------------------------------------------------------------------
    // Creacion de la instancia de la clase del mapa
    // ----------------------------------------------------------------------
    GMAP1 = new GFMap(gmConfig);
    //GMAP1.AddControl("divMapAddress", "TOP_LEFT");
}

function MuestraDatos(config) {
    var CFG = config || {};
    CFG.CiaId = CFG.CiaId || -1;
    CFG.CodOper = CFG.CodOper || "";
    CFG.CodPdi = CFG.CodPdi || "";
    CFG.NumFol = CFG.NumFol || "";
    CFG.HoraIni = CFG.HoraIni || ""; // "AAAA-MM-DD HH:MM:SS"
    CFG.HoraFin = CFG.HoraFin || "";

    var error = "";
    if (!(CFG.CiaId > 0))
        error += "'CiaId' : " + CFG.CiaId + " | ";
    if (CFG.CodOper == "")
        error += "'CodOper' : " + CFG.CodOper + " | ";
    if (CFG.CodPdi == "")
        error += "'CodPdi' : " + CFG.CodPdi + " | ";
    if (CFG.HoraIni == "")
        error += "'HoraIni' : " + CFG.HoraIni + " | ";
    if (CFG.HoraFin == "")
        error += "'HoraFin' : " + CFG.HoraFin + " | ";

    if (error != "") {
        //alert("ERROR: Datos incorrectos:\n\n" + error);
        return;
    }

    // Recupera datos de la ruta a mostrar
    window.dhx.ajax.query({
        method: "POST",
        url: "../webServices/WSAlertasVisor.asmx/OperClienteRuta",
        data: "{'cia':'" + CFG.CiaId + "','oper':'" + CFG.CodOper + "','pdi':'" + CFG.CodPdi + "','fini':'" + CFG.HoraIni + "','ffin':'" + CFG.HoraFin + "'}",
        async: true,
        callback: function (result) {
            //  result.xmlDoc => status=200   statusText="OK"  responseXML
            if (result && result.xmlDoc.status == 200) {
                var res = window.dhx.s2j(result.xmlDoc.responseText).d; // convert response to json object
                MuestraMarcadores(res, CFG);
            } else {
                alert("ERROR: " + result.xmlDoc.status + " " + result.xmlDoc.statusText);
            }
        },
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=utf-8;'
        }
    });
}

function MuestraMarcadores(res, cfg) {
    if (res.NomOper == cfg.CodOper)
        res.NomOper = "Gabriel Llorentes Solis";

    for (var i = 0; i < MARKERS.length; i++) {
        MARKERS[i].marker.setMap(null);
    }
    MARKERS = [];
    GMAP1.setRoute('B', null);
    var pointsToFit = [];

    var posIni = null, posFin = null, posCenter = null;

    document.getElementById("dtsFolio").innerHTML = "<span style='font-size:1.2em;'>" + cfg.NumFol + "</span><br/>&nbsp;";

    // UBICACION PDI

    document.getElementById("dtsPDI").innerHTML = "<b>" + cfg.CodPdi + " - " + res.NomPdi + "</b><br/>" + res.DomPdi;

    if (!(res.LatPdi == 0 && res.LatPdi == 0)) {
        var label = cfg.HoraFin.substr(11, 5) + "#BG:#ff9999";
        var title = cfg.NumFol + " - " + cfg.CodPdi + " - " + res.NomPdi;
        var text = "<b>" + cfg.HoraFin.substr(11, 5) + " " + title + "</b><br/>" + res.DomPdi;
        var icon = ICONS_PATH + "target24.png";
        var dir = new Object();
        dir.tipo = "PDI";

        dir.marker = GMAP1.createMarker([res.LatPdi, res.LngPdi], icon, label, title, text);
        MARKERS.push(dir);
        posFin = { lat: res.LatPdi, lng: res.LngPdi };
        posCenter = posFin;
        pointsToFit.push(posCenter);
    }

    // UBICACION DEL OPER (Pos. actual)
    document.getElementById("dtsOper").innerHTML = "<b>" + cfg.CodOper + " - " + res.NomOper + "</b>";

    if (res.posiciones.length > 0) {
        var label = res.posiciones[0].fch.substr(11, 5);
        var title = cfg.CodOper + " - " + res.NomOper;
        var text = "<b>" + label + " - " + title + "</b><br/>#dom#";
        var icon = ICONS_PATH + "user24.png";
        var dir = new Object();
        dir.tipo = "OPER";

        dir.marker = GMAP1.createMarker([res.posiciones[0].lat, res.posiciones[0].lng], icon, label, title, text);
        MARKERS.push(dir);
        posCenter = { lat: res.posiciones[0].lat, lng: res.posiciones[0].lng };
        pointsToFit.push(posCenter);
    }

    // POS. CERCANA AL INICIO DE LA ALERTA

    if (res.posiciones.length > 0) {
        var uPos = res.posiciones.length - 1;
        var label = res.posiciones[uPos].fch.substr(11, 5) + "#BG:#00ff00";
        var title = "Inicio de Traslado";
        var text = "<b>" + res.posiciones[uPos].fch.substr(11, 5) + " " + title + "</b><br/>#dom#";
        var icon = ICONS_PATH + "icong.png";
        var dir = new Object();
        dir.tipo = "INI";

        dir.marker = GMAP1.createMarker([res.posiciones[uPos].lat, res.posiciones[uPos].lng], icon, label, title, text);
        MARKERS.push(dir);
        posIni = { lat: res.posiciones[uPos].lat, lng: res.posiciones[uPos].lng };
        pointsToFit.push(posIni);
    }

    // Puntos de Rastreo entre las fechas seleccionadas
    var linRas = [];
    for (var i = 0; i < res.posiciones.length; i++) {
        var pos = { lat: res.posiciones[i].lat, lng: res.posiciones[i].lng };
        if (i > 0 && i < (res.posiciones.length - 1)) {
            var label = res.posiciones[i].fch.substr(11, 5);
            var title = label + " Rastreo";
            var text = "<b>" + title + "</b><br/>#dom#";
            var icon = ICONS_PATH + "marker.png";

            var dir = new Object();
            dir.tipo = "RAST";

            dir.marker = GMAP1.createMarker(pos, icon, "", title, text);
            MARKERS.push(dir);
        }
        linRas.push(pos);
    }

    // Linea entre Inicio de alerta y PDI
    /*
    var dir = new Object();
    dir.tipo = "LINE-INI";
    dir.marker = new google.maps.Polyline({
        path: [posIni, posFin],
        strokeColor: "#0066CC",
        strokeOpacity: .5,
        strokeWeight: 2
    });
    dir.marker.setMap(GMAP1.map);
    MARKERS.push(dir);
    */
    P1Ruta = posIni;
    P2Ruta = posFin;
    MostrarRuta();

    // Linea Rastreo
    dir = new Object();
    dir.tipo = "LINE-RAS";
    dir.marker = new google.maps.Polyline({
        path: linRas,
        strokeColor: "#ef6a00",
        strokeOpacity: .5,
        strokeWeight: 2
    });
    dir.marker.setMap(GMAP1.map);
    MARKERS.push(dir);

    if (pointsToFit.length > 0) {
        GMAP1.ZoomToFit(pointsToFit);
    }
}

function MostrarRastreo() {
    var mostrarRastreo = document.getElementById("verRastreo").checked;
    var unirRastreo = document.getElementById("unirRastreo").checked;

    for (var i = 0; i < MARKERS.length; i++) {
        if (MARKERS[i].tipo == "RAST")
            MARKERS[i].marker.setMap(mostrarRastreo ? GMAP1.map : null);
        if (MARKERS[i].tipo == "LINE-RAS") {
            if (mostrarRastreo && unirRastreo)
                MARKERS[i].marker.setMap(GMAP1.map);
            else
                MARKERS[i].marker.setMap(null);
        }
    }
}

function MostrarRuta() {
    if (rutaMostrada) {
        GMAP1.setRoute('B', null);
    } else {
        GMAP1.setRoute('I', P1Ruta);
        GMAP1.setRoute('F', P2Ruta);
    }
    rutaMostrada = !rutaMostrada;
}

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}

function MapaAgregarPDI(pdiId) {
    pdiId = pdiId || -1;
    if (pdiId < 1)
        return;

    // Recupera datos del PDI a mostrar
    window.dhx.ajax.query({
        method: "POST",
        url: "../webServices/WSAlertasVisor.asmx/DatosPDI",
        data: "{'id':'" + pdiId + "'}",
        async: true,
        callback: function (result) {
            //  result.xmlDoc => status=200   statusText="OK"  responseXML
            if (result && result.xmlDoc.status == 200) {
                var res = window.dhx.s2j(result.xmlDoc.responseText).d; // convert response to json object
                if (res._estatus != "OK")
                    alert("ERROR: " + result.xmlDoc.status + " " + result.xmlDoc.statusText);
                else
                    MapaAgregarPDI_Result(res);

                var data = { "origen": MI_ORIGEN, "proceso": "LOADED", "params": {} };
                SendWebMessage(parent, data, "*");
            } else {
                alert("ERROR: " + result.xmlDoc.status + " " + result.xmlDoc.statusText);

                var data = { "origen": MI_ORIGEN, "proceso": "LOADED", "params": {} };
                SendWebMessage(parent, data, "*");
            }
        },
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=utf-8;'
        }
    });
}

function MapaAgregarPDI_Result(res) {
    if (res._estatus != "OK")
        return;
    // res => _estatus, Id, Codigo, Nombre, Domicilio, Latitud, Longitud
    if (MARKER_PDI)
        MARKER_PDI.setMap(null);

    var pos = { lat: res.Latitud, lng: res.Longitud };
    var icon = ICONS_PATH + "marker.png";
    var label = res.Codigo;
    var title = label + " - " + res.Nombre;
    var text = "<b>" + title + "</b><br/>" + res.Domicilio;

    MARKER_PDI = GMAP1.createMarker(pos, icon, label, title, text);
    GMAP1.Center(pos);
}

// FUNCIONES PARA COMUNICACION POST MESSAGE

function LoadedInFrame() {
    var loadedInFrame = false;
    try {
        loadedInFrame = (window.self !== window.top);
    } catch (e) {
        loadedInFrame = true;
    }
    return loadedInFrame;
}

// add event cross browser
function addEvent(elem, event, fn) {
    if (elem.addEventListener) {
        elem.addEventListener(event, fn, false);
    } else {
        elem.attachEvent("on" + event, function () {
            // set the this pointer same as addEventListener when fn is called
            return (fn.call(elem, window.event));
        });
    }
}

function ReceiveWebMessage(e) {
    if (!(e.origin == "http://cbmex4.com" || e.origin == "http://cbmex7.com"))
        return;
    // data =>  {"origen":"xxx", "proceso":"yyy", "params":{...}}

    var dataEnviar = { "origen": MI_ORIGEN, "proceso": e.data.proceso, "params": {} };
    if (e.data.proceso == "SIGUE_VIVO") {
        e.source.postMessage(dataEnviar, e.origin);
    } else if (e.data.proceso == "CARGARPDI") {
        MapaAgregarPDI(e.data.params.PDIID);
    }
}

function SendWebMessage(obj, data, url) {
    // use JSON.stringify() and JSON.parse() to deal with
    // sending any kind of data structure that isn’t a string.
    // data =>  {"origen":"xxx", "proceso":"yyy", "params":{...}}
    if (!LOADED_IN_FRAME)
        return;

    obj.postMessage(data, url);
}

function FinSesion() {
    try {
        var data = {
            "origen": MI_ORIGEN,
            "proceso": "FINSESION",
            "params": {}
        };
        SendWebMessage(parent, data, "*");
    } catch (e) {
        //alert(e.message);
    }
}
