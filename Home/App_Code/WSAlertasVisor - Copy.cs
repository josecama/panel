﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WSAlertasVisor
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WSAlertasVisor : System.Web.Services.WebService {


    public WSAlertasVisor () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public OperClienteRuta_Result OperClienteRuta(int cia, string oper, string pdi, string fini, string ffin)
    {
        string cnnStr = "Data Source=NOZONE-43E0E307;Initial Catalog=MapaLocalizadorVisor;Persist Security Info=True;Connection Timeout=150;User ID=sa;Password=_sa";
        if (HttpContext.Current.Request.Url.Host == "localhost")
            cnnStr = "Data Source=SQLSVRTEST;Initial Catalog=MapaLocalizadorVisor;Persist Security Info=True;Connection Timeout=150;User ID=sa;Password=_sa";
        else if (HttpContext.Current.Request.Url.Host == "cbmex4.com")
            cnnStr = "Data Source=10.1.2.87;Initial Catalog=MapaLocalizadorVisor;Persist Security Info=True;User ID=sa;Password=_sa";

        SqlConnection cnn = new SqlConnection(cnnStr);
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        cmd.Connection = cnn;

        OperClienteRuta_Result res = new OperClienteRuta_Result();
        res._estatus = "";
        res.NomOper = "";
        res.NomPdi = "";
        res.DomPdi = "";
        res.ixPosHoraIni = -1;
        res.LatPdi = 0;
        res.LngPdi = 0;
        res.posiciones = new List<Position>();

        try
        {
            cnn.Open();

            // Recupera datos del OPERADOR

            cmd.CommandText = "SELECT OPER_ID, OPER_CODIGO, OPER_NOMBRE " +
                "FROM [MapaLocalizadorVisor].[dbo].[OPER] " +
                "WHERE OPER_CIA_ID=" + cia + " AND OPER_CODIGO='" + oper + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows && dr.Read())
            {
                res.NomOper = dr["OPER_NOMBRE"].ToString();
            }
            dr.Close();

            // Recupera datos del PDI

            cmd.CommandText = "SELECT piNombre, piDirRutaIntegrada, piLatitud, piLongitud " +
                "FROM [MapaLocalizadorVisor].[dbo].[PuntosInteres] " +
                "WHERE piCoId=" + cia + " AND piClave='" + pdi + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows && dr.Read())
            {
                res.NomPdi = dr["piNombre"].ToString();
                res.DomPdi = dr["piDirRutaIntegrada"].ToString();
                res.LatPdi = (double)dr["piLatitud"];
                res.LngPdi = (double)dr["piLongitud"];
            }
            dr.Close();

            // Recupera Rastreo del Operador

            cmd.CommandText = "SELECT TOP 1000 pId,pubLatitud,pubLongitud,pubFechaGMT,pubFechaLocal " +
                "FROM [Ubicacell].[dbo].[Positions] " +
                "WHERE pubCiaId = " + cia + " AND pEqCodPersona = '" + oper + "' " +
                "AND pubFechaLocal BETWEEN '" + fini + "' AND '" + ffin + "' " +
                "ORDER BY pubFechaLocal desc ";
            dr = cmd.ExecuteReader();
            while (dr.HasRows && dr.Read())
            {
                Position pos = new Position();
                pos.fch = ((DateTime)dr["pubFechaLocal"]).ToString("yyyy-MM-dd HH:mm:ss");
                pos.lat = double.Parse(dr["pubLatitud"].ToString());
                pos.lng = double.Parse(dr["pubLongitud"].ToString());
                res.posiciones.Add(pos);
            }
            dr.Close();
        }
        catch (Exception ex)
        {
            res._estatus = "ERROR: " + ex.Message;
        }

        if (cnn.State.Equals(ConnectionState.Open))
            cnn.Close();

        return res;
    }

    public struct OperClienteRuta_Request
    {
        public int CiaId;
        public string CodOper;
        public string CodPdi;
        public string HoraIni;
        public string HoraFin;
    }
    public struct OperClienteRuta_Result
    {
        public string _estatus;
        public string NomOper;
        public string NomPdi;
        public string DomPdi;
        public double LatPdi;
        public double LngPdi;
        public List<Position> posiciones;
        public int ixPosHoraIni;
    }
    public struct Position
    {
        public double lat;
        public double lng;
        public string fch;
    }
}
