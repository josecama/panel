var cell_indicators = null;
var cell_searchbar = null;
var cell_table = null;
var grid_alerts = null;
var cell_alert = null;
var tabbar_alert = null;
var tab_details = null;
var tab_history = null;
var cell_header_tree = null;
var form_searchbar = null;
var cell_tree = null;
var grid_tree_activas = null;
var grid_tree_historico = null;
var menu_grid = null;

var grid_alerts_selected = null;

//data
var grid_tree_activas_data = [];
var grid_tree_historico_data = [];
var grid_alerts_data = [];

dhtmlxEvent(window, "load", function () {
    CreateLayout();
    Init();
});

var grid_row = {
	'id' : '',
	'cell_row' : [],
	'position' : '',
	'parent_id' : ''
}

function Init(){
	cell_alert.attachObject('cell_alert');
    //cell_searchbar.attachObject('cell_searchbar');
	cell_tree.attachObject('cell_tree');
	cell_indicators.attachURL('ModuleAlerts.html');

	Create_Grid_Activas();
	Create_Grid_Historico();
	
	data_grid_tree_example();

	Refresh_Grid_Tree_Activas();
	Refresh_Grid_Tree_Historico();

	InitEvents();
	Create_Menu_Grid();

	//$("#cell_alert").parent().css("overflow", "scroll");
	$(".dhxform_btn_filler").eq(0).parent().css("margin-left", 20);
	$(".dhx_cell_cont_layout").css("padding", 0);
	$("#grid_tree_activas .xhdr").css("border-bottom", "2px solid white");
	cell_indicators.setHeight('34');
	cell_searchbar.setHeight('37');

	/////////////////////////////////////
	$('#select-email-multiple').select2({
		tags: "true",
		placeholder: "",
		allowClear: true,
		"language": {
			"noResults": function(){
				return "Resultados no encontrados.";
			}
		},
		escapeMarkup: function (markup) {
			return markup;
		}
	});

	$('#select-cola-multiple').select2();
	grid_tree_activas.selectRow(4);
	/*setTimeout(function(){ 
		
	}, 3000);*/
	
	Clear_Grid_Alerts();
    data_grid_alerts();
    Refresh_Grid_Alerts();
	grid_alerts.selectRow(0);

	$("#cell_alert").parent().css("border", "1px solid black");
	$("#cell_alert").parent().css("top", "0px");
	$("#cell_alert").parent().parent().children('.dhx_cell_hdr_hidden').hide();
	
}

function Create_Menu_Grid(){
	menu_grid = new dhtmlXMenuObject();
	menu_grid.renderAsContextMenu();	
	menu_grid.addNewChild(menu_grid.topId, 0, "llamada", "Llamada", false, null);
	menu_grid.addNewChild(menu_grid.topId, 1, "chat", "Notificación Chat", false, null);
	menu_grid.addNewChild(menu_grid.topId, 2, "sms", "Enviar SMS", false, null);
	menu_grid.attachEvent("onClick", function(id, zoneId, cas){
		switch(id){
			case 'llamada':
				Menu_Grid_Llamada(grid_alerts_selected);
				break;
			case 'chat':
				Menu_Grid_Chat(grid_alerts_selected);
				break;
			case 'sms':
				Menu_Grid_SMS(grid_alerts_selected);
				break;
		}
	});
	grid_alerts.attachEvent("onBeforeContextMenu", function(id, ind, obj){
		grid_alerts_selected = id;

		return true;
	});

	grid_alerts.enableContextMenu(menu_grid);
}

function InitEvents(){
	grid_tree_activas.attachEvent("onRowSelect",function(rowID, celInd){
		grid_tree_activas_onRowSelect(rowID, celInd);
	});

	grid_tree_historico.attachEvent("onRowSelect",function(rowID, celInd){
		grid_tree_historico_onRowSelect(rowID, celInd);
	});

	grid_alerts.attachEvent("onRowSelect",function(rowID, celInd){
		//debugger;
	});
}

function Clear_Grid_Alerts(){
	grid_alerts_data = [];
	grid_alerts.clearAll();
}

function Refresh_Grid_Alerts(){
	for (var key in grid_alerts_data) {
		if (grid_alerts_data.hasOwnProperty(key)) {
			var row = grid_alerts_data[key];
			AddRow_Grid_Alerts(row);
		}
	}
}

function Refresh_Grid_Tree_Activas(){
	Refresh_Grid_Tree(grid_tree_activas, grid_tree_activas_data);
	$('#grid_tree_activas').css('width', 400);
	//$('#Activas .xhdr').css('height', 0);
}

function Refresh_Grid_Tree_Historico(){
	Refresh_Grid_Tree(grid_tree_historico, grid_tree_historico_data);
}

function Refresh_Grid_Tree(grid_tree, grid_tree_data){
	for (var key in grid_tree_data) {
		if (grid_tree_data.hasOwnProperty(key)) {
			var row = grid_tree_data[key];
			AddRow_GridTree(grid_tree, row);
		}
	}
}

function Create_Grid_Activas(){
		grid_tree_activas = new dhtmlXGridObject('grid_tree_activas');
		grid_tree_activas.setImagePath("./assets/dhtmlx/skins/mapcel/imgs/");
		grid_tree_activas.setHeader("1,2");
		grid_tree_activas.setInitWidths("340,50");
		grid_tree_activas.setColAlign("left,center");
		grid_tree_activas.setColTypes("tree,ro");
		grid_tree_activas.enableResizing("false,false");
		grid_tree_activas.enableAutoHeight(true, ($(window).height() - 90), true);
		//grid_tree_activas.enableAutoHeight(true, ($(window).height() - 130), true);
		grid_tree_activas.init();
}

function AddRow_Grid_Alerts(data_row){
	grid_alerts.addRow(data_row.Id, data_row.Value);
}

function AddRow_GridTree(gridObject, data_row){
	var icon_row = "";
	if(data_row.parent_id == null){
		icon_row = "folder.gif";
	}
	else{
		icon_row = "blank.gif";
	}

	gridObject.addRow(data_row.id, data_row.cell_row, data_row.position, data_row.parent_id, icon_row, false);
	gridObject.expandAll();
}

function Create_Grid_Historico(){
	grid_tree_historico = new dhtmlXGridObject('grid_tree_historico');
	grid_tree_historico.setImagePath("./assets/dhtmlx/skins/mapcel/imgs/");
    grid_tree_historico.setHeader('<label style="margin-right: 5px;">De:</label><input type="text" style="width: 30%;" value="01/Mar/2017" /><button type="button"><span class="glyphicon glyphicon-calendar"></span></button><label style="margin-left:10px; margin-right: 5px;">A:</label><input type="text" style="width: 30%;" value="05/Mar/2017" /><button type="button"><span class="glyphicon glyphicon-calendar"></span></button>,<span style="background-color: #BA1919; display: block; width: 17px; position: relative; left: -6px; color: white; text-align: center;">14</span>,<span style="background-color: #F8821F; display: block; width: 17px; position: relative; left: -6px; color: white; text-align: center;">6</span>,<span style="background-color: #E6B63D; display: block; width: 17px; position: relative; left: -6px; color: white; text-align: center;">9</span>');
    grid_tree_historico.setInitWidths("310,25,25,25");
    grid_tree_historico.setColAlign("left,center,center,center",);
    grid_tree_historico.setColTypes("tree,ro,ro,ro");
	grid_tree_historico.enableResizing("false,false,false,false");
	grid_tree_historico.enableAutoHeight(true, ($(window).height() - 117), true);
    grid_tree_historico.init();
}

function ChangeTabActive(ItemTab){
    ItemTab.setActive();
}

function CreateLayout(){
	window.dhx4.skin = 'dhx_web';
	var main_layout = new dhtmlXLayoutObject(document.body, '3T');

	var a = main_layout.cells('a');
	a.hideHeader();
	a.fixSize(0,1);
	var layout_5 = a.attachLayout('1C');

	var cell_indicators = layout_5.cells('a');
	cell_indicators.setHeight('20');
	cell_indicators.hideHeader();
	cell_indicators.fixSize(0,1);





	var b = main_layout.cells('b');
	var layout_2 = b.attachLayout('2E');

	var cell_4 = layout_2.cells('a');
	cell_4.setHeight('222');
	cell_4.hideHeader();
	var layout_3 = cell_4.attachLayout('2E');

	var cell_searchbar = layout_3.cells('a');
	cell_searchbar.setHeight('40');
	cell_searchbar.setWidth('50');
	cell_searchbar.hideHeader();
	cell_searchbar.fixSize(0,1);
	var str = [
		{ type:"input" , name:"form_input_1", label:"Nombre-Oper", className:"font-size-dhtmlx-search", inputWidth:60, offsetLeft:"20"  },
		{ type:"newcolumn"   },
		{ type:"input" , name:"form_input_2", label:"Cod-Oper", className:"font-size-dhtmlx-search", inputWidth:100, offsetLeft:"20"  },
		{ type:"newcolumn"   },
		{ type:"input" , name:"form_input_3", label:"Folio", className:"font-size-dhtmlx-search", inputWidth:90, offsetLeft:"20"  },
		{ type:"newcolumn"   },
		{ type:"input" , name:"form_input_4", label:"PDI", className:"font-size-dhtmlx-search", inputWidth:90, offsetLeft:"20"  },
		{ type:"newcolumn"   },
		{ type:"input" , name:"form_input_5", label:"AlertaID", className:"font-size-dhtmlx-search", labelWidth:60, labelAlign:"right", inputWidth:90  },
		{ type:"newcolumn"   },
		{ type:"button" , name:"form_button_1", value:"Filtrar", width:"50"  }
	];
	var form_1 = cell_searchbar.attachForm(str);




	var cell_table = layout_3.cells('b');
	cell_table.hideHeader();
	cell_table.fixSize(0,1);
	var grid_alerts = cell_table.attachGrid();
	grid_alerts.setIconsPath('./assets/dhtmlx/skins/mapcel/imgs/dhxgrid_web');
	
	grid_alerts.setHeader(["Nombre Operador","Código Oper","Primera Alerta","Última Alerta","Territorio","Tot-1D","Otras","Total","<span class='glyphicon glyphicon-lock'></span>"]);
	grid_alerts.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
	
	grid_alerts.setColAlign('left,left,left,left,left,left,left,left,left');
	grid_alerts.setColVAlign('middle,middle,middle,middle,middle,middle,middle,middle,middle');
	grid_alerts.enableResizing('true,true,false,false,false,false,false,false,true');
	grid_alerts.setColSorting('str,str,str,str,str,str,str,str,str');
	grid_alerts.setInitWidths('150,100,120,120,140,50,50,50,30');
	//grid_alerts.enableMultiselect(true);
	grid_alerts.init();
	//grid_alerts.load('./data/grid.xml', 'xml');
	





	var cell_alert = layout_2.cells('b');
	cell_alert.hideHeader();
	cell_alert.fixSize(0,1);





	var c = main_layout.cells('c');
	c.setWidth('400');
	c.fixSize(1,0);
	var layout_1 = c.attachLayout('1C');

	var cell_tree = layout_1.cells('a');
	cell_tree.setHeight('85');
	cell_tree.hideHeader();
	cell_tree.fixSize(0,1);
    //////////////////////////////////////////////////////////////////
    this.cell_indicators = cell_indicators;
    this.cell_searchbar = cell_searchbar;
    this.cell_table = cell_table;
    this.grid_alerts = grid_alerts;
    this.cell_alert = cell_alert;
    this.tabbar_alert = tabbar_alert;
    this.tab_details = tab_details;
    this.tab_history = tab_history;
    this.cell_header_tree = cell_header_tree;
    this.form_searchbar = form_searchbar;
    this.cell_tree = cell_tree;
}