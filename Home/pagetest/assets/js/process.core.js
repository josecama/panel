//////////////////////////////// Enviar informacion al hijo
function sendMessage(obj, data, url){
	url = url || "";

	if(url == "")
		url = "http://cbmex4.com/DispachConsole/Panel/Home/pagetest/";

    obj.postMessage(data, url);
}

function receiveMessage(callback){
    window.addEventListener('message', callback, false);
}

function callback(data){
	if(data.origin == "http://www.cbmex7.com" || data.origin == "http://www.cbmex4.com"){
		var origen = data.data.origen;
		var proceso = data.data.proceso;
    	var parameters = data.data.params;
    	window[origen + proceso](parameters);
	}
}